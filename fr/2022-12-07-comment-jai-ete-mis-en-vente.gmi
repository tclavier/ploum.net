# Comment j’ai été mis en vente sur le Web… à mon insu !

> Dans ce billet, je vous explique comment j’ai découvert qu’une société de marketing propose mes services, mettant en avant une version fantaisiste de ma biographie, sans que j’en aie été informé.

Mon recueil de nouvelles « Stagiaire au spatioport Oméga 3000 » s’ouvre sur la génération d’un auteur artificiel adapté à vos goûts selon vos données personnelles collectées.

Lorsque j’ai écrit cette introduction, j’étais persuadé que j’allais me faire rattraper un jour ou l’autre par la réalité. Je n’imaginais pas que ce serait avant même que le livre soit disponible dans les librairies !

Et pour cause…

## La découverte d’un conférencier homonyme

Après avoir publié un billet sur l’invasion des contenus générés par des AI, j’allais faire directement l’expérience de devenir un conférencier généré automatiquement ! 

=> /2022-12-05-drowning-in-ai-generated-garbage.gmi Drowning in AI generated garbage

Testant mon nouveau site, quelle ne fut pas ma surprise de trouver sur la première page Google de la recherche « Lionel Dricot » un profil à mon nom sur un site dont je n’avais jamais entendu parler.

=> /files/google_sn.png Capture d’écran d’une recherche Google pour « Lionel Dricot »

Un profil décrivant ma biographie avec moult détails, reprenant des photos et vidéos de diverses conférences. J’étais intrigué. Sur Mastodon, un lecteur me signala que le site était chez lui le premier résultat Bing pour une recherche sur mon patronyme .

=> /files/bing_sn.png Capture d’écran d’une recherche Bing pour « Lionel Dricot »

Un site étrange, à l’apparence très professionnelle et qui se présente comme une entreprise de « Celebrity Marketing ». Le simple fait que je sois sur un site de Celebrity Marketing a fait pouffer mon épouse. Elle a d’ailleurs remarqué que l’entreprise tire son nom de Simone Veil et Nelson Mandela. Utiliser Simone Veil et Nelson Mandela pour faire du « Celebrity Marketing », ça pose le niveau ! Ah ouais quand même…

=> /files/profil_sn.png Mon profil sur le site incriminé

Petite précision : je ne ferai pas de lien vers ce site, car c’est explicitement interdit dans leurs conditions d’utilisation.

=> /files/cu1_sn.png Conditions d’utilisation du site S&N interdisant de faire un lien vers le site

Pratiquement, que fait cette société ? C’est très simple : elle met en contact des entreprises à la recherche de conférenciers et des conférenciers. C’est un service assez courant, j’ai même été en contact il y a quelques années avec une agence de ce genre. Souvent, ces agences signent un contrat d’exclusivité : le conférencier est obligé de passer par l’agence pour toutes les conférences qu’il donne. En échange, l’agence lui trouve des conférences, fait sa promotion, le place, voire lui trouve un remplaçant en cas de forfait (j’ai moi-même effectué ce genre de remplacements).

Sauf que dans le cas présent, je n’ai signé aucun contrat, je n’ai pas donné mon accord ni même été vaguement informé ! Le site donne l’impression que, pour me contacter, il faut absolument passer par eux. Nous ne sommes plus dans la bêtise, mais dans la malhonnêteté caractérisée.

=> /files/contact_sn.png Formulaire pour me contacter… via le site S&N !

## Où je découvre des facettes ignorées de ma propre vie

La lecture de ma biographie est particulièrement intéressante, car, à première vue, elle est tout à fait crédible. Une personne peu informée n’y trouverait, à première vue, pas grand-chose à redire à part quelques fautes d’orthographe (mon roman s’appelle « Printeurs », à la française, pas « Printer » et j’ai du mal à imaginer qu’il puisse être perçu comme un message d’espoir ! La scène du nouveau-né dans le vide-ordure n’était pas assez explicite ?)

Mais une lecture attentive relève des aberrations. Ces aberrations ont toutes une explication pour peu qu’on se mette à creuser. Ainsi j’aurais écrit une nouvelle intitulée « Voulez-vous installer Linux mademoiselle ? ». Comme l’a découvert un lecteur, cette phrase est extraite d’une de mes nouvelles intitulées « Les non-humains », publiée sur Linuxfr et Framasoft. 

=> https://framablog.org/2009/05/18/les-non-humains-une-nouvelle-de-ploum/ Les non-humains sur Framasoft

J’ai également appris également que je suis cofondateur d’Ubuntu. Excusez du peu ! C’est bien entendu faux. Je suis co-auteur du premier livre publié sur Ubuntu, ce qui est très différent. Certaines phrases semblent également sorties de leur contexte (pourquoi insister sur l’obésité et la malnutrition ?) Enfin, le tout se termine par le sublime :

> Lors de ses conférences, Ploum nous prédit un monde plus sain et doux.

Le ton général et les références font fortement penser à un texte généré artificiellement. Du type : « Donne-moi une biographie de Lionel Dricot », le tout en anglais suivi d’une traduction automatique. Il est possible que ce soit ce qu’on appelle un « mechanical turk », un travailleur sous-payé à qui on demande un travail que pourrait faire une IA (très fréquent dans les chats de support). Mais cela aurait dû au moins lui prendre une heure et j’ai du mal à imaginer qu’on paye une heure de travail pour pondre ma biographie.

Que le texte soit ou non généré par une IA, cela ne change rien. Il pourrait très bien l’être et est représentatif de ce que produisent et produiront toujours les IAs : quelque chose qui a l’air correct, mais est constellé de fautes difficilement détectables pour un non-spécialiste (j’ai la chance d’être le plus grand spécialiste vivant de ma propre biographie).

## Comment réagir ?

À ce stade, je pourrais tout simplement envoyer un mail et exiger le retrait de la page, l’histoire en resterait là. J’ai alerté une connaissance qui est également sur ce site.

Mais ce serait trop facile. L’existence de ce profil pose plusieurs problèmes.

Premièrement en se mettant en intermédiaire entre moi et des clients potentiels sans mon accord et en donnant l’impression que je suis affilié à cette entreprise. Cela pourrait sérieusement nuire à mon image ou à mon business (si j’avais l’une ou l’autre).

Mais l’existence de ce genre de profil peut tordre la réalité de manière encore plus insidieuse. Admettons qu’un wikipédien, affilié ou nom à cette entreprise, se serve de ces infos pour créer une fiche Wikipédia à mon nom. Cela semble parfaitement légitime vu que cette page semble avoir été faite avec mon accord. Cette info pourrait être reprise ailleurs. Soudainement, je deviendrais l’auteur d’une nouvelle que je n’ai jamais écrite. De nombreux libristes informés s’affronteront pour savoir si je suis oui ou non cofondateur d’Ubuntu. Déjà que je suis devenu un écrivain français sur Babelio !

En envoyant un simple mail pour demander le retrait de cette page, je légitime cette pratique business et me prépare à devoir surveiller en permanence le web pour faire retirer les profils générés sans mon accord.

Attaquer en justice une société dans un pays qui n’est pas le mien (car Babelio se plante, pour info) ? Ô joies administratives en perspectives ! (si vous êtes juriste spécialisé et intéressé, contactez-moi)

Ou alors il me reste la solution de lutter avec mes armes à moi. De faire le ploum et de vous raconter cette histoire de la manière la plus transparente possible. Afin de vous mettre en garde sur le fait que tout ce que vous lisez sur le web est désormais un gloubi-boulga qui a l’air sérieux, qui a l’air correct, mais qui ne l’est pas. Toutes les plateformes sont impactées. Tous les résultats des moteurs de recherche. 

En rendant cette histoire publique, je sais que la société va réagir avec « ouin-ouin je suis une entrepreneuse-je-ne-pensais-pas-à-mal-je-le-ferai-plus » ou alors « c’est-le-stagiaire-qui-a-fait-une-erreur-on-le-surveillera-mieux » voire « on-a-fait-ce-profil-avec-nos-petites-mains-parcec-qu’on-admire-votre-travail-on-penserait-que-vous-seriez-flatté ». Bref d’odieux mensonges hypocrites. C’est la base du métier du marketing : mentir pour pourrir la vie des autres (et détruire la planète).

Et si la malhonnêteté ne vous est pas encore flagrante, apprenez que la société se targue de posséder la propriété intellectuelle des textes et photos sur son site. Je pense que le photographe du TEDx Louvain-la-Neuve serait ravi de l’apprendre… La plupart de ces images de moi ne sont même pas sous licence libre !

=> /files/cu2_sn.png Conditions d’utilisation du site S&N stipulant la propriété intellectuelle des contenus

## Le futur du web…

Si cela n’était pas encore clair, je suis désormais la preuve vivante que tout ce que pond le marketing est du mensonge. Ce qui est juste ne l’est que par hasard. Et tout ce qui nous tombe sous les yeux est désormais du marketing. Pour sortir de ce merdier, il va falloir trouver des solutions (Bill Hicks en proposait une très convaincante…).

=> https://invidious.esmailelbob.xyz/watch?v=tHEOGrkhDp0 Bill Hicks on Marketing

Nous allons devoir reconstruire des cercles de confiance. Oublier nos formations à reconnaître les « fake news » et considérer toute information comme étant fausse par défaut. Identifier les personnes en qui nous avons confiance et vérifier qu’un texte signé avec leur nom est bien de leur plume. Ce n’est pas parce qu’il y’a un cadenas vert ou une marque bleue à côté du pseudo que l’on peut faire confiance. C’est même peut-être le contraire…

Bref, bienvenue dans un web de merde !
