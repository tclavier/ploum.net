# Des petits gestes…

Aucun service Google ne tourne sur mon téléphone. En utilisant l’application Adguard, j’ai découvert que mon téléphone faisait de nombreuses requêtes vers facebook.com. Le coupable ? L’application de l’IRM, l’institut royal météorologique, une institution pour laquelle j’ai beaucoup de respect.

=> https://www.meteo.be/fr/ Site officiel de l’IRM

Je me suis donc rendu sur le site de l’IRM et j’ai soumis une plainte arguant qu’un service public ne devait/pouvait pas contribuer à ces pratiques, surtout sans informer ses utilisateurs. La réponse m’est parvenue après quelques jours :

> Notre application contenait effectivement une librairie Facebook que nous n'utilisions pas, mais qui était malheureusement restée activée. Nous avons introduit une demande auprès du service qui s'occupe de notre application de supprimer cette librairie. Nos collègues vous remercient d'avoir remarqué cette erreur de notre part et de nous l'avoir communiquée.

Voilà, c’était tout simple. Bien sûr, dans un monde idéal, les applications financées par le service public devraient être open source mais cela fait plaisir de souligner qu’être espionné par Facebook n’est plus considéré comme normal et allant de soi.

Bon, évidemment, on  ne parle pas de la rtbf, un service public dont le site web est une honte lorsqu’on voit à quel point il est littéralement rempli d’espions logiciels.

Une autre petite résolution tout simple que j’ai prise est de désormais mettre les vidéos de mes conférences sur Peertube plutôt que de vous envoyer sur Youtube.

=> https://indymotion.fr/c/oneploumshow/videos La chaîne Peertube de Ploum

Ma dernière conférence, « Pourquoi ? », a été visionnée dix fois plus sur Peertube que sur le Youtube officiel de la conférence. Tout simplement car j’ai mis le lien Peertube en avant par rapport à celui vers Youtube. La preuve s’il en est que ce ne sont pas ces plateformes qui nous apportent des vues mais bien le contraire. Mettre Peertube « par défaut » est une démarche assez simple qui peut avoir au final un impact important.

En explorant Peertube à travers Sepia Search, j’ai d’ailleurs découvert qu’un·e archéoternaute y avait uploadé une de mes œuvres de jeunesse.

=> https://search.joinpeertube.org/ Sepia Search - la recherche sur Peertube
=> https://peertube.iriseden.eu/w/9852b63e-572e-4b8a-b091-57d891f61947 Ploum - Ce pauvre ordi

Heureusement pour les mélomanes, c’est la seule chanson qui semble avoir survécu. Comme vous avez tous pu le constater, je suis un parolier, pas un musicien ni un chanteur… Si je retrouve d’autres chansons ou courts-métrages, faudra que j’uploade tout ça un jour. 

Bref, se libérer des monopoles et du capitalisme de surveillance est une lutte comparable à l’écologie : il faut théoriser, discuter les enjeux planétaires. Mais il est également possible d’accomplir des petits gestes individuels qui peuvent inspirer d’autres et, sur le long terme, faire la différence.


