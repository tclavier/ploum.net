# Je suis un prisonnier
=> files/old/Screen-Shot-2015-01-17-at-10.48.061-e1421488522956.png

Un homme a sacrifié son mariage, sa vie de famille et a délaissé l’éducation de ses enfants afin de subvenir aux besoins de son frère handicapé. Et il a toujours considéré ce sacrifice comme allant de soi. Jusqu’au jour où…

=> https://indymotion.fr/w/6wumyCvkRaG1Gmb1cU1NEF Visionner le court-métrage sur PeerTube
=> http://www.dailymotion.com/video/x2ln8ct_je-suis-un-prisonnier_shortfilms Visionner le court-métrage sur dailymotion 

« Je suis un prisonnier » est le premier court-métrage dont j’ai écrit le scénario sans l’avoir réalisé moi-même. Réalisé par Thomas van der Straeten dans le cadre du Festival Nikon, l’écriture de « Je suis un prisonnier » était assortie de lourdes contraintes : 140 secondes max, le thème du choix, un titre commençant par « Je suis… » et un budget minimal. Cette très courte durée m’a donc donné l’idée d’utiliser le titre non pas comme un élément descriptif mais comme un élément explicatif de l’histoire. Finalement, c’est peut-être un peu obscur…

Écrire un scénario sans le réaliser soi-même a été une expérience nouvelle pour moi et particulièrement instructive. En effet, plus question de combler les lacunes du scénario au moment du tournage voire du montage (cela m’est arrivé de tourner en catastrophe une scène en cours de montage). Au vu du résultat, je note plusieurs points :

* Travailler avec Thomas est un plaisir ! Il a une vision claire du rôle de réalisateur/chef de projet et m’a fait énormément confiance pour le scénario tout en étant très pertinent dans ses remarques.
* Contrôler la durée tout en préservant la compréhension est ce qu’il y a de plus complexe. Ici, à force de raccourcis, il n’est par exemple pas évident que les frais d’hôpitaux du personnage principal sont engendrés par son frère handicapé. Une simple phrase qui, a force d’être retravaillée, a perdu un peu de sa force. Leçon à retenir !
* Le vocabulaire doit être adapté à l’acteur. J’ai visionné la vidéo du casting et je n’ai pas pensé à retravailler le monologue qui ne semble pas du tout naturel pour le personnage tel que l’acteur l’interprète. C’est également une leçon que j’imprime.
* C’est génial de voir quelqu’un d’autre mettre en image les idées que j’ai imaginé. Et d’en ressortir des émotions, des concepts auxquels je n’avais même pas pensé !

Moralité : c’est en forgeant qu’on devient forgeron. Alors que j’ai toujours rêvé d’être acteur, réalisateur et scénariste, je me rends compte que le scénario est l’élément qui m’intéresse le plus et me passionne. J’ai donc une réelle envie de continuer dans cette voie et je suis ouvert aux propositions de collaborations, dans les limites de mon agenda. Appel aux réalisateurs en manque d’idées !

Et bravo Thomas pour ta première réalisation et notre première collaboration. J’espère qu’il y en aura d’autres.
