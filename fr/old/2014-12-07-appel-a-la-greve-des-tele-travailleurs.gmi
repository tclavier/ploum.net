# Appel à la grève des télé-travailleurs !
=> files/old/5102220856_1902ff0200_z.jpg

Camarades télé-travailleurs,

Le syndicat des Télé-Travailleurs en Association Générale Libre (TTAGL) s’associe à la lutte initiée par nos camarades des syndicats classiques et à leurs revendications.

Il a en effet été observé que, nonobstant les piquets de grève organisés partout dans le pays, de nombreux travailleurs renégats rentrent chez eux et, au lieu de ne rien faire, travaillent à distance depuis leur ordinateur personnel.

Rappelons que la grève est l’affaire de tous. Il est nécessaire que nous soyons unis face aux nantis et à leurs sbires du gouvernement.

Dans un soucis de ramener nos camarades égarés dans le droit chemin et de leur faire prendre conscience de l’esclavage à domicile qui leur est imposé, le TTAGL a décidé de prendre une série de mesures.

1. Via des attaques de type “DDOS”, l’accès à tous les sites webs pouvant être utilisés à des fins professionnelles sera rendu inaccessible. En temps normal, ces mesures sont bien entendu illégales mais, un préavis de grève ayant été déposé par le TTAGL, la police tolérera ces agissements et ne prendra aucune plainte. Ces “DDOS” seront filtrants afin de laisser passer les communications urgentes.
2. L’utilisation d’Internet sera scrupuleusement scrutée durant la journée de grève. Le personnel syndiqué des fournisseurs d’accès nous fournira les IPs des personnes ayant eu une activité internet poussée durant la journée de grève. Rappelons que s’informer, lire ou regarder des conférences en vidéo vous rend plus productif et sont donc des activités apportant un bénéfice direct à votre patron. Ces activités sont donc strictement interdites durant les jours de grève.
3. L’utilisation de Facebook n’est tolérée qu’avec les personnes qui n’ont aucun lien professionnel avec vous. Les contacts sociaux entre collègues ou avec des clients sont bénéfiques à votre patron et sont donc à proscrire durant les grèves. Une cellule spéciale du TTAGL a été mise en place afin de surveiller l’activité Facebook.
4. Afin de s’assurer de la coopération de tous, des brouilleurs de wifi circuleront dans les grandes villes dans des véhicules aux couleurs du TTAGL. La police a ordre de ne pas entraver notre liberté constitutionnelle de circuler.

Il est important de souligner que ces mesures sont prises dans l’intérêt général. Par solidarité, interdisez-vous tout ce qui pourrait être bénéfique à votre productivité et, par conséquence, à votre patron durant les journées de grève. Évitez les lectures intelligentes, le repos, la méditation, le sport !

Nous sommes conscients que des complices du grand patronat chercheront à outrepasser ces mesures afin d’égoïstement protéger leurs intérêts au mépris du bien commun. Le TTAGL agira en fonction de la gravité des actes allant griffer voire défoncer le véhicule des coupables.

Camarade, la lutte ne fait que commencer ! Soyons solidaires ! N’oubliez pas que nous combattons pour le bien de tous et pour la défense de vos droits !

Photo par Marcovdz. Relecture par Le Gauchiste.

=> https://www.flickr.com/photos/marcovdz/5102220856 Marcovdz
=> http://gauchiste.fr Le Gauchiste
