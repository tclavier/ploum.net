# Le jour où l’univers boucla, une fois

Tu as déjà entendu l’histoire du sourd ? Non ? Et bien lui non plus !

Et la blague de l’amnésique qui.. qui …

…

…

En tout cas, elle est excellente. Du moins, je pense.

Ou la b-b-b-b-blablag-gue d-d-d-du b-b-b-b-bê-gue…

Mais la meilleure était bien entendu celle du sourd bêgue amnésique. Mais elle est longue à raconter.

Enfin, on rigole, on rigole. Mais j’ai un pote qui est amnésique et bêgue et c’est pas drôle du tout. C’est pas facile à porter comme handicap, déjà qu’il a pas toutes les frites dans le même sachet. Sérieux. À chaque fois qu’il veut dire un mot, il commence mais ne

…

…

Puis il sait plus. Le plus frustrant c’est quand il te raconte une blague.

L’autre jour, il a voulu me raconter l’histoire du sourd bêgue amnésique.

Sérieux, c’est terrible. Et puis, ça aurait pu être dangereux. Si le bêgue de la blague raconte la blague du bêgue à son tour. Qui raconte à son tour la blague. Et tu continues comme ça à trotinette, Claudette. Ce genre de récursion produit des instabilités spatio-temporelles qui peuvent être à la base du repli des dimensions sur elle-même. Pas vraiment la fin du monde mais guêre plus réjouissant sauf si la perspective d’être réduit en soupe de quarks t’enchante. Si ça arrive, relativise, entonne le dernier quantique car l’univers est sur la corde raide[1].

Mais heureusement pour l’univers, mon pote est Parisien. C’est important. Pour une raison que j’ignore, les parisiens aiment les blagues belges. Et rajoutent des belges dans toutes les blagues en prenant l’accent typique « Parisien essayant d’imiter Coluche imitant un Suisse imitant un Belge ». Pourquoi les Parisiens aiment ces blagues ? Ça tu peux pas dire. Nonante-neuf pourcent du monde se le demande fieu. Mais le résultat est là : mon pote a voulu raconter l’histoire du sourd bêgue amnésique et belge.

Du coup, il a raconté la blague du bêgue belge, une fois.

Au lieu de boucler, il l’a bouclé.

Un petit une fois qui fait toute la différence.

Et l’univers est toujours là. Mais pour combien de temps ? Qui nous dis que le monde ne s’écroulera pas à la prochaine tentative de raconter l’histoire du bêgue amnésique hydrocéphale ?

Rien, justement. Alors toi aussi fais un geste pour la sauvegarde de l’univers et apprend à tous tes amis parisiens que les Belges disent tous « une fois » à la fin de leur phrase et que c’est extraordinairement drôle de placer « une fois » dans chaque blague.

Encourage-les, aide-les, ris de leurs blagues ! Notre survie en dépend.

Camarade, ensemble, nous sauverons l’univers une fois !

#### Notes

[1] Théologiquement, la question est intéressante : quelle fût donc la blague débile à l’origine du big bang ?
