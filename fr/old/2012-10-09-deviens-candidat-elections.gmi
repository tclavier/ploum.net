# Deviens une star, sois candidat aux élections

Tu as été blogueur influent puis, sans grand succès, tu as tenté de devenir web-entrepreneur. Mais aujourd’hui, tu t’ennuies. Tes journées sont mornes et personne ne fait attention à toi.

=> /162-toi-aussi-deviens-un-veritable-blogueur-web-20 blogueur influent
=> /deviens-web-entrepreneur web-entrepreneur

Ne t’inquiètes pas, j’ai la solution : deviens candidat aux élections communales !

=> /pourquoi-voter-lionel-dricot candidat aux élections communales

=> ../files/old/colleur_affiche.jpg Colleur d'affiche pirate

Il faut prévoir le coup six ans à l’avance, aux élections précédentes : tu iras coller des affiches pour la tête de liste, tu te feras copain avec les autres candidats. Le parfait militant de base désintéressé. Pendant six années, tu seras de toutes les réunions, de toutes les fêtes de la locale.

Puis, les élections approchant, tu te verras sans doute proposer une place sur la liste. Si ce n’est pas le cas, tente de mettre en épingle ta visibilité sur le web. Oublie juste de dire qu’elle est déclinante.

Finalement, après bien des réunions, c’est officiel : tu es 17ème. Une place de choix !

Être candidat, c’est surtout dépenser du temps et de l’argent pour un hypothétique poste de presse bouton dans un conseil communal. Car, bien évidemment, tu as du signer un papier t’engageant à ne jamais voter contre ton chef de groupe dans le cas peu probable où tu serais élu.

Te voilà en campagne. La campagne, c’est devenir soudainement passionné par les marchés et les brocantes alors que tu n’y mettais jamais les pieds. Il faut dire que tu n’es pas le seul. Même les ténors politiques se prennent de passion pour les légumes bio :
— Mettez-moi ces trois concombres s’il-vous-plait !
— Euh… ce sont des courgettes monsieur le bourgmestre.

Tu vas être partout. Subitement, tu trouves indispensable d’aller écouter cette conférence du cercle du 3ème âge sur l’étymologie des noms de rue dans la commune sous Napoléon. Le conférencier lui-même est surpris par la foule. Près de 50 personnes dont 2 ne sont pas candidats.

De foires aux boudins en fêtes de quartiers, tu vas rencontrer des tas de personnes très intéressées par ton programme et te posant des questions : les candidats des autres listes. Les quelques non-candidats qui osent encore sortir à cette période de l’année t’insulteront en conchiant la politique.

Mais tu gardes le sourire, même s’il se crispe parfois. Quand une personne te parle de ses problèmes, tu promets d’en faire une priorité et que, une fois élu, il ne faudra pas une semaine pour le résoudre, même si cela implique de mettre un bouchon anal à tous les chiens de la ville.

=> /politicien-hypocrite gardes le sourire

Tu fais imprimer ta tête en grand. Chaque week-end, tu fais le tour des panneaux pour vérifier si tu n’as pas été surcollé. Surtout par cet enfoiré de 15ème sur ta liste. À une semaine du scrutin, tu réalises que tu n’as pas écoulé le tiers des affiches qui t’ont coûté un mois de salaire. Du coup, tu retapisses les panneaux.

Ton affiche devient aussi ton avatar Facebook. Car Facebook est partie intégrante de la campagne : tu ne postes plus que des messages expliquant le programme de ta liste ou alors des photos « humoristiques » montrant des tracts de la liste adverse dans une poubelle ou piétinés sur un trottoir. Tu n’aimes et tu ne partages plus que les contenus de tes colistiers.

=> /je-suis-sur-facebook Facebook

Cela a un prix : la grande majorité de tes amis t’ont supprimé ou ignorent tes messages. Ils n’en ont rien à faire de la politique, encore moins de celle de ta commune.

La seule chose que tu ne forwardes pas, c’est l’honteux lipdub que ta tête de liste a tenu à mettre sur Youtube et dans lequel on te voit te déhancher maladroitement d’un air gêné. Bizarrement, tes voisins l’ont retenu celui-là, t’interpellant d’un bruyant « Hey, John Travolta ! » quand ils te croisent.

Tu as réactivé ton défunt compte Twitter et tu t’es créé un « blog de campagne » dont tu twittes chaque billet. Ton Twitter et ton blog sont suivis, en tout et pour tout, par tes colistiers. Du moins ceux qui en ont compris le fonctionnement. Le dernier sur ta liste fait une campagne de feu sur Twitter, twittant chaque rencontre, chaque discussion. Il n’a juste pas compris que son compte était protégé et qu’il n’avait absolument aucun follower.

=> /quel-twitteur-es-tu compte Twitter

Hier soir, le cirque venait en ville. Ton conjoint t’a proposé d’y emmener les enfants. Tu as dit que tu ne pouvais pas car tu participais au débat de l’Amicale des Comités des Quartiers Communautarisés et Unis, l’ACQeCOUCOU. Tu t’es retrouvé au débat avec 89 personnes, dont 88 candidats et un organisateur.

Ton conjoint t’a dit que le spectacle était superbe, que les enfants ont adoré, que la moitié de la ville était là, qu’il avait vu des amis d’un quartier éloigné qui ignorait que tu te présentais et que tu aurais aimé.

Tu as regardé la pile de tracts que tu te promettais de mettre dans les boîtes aux lettres le lendemain. Et, dans un profond soupir, tu t’es rendu compte que tu n’étais plus trop sûr de vouloir être élu.
