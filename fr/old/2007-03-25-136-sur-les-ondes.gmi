# Sur les ondes…

=> ../files/old/loutre.jpg Loutre et ondes
Il y a des années, nous nous prélassions au fil de l’eau. Mais le wifi est dans l’air du temps. Surfons sur la vague, sortons la tête hors de l’eau et laissons-nous bercer par le doux mouvement de l’onde.

=> http://pdphoto.org/PictureDetail.php?mat=pdef&amp;pg=5108 

Retour sur 3 chroniques purefm qui ont la particularité d’avoir été toutes les trois enregistrées via des méthodes différentes, en utilisant des protocoles différents. Mais au fond, c’est quoi un protocole ? Et puis, comme dirait Kristina, c’est quoi ce jargon hermétique que je sors toutes les semaines sur PureFM ?

=> http://pureblog.purefm.be/ PureFM

Le protocole c’est en fait la langue avec laquelle deux appareils vont communiquer. Pour pouvoir échanger des informations, il est évident que les appareils doivent parler la même langue. Un exemple de protocole qui fonctionne très bien est le protocole Global System for Mobile communications, autrement dit GSM (merci Geoffrey pour le lien). Vous le connaissez peut-être 😉 Le protocole GSM est un standard et il est ouvert. Cela signifie que n’importe quel constructeur peut avoir accès aux « spécifications » pour savoir comment construire un GSM. Imaginez un instant que les propriétaires de Nokia ne puissent appeler que les autres Nokia et les propriétaire d’Ericsson ne puissent appeler que Ericcson ? Cela vous semble tellement absurde que l’on doive avoir un GSM de chaque marque pour communiquer avec tout le monde ?

=> /39-jabber-pour-marie-et-les-non-ordinateuriens protocole
=> http://www.time.com/time/2007/treaty_of_rome/7.html Global System for Mobile communications
=> http://www.geofox.org/ Geoffrey

C’est donc en utilisant le protocole GSM que j’ai parlé des licences Creative Commons.

=> http://pureblog.purefm.be/2007/03/le_podcast_et_l_3.html licences Creative Commons

=> ../files/old/vagues.jpg Vagues
Une fois au Mexique, je retrouve un de mes protocoles préférés, à savoir : l’email. De nouveau, l’email est un protocole standard et ouvert. N’importe qui peut écrire un logiciel de mail. Imaginez une seconde que les mails depuis une adresse @purefm.be puisse seulement atteindre une autre adresse @purefm.be, qu’avec mon adresse ploum@fritalk je puisse seulement joindre les personnes ayant un compte sur fritalk.com. Absurde non ? Ça n’a aucun sens !

=> http://pdphoto.org/PictureDetail.php?mat=pdef&amp;pg=5567 

=> /107-the-captive-clown l’email

Mais voilà, le mail est un protocole qui ne supporte pas la voix[1]! On ne peut pas se parler. Je ne peux pas faire ma chronique pour PureBlog. On me répondra : « Skype ». Mais voilà, pour pouvoir parler avec une personne utilisant Skype, il faut obligatoirement utiliser Skype. Le protocole qui permet à deux Skype de communiquer est secret ! On est pas très loin de GSMs non compatibles, vous ne trouvez pas ? Et ce n’est pas le seul problème de Skype. C’est donc avec un bon vieux téléphone que je réalise ma chronique.

=> http://pureblog.purefm.be/2007/03/le_podcast_et_l_5.html ma chronique

Heureusement, il existe des protocoles libres qui permettent de discuter à travers le réseau. Par exemple SIP. Je peux donc utiliser le logiciel que je veux pour discuter, que ce soit Ekiga ou mon préféré, OpenWengo, qui me permet en plus de voir, avec un plaisir non dissimulé, la personne à qui je parle. Cette semaine, j’ai utilisé le logiciel non-libre Gizmo. À noter, quand il s’agit de clavarder[2], Gizmo et OpenWengo utilisent tous les deux un autre protocole libre et standard : Jabber. Vous pouvez donc communiquer avec les personnes utilisant l’un ou l’autre de ces logiciels mais aussi avec vos amis sur Google Talk ou même avec l’informaticien de service qui utilise le logiciel Gajim avec un compte sur le serveur Apinc.org. Contrairement à MSN, qui est, comme Skype, un protocole fermé, Jabber, SIP et les protocoles ouverts en général permettent donc à chacun de choisir son propre logiciel, de choisir son fournisseur de service et de continuer à communiquer avec tout le monde.

=> http://fr.wikipedia.org/wiki/Session_Initiation_Protocol SIP
=> http://ekiga.org/ Ekiga
=> http://www.openwengo.org/ OpenWengo
=> http://www.gizmoproject.com/ Gizmo
=> http://www.jabberfr.org/ Jabber
=> http://www.google.com/talk/ Google Talk
=> http://gajim.org/index.php?lang=fr Gajim
=> http://jabber.apinc.org/ Apinc.org

C’est donc via le protocole libre SIP, tout en chattant sur Jabber avec Damien, que j’ai réalisé ma chronique de ce dimanche.

=> http://www.bloggingthenews.info/ Damien
=> http://pureblog.purefm.be/2007/03/le_podcast_et_l_7.html chronique de ce dimanche

Le thème de ma chronique ? Les protocoles libres bien sûr 😉

#### Notes

[1] en effet, un bon protocole est généralement réservé à un usage bien précis

[2] « chater, en quebecquois. J’aime beaucoup cette expression
