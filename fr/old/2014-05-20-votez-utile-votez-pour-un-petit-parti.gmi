# Votez utile, votez pour un petit parti !
=> files/old/vote1.jpg

Une campagne électorale, finalement, ne s’adresse qu’à une petite partie de la population : les électeurs intéressés et indécis. À un extrême, on trouve ceux qui n’ont que faire de tout ce cirque, qui ne s’intéressent pas à la politique ou qui n’ont pas les outils intellectuels pour le faire. Ils voteront blanc, pas du tout ou sans réfléchir. De l’autre, on trouve les militants, les convaincus de toujours. Ils se pressent en masse aux meetings, ils tapissent leur jardin d’affiches et n’auront pas une seconde d’hésitation en mettant le bulletin dans l’urne.

Tout l’enjeu d’une élection se concentre donc sur une petite frange de la population indécise et, par essence, volatile.

Le mensonge du vote utile

Pour les partis politiques, dont le financement dépend du résultat des urnes, un électorat volatil est catastrophique. Pour fidéliser l’électeur et éviter la tentation d’aller voter ailleurs, l’argument massue reste le « vote utile ». Sous-entendu : voter pour un autre que nous est inutile, surtout si cet autre est un petit parti.

Or, si il y a bien quelque chose d’inutile dans notre système électoral, c’est de voter pour un gros parti. Les habitudes sont tellement ancrées que les sièges sont déjà répartis ! Il y a un an, je lisais un article qui expliquait comment les sièges allaient être répartis entre les 4 grands partis dans ma circonscription électorale. À tel point que se pose encore la question : à quoi bon voter si tout est déjà décidé ?

La réponse est simple : tout n’est pas encore décidé. Cette assurance de façade sert surtout à vous résigner. Car si vous comptez parmi les indécis, vous pouvez encore faire changer les choses en vous tournant vers un « petit parti ».

Petits partis, grandes idées

Un petit parti se centre généralement sur quelques idées maîtresses, quelques concepts. Chaque voix vient donc valider une vision. Même sans élus, un petit parti avec des voix attirera l’attention. Les partis traditionnels se pencheront dessus pour voir s’il n’y a pas des idées à reprendre, histoire de récupérer des électeurs. Si c’est le cas, le petit parti aura accompli sa mission.

Bref, votez pour des idées, pas pour un visage !

Petit parti, nouvelles têtes

Nous n’arrêtons pas d’entendre que voter pour un petit parti, c’est faire le lit de l’extrémisme, c’est permettre à de dangereux nazis pédophiles de devenir parlementaires avec la pluie de sauterelles qui s’ensuivra.

En réalité, le risque est mitigé. Comme l’a démontré de long et en large Laurent Louis, le pouvoir de nuisance d’un parlementaire isolé est relativement limité. Et bien voilà, c’est comme ça qu’on apprend. Pas de soucis, si on ne l’aime pas, il suffit de tenter un autre.

=> /laurent-louis-le-depute-pour-qui-personne-na-vote/ Laurent Louis

Par contre, les petits partis nous offrent parfois d’excellentes surprises. Avant les élections communales de 2012, j’avais rencontré François Schreuer pour un éventuel rapprochement Vega/Pirate. Connaissant François de longue date et étant souvent en désaccord avec son idéologie, je n’étais pas favorable à ce rapprochement. Force est de constater que je me suis trompé : seul élu Véga du conseil communal de Liège, François effectue un travail absolument remarquable de transparence et d’analyse en profondeur des dossiers où le pragmatisme a clairement pris le pas sur l’idéologie.

=> http://francois.schreuer.org/blog/ François Schreuer
=> http://vega.coop/ Vega

Cet exemple illustre à merveille l’intérêt des petits partis : pas de promettre des montagnes qu’ils ne tiendront pas mais bien d’envoyer une taupe au parlement, quelqu’un dont la mission première sera de publier ce qu’il voit, entend. Bref, un pirate, un François Schreuer ou un Snowden dans chaque assemblée.

=> http://artimuses.be/2014/04/une-taupe-au-parlement-wallon/ une taupe au parlement

En Brabant-Wallon, votez Pirate, votez Patrick Installé

Si vous êtes en Brabant-Wallon, la taupe idéale est connue : Patrick Installé, tête de liste Pirate pour le parlement Wallon.

=> http://p-installe.be/ Patrick Installé

Petit bonhomme joyeux et sautillant, Patrick a tout du non-politicien. La barbe et les cheveux en bataille, l’éternel sac d’écolier sur le dos, il ne cherche visiblement pas à séduire. On l’imagine mal faire de grands discours enflammés face à un auditoire. Passionné, débordant d’enthousiasme, il s’emballe plus volontiers au coin d’une table que sur un podium.

Mais Patrick n’est pas là pour devenir un politicien. Son objectif ? Réimpliquer le citoyen dans le processus politique. Bien avant de rejoindre les pirates, Patrick avait déjà lancé Droit de regard, un site qui compile tout les documents politiques sur lesquels Patrick a mis la main, en majorité les ordres du jour et les comptes rendus de conseils communaux. Infatigable, Patrick sillonne le Brabant-Wallon pour assister aux conseils communaux, pour observer, noter, comprendre. Il y a des chances que Patrick connaisse mieux la politique de votre propre commune que vous-même ! Bref, c’est un Patrick qu’il nous faut au parlement !

=> http://www.droitderegard.be/ Droit de regard

Pas de doute, au parlement wallon, ma voix ira à Patrick.

À l’Europe avec Stand Up USE

En Belgique, les pirates ne se présenteront pas à l’Europe faute d’avoir récolté les 5000 signatures nécessaires (et la responsabilité nous en incombe complètement).

Grâce à Nicolas, qui a passé en revue les partis candidats, j’ai découvert Stand Up USE, un mouvement pro-européen qui milite pour la création des États-Unis d’Europe. Une belle idée que je soutiens personnellement vu que c’est une première étape indispensable avant les États-Unis Terriens.

=> http://artimuses.be/2014/05/de-lindigence-de-loffre-electorale-en-brabant-wallon/ passé en revue les partis candidats
=> http://www.standupforuseurope.eu/ Stand Up USE

Stand Up USE, outre un nom de liste abscons et digne d’une distribution Linux, n’est pas un parti. Sophie Heine, tête de liste, a gentiment répondu par mails à mes questions. Sophie se définit personnellement de gauche mais la liste se veut hors de ce clivage. Selon elle, l’urgence de l’intégration européenne devrait dépasser les divergences idéologiques classiques.

=> http://www.youtube.com/watch?v=wDCnU9AyIYg Sophie Heine

Afin de promouvoir sa vision de l’Europe, Stand Up USE a créé un journal en ligne, Le Nouvel Européen, qui raconte notre quotidien dans un futur européen où la Belgique aurait gagné la coupe du monde au Brésil. Autant dire que j’approuve sans réserve le procédé.

=> http://lenouveleuropeen.be/ Le Nouvel Européen
=> http://lenouveleuropeen.be/etats-unis-deurope-top-mundial-bresilien/ la Belgique aurait gagné la coupe du monde au Brésil
=> /tag/futur/ le procédé

Stand Up USE déçoit cependant par sa volonté de « créer de l’emploi ». J’ai signalé à Sophie l’absurdité d’une telle affirmation mais elle tient bon. « Ce n’est pas absurde du tout de mettre la création d’emplois au cœur de son programme, au contraire ! Le chômage est un problème dramatique aujourd’hui en Europe. 25% des jeunes sont actuellement sans emploi en Europe! »

=> /lalienation-du-plein-emploi/ l’absurdité d’une telle affirmation

Paradoxalement, Stand Up USE s’affirme clairement en faveur du revenu de base. Allez, on va dire que ceci compense cela.

=> http://lenouveleuropeen.be/leurope-cree-lallocation-universelle/ en faveur du revenu de base

En conclusion

Personnellement, j’ai décidé de voter utile. J’ai choisi deux petits partis qui recevront ma voix. Je ne suis peut-être pas d’accord à 100%, je les vois mal être au pouvoir. Mais, au moins, je suis certain que mon vote ne sera pas dilué dans une insipide majorité.

En votant pour un petit parti, je sais que je vote utile !

Photo par Paul Bossu. Relecture par Sylvestre.

=> https://www.facebook.com/PirateAndDevil Paul Bossu
=> http://sylvestre.org Sylvestre
