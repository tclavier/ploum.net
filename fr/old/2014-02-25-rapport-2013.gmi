# Rapport 2013

Chers amis,

En 2012, je vous avais fait part de mes gains Flattr (mot de passe: 2012). Voici, avec un peu de retard, mon rapport 2013.

=> /rapport-flattr-2012/ mes gains Flattr

Pour retracer l’historique, j’ai commencé à gagner de l’argent avec mon blog avec 27€ en 2010. Puis 86€ en 2011 et 182€ en 2012, le tout uniquement avec Flattr. Une belle progression. Mais qui n’était rien par rapport à 2013. Dès janvier, mon billet « Et si vous testiez le web payant ? » a un succès phénoménal et le mois de janvier me rapporte 98€. Plus que toute l’année 2011 !

=> /si-vous-testiez-le-web-payant/ Et si vous testiez le web payant ?

Jusqu’en juin, mon blog me rapporte entre 40€ et 100€ par mois rien qu’avec Flattr. Et puis vient mon billet « Ce blog est payant ». Au départ commencé comme un billet générique encourageant à donner aux créateurs que l’on apprécie, je décide, en cours d’écriture, de le rendre moins hypocrite et de le centrer sur moi. Je trouve également amusant de faire un peu peur avec un titre légèrement provocateur. Le succès est foudroyant. 171€ en juillet. Désormais, un mois sur Flattr me rapporte entre 150€ et 220€.

=> /ce-blog-est-payant/ Ce blog est payant

Cela, sans compter les autres paiements : paypal, principalement (entre 1 et 2 dons par mois pour un total de 15/20€ mensuel, en moyenne) mais également virement bancaires, bitcoins et même un léger appoint sur Patreon (même si Patreon reste très cher en termes de frais). Vous avez également participé à mon NaNoWriMo à hauteur de 350€ !

=> /la-liberte-de-soutenir/ sans compter les autres paiements
=> /no-proprietary-service.html sur Patreon
=> /9-ans-de-blog-le-defi-nanowrimo/ NaNoWriMo

Je reste absolument abasourdi par le soutien de mes lecteurs. Merci ! Cela m’a fait beaucoup réfléchir et cela m’a donné des idées. Je ne suis pas exceptionnel. Vous ne m’envoyez pas de l’argent pour mes beaux yeux. 200€ par mois c’est à la fois pas grand chose et à la fois énorme. Il « suffirait » de multiplier cela par 10 pour vivre confortablement (bon, je ne compte pas les taxes). Il y a une évolution de l’échange économique et je souhaite arriver à partager autant que possible mon expérience avec d’autres créateurs. Je ne vous en dis pas plus pour le moment, j’y travaille. Si vous lisez ceci, c’est que vous êtes responsable de toutes ces idées qui me passent par la tête pour le moment 😉

Bilan 2013

Je suis désolé mais j’ai horreur de la comptabilité. Je ne ferai donc pas de bilan pour les paiements autres que Flattr. D’ailleurs, pour respecter la vie privée des donateurs, je ne garde pas les adresses emails ni les noms (je suis même incapable de contacter ceux qui font des virements bancaires. Si vous en êtes, soyez ici remerciés !).

Pour Flattr, le bilan est par contre plus simple : en 2013, j’ai gagné 1539€ dont 1380€ via mon blog. Le 11 février 2013, je fêtais mon 999ème Flattr et je me fixais les objectifs suivants :

1. Que mon blog devienne mon « objet » le plus flattré, devant GTG. Accompli haut la main, mon blog compte à présent 301 flattrs !

=> https://flattr.com/thing/76498/Where-is-Ploum 301 flattrs

2. Gagner plus d’1€ par jour avec Flattr. Accompli haut la main, j’ai gagné en 2013 4,2€ par jour.

3. Je voulais recevoir des flattrs de 500 personnes différentes. Accompli, je suis à 677.

4. Je voulais que de la fiction me rapporte via Flattr. La fiction reste le parent pauvre de mes gains Flattr avec une exception : C’est la vie, mon recueil de nouvelles, est mon second billet le plus rentable de 2013 avec 48 clics et 44€.

=> /cest-la-vie/ C’est la vie

Dans le top de 2013, on trouve donc :

Ce blog est payant (76 clics, 55€) suivi par C’est la vie (48 clics, 44€) et Et si vous testiez le web payant ? (60 clics, 43€). Après le podium, La première guerre civile mondiale (41 clics, 41€), Pourquoi vous êtes favorable au revenu de base (39 clics, 37€) et l’étonnant Nous sommes tous d’extrême droite (24 clics, 29€) qui a pourtant été fort critiqué. Complétons ce top 10 avec l’aliénation du plein emploi (26 clics, 29€), The Cost of Being Convinced (25 clics, 28€), Paying for the Web (39 clics, 27€) et la révolte des rebelles apatrides (32 clics, 23€).

=> /ce-blog-est-payant/ Ce blog est payant
=> /cest-la-vie/ C’est la vie
=> /si-vous-testiez-le-web-payant/ Et si vous testiez le web payant ?
=> /la-premiere-guerre-civile-mondiale/ La première guerre civile mondiale
=> /pourquoi-vous-etes-sans-le-savoir-favorable-au-revenu-de-base/ Pourquoi vous êtes favorable au revenu de base
=> /nous-sommes-tous-dextreme-droite/ Nous sommes tous d’extrême droite
=> /lalienation-du-plein-emploi/ l’aliénation du plein emploi
=> /the-cost-of-being-convinced/ The Cost of Being Convinced
=> /paying-web/ Paying for the Web
=> /la-revolte-des-rebelles-apatrides/ la révolte des rebelles apatrides

Le seul billet d’avant 2013 a rivaliser avec ce top 10 est Why I’m a Pirate (60 clics, 27€).

=> /im-a-pirate/ Why I’m a Pirate

De manière générale, on constate que mes réflexions sur la société sont, de loin, mes billets les plus rentables. La série des Printeurs, par exemple, est extrêmement peu rentable, gagnant entre 2€ et 4€ par épisode. Exceptions : Printeurs 2 (21 clics, 21€) et Printeurs 5 (15 clics, 16€). Mes « Lettres du futur » sont également assez peu populaires, la première étant Récit de voyage (23 clics, 18€). Quand aux nouvelles plus construites, la plus rentable reste Les filons chocolatifères de la Lune (22 clics, 13€). Pourtant, ce sont les textes que je préfère écrire. Mmm, c’est déprimant les bilans au fond…

=> /printeurs-1/ Printeurs
=> /printeurs-2/ Printeurs 2
=> /printeurs-5/ Printeurs 5
=> /recit-de-voyage/ Récit de voyage
=> /les-filons-chocolatiferes-de-la-lune/ Les filons chocolatifères de la Lune

Il est amusant de remarquer que la rentabilité n’est absolument pas corrélée au buzz dans les médias et les réseaux sociaux. Faire le buzz n’est donc, pour moi, pas rentable. C’est admirable de réaliser qu’écrire des choses intéressantes, fouillées, construites me rapporte plus que faire le buzz. Et ça, c’est grâce à vous ! Merci !

Utilisation de l’argent

Concrêtement, à quoi me sert cet argent ? Et bien, la réponse est très simple : je ne l’ai pas dépensé (sauf pour Flattrer d’autres, à hauteur de 1% de la somme de mes revenus). Je l’ai gardé et, maintenant que je suis sans emploi, je compte l’utiliser pour me payer un mois de salaire à travailler à un projet qui me tient très fort à cœur. Ce projet ? C’est justement de rendre possible à chacun de payer librement du contenu sur le web. J’espère vous en reparler très bientôt. Et si vous souhaitez soutenir mon effort, c’est très facile. Il suffit de m’envoyer un paiement ! (Mais je comprends que vous souhaiteriez en savoir plus avant de vous engager)

=> /objectif-1/ 1% de la somme de mes revenus

Encore merci à tous pour votre soutien en 2013 et j’espère que 2014 va voir se généraliser cette nouvelle économie de l’information et de la culture libre.
