# Pourris-moi la vie, oh oui, encore !

Je suis nominé pour le concours Persoweb, n’oubliez pas de voter pour moi, plus d’infos à la fin de ce billet.

=> http://www.persoweb.be/page/persoweb-2007-blog-nomine/12.aspx?blogid=79&amp;blog_action=vote voter pour moi

=> ../files/old/tincan.jpg Virons les téléphones
Tous les téléphones portables modernes disposent d’une merveilleuse fonction cachée et non désactivable, la fonction « Je te pourris aléatoirement une bonne nuit de sommeil ».

Le principe de cette fonction est simple : répéter durant toute la nuit et toutes les 5 minutes un petit signal sonore trop faible pour vous réveiller complètement mais assez fort pour vous sortir du sommeil et vous laisser le temps de penser « Faut vraiment que je balance cette horreur par la fenêtre » avant de vous rendormir profondément.

Certains prétendent qu’il s’agit uniquement de la fonction censée vous avertir que la batterie est faible. Effectivement, ce signal ne se présentent que lorsque la batterie est presque morte.

Pourtant, tout le reste mis de côté, il y a une chance sur 2 que ce signal se produise pendant la nuit. Ajoutez à cela que lorsqu’on utilise son portable ou bien qu’on attend un appel, on remarque très vite le symbole batterie faible et on prend les mesures qui s’imposent. La probabilité que ce signal se produise durant une nuit où vous ne voulez pas vous préoccuper du téléphone est donc de 90%.

Si vraiment il était nécessaire de prévenir que la batterie allait rendre l’âme, un seul signal suffirait. Cependant, ce signal se produit toutes les 5 minutes jusqu’à épuisement complet de la foutue batterie, laquelle tiens généralement toute la bon dieu de merde de nuit.

Étant donné que lorsqu’on est réveillé en pleine nuit par ce satané signal de couille, on prend rarement le temps de trouver l’enfoiré de chargeur, on se contente d’éteindre la machine infernale en maudissant jusqu’à la troisième génération les salopards d’ingénieurs responsables de cette fonction à la mords-moi le noeud, on pourrait très bien imaginer que ce foutu téléphone s’éteigne de lui-même en constatant que cette batterie de crotte est faible.

La preuve est donc faite que le but de cette fonction maudite est bien uniquement de vous pourrir la vie, de vous rendre désagréable et ronchon le lendemain et de faire se bidonner quelques ingénieurs qui vous imaginent, à raison, vous lever finalement au douzième signal vers 4h du mat pour errer comme un zombie dans le noir de votre chambre à la recherche de cet enfoiré de flûte de crotte de téléphone de mes deux qui, bien entendu, ne fait plus aucun son et ne peut donc être localisé facilement.

En arrivant le lendemain au travail, le visage blafard, les yeux hagards, vous vous vengerez en étranglant jouissivement le premier collègue qui vous dira : « Dis donc, t’as l’air un peu à plat ce matin ! »

Voilà, ça n’a rien à voir avec le billet mais j’ai eu la surprise d’être nominé au concours Persoweb dans la catégorie IT/Techno. J’ai peut-être la chance de gagner un voyage surprise mais je suis opposé à quelques pointures du domaine : Korben, Kaosproject, Narcissique et Trendsnow.

=> http://www.persoweb.be Persoweb
=> http://www.korben.info/ Korben
=> http://www.kaosproject.be/blog/index.php Kaosproject
=> http://blog.narcissique.fr/ Narcissique
=> http://www.trendsnow.net/ Trendsnow

La seule chance que j’ai de gagner face à ces géants, c’est grâce à vos votes (vous pouvez voter une fois par jour et par IP) et aux votes de vos amis et des amis de vos amis ! Si je gagne, je promets d’aller chercher mon prix avec un t-shirt Ubuntu, pour la photo officielle 🙂

=> http://www.persoweb.be/page/persoweb-2007-blog-nomine/12.aspx?blogid=79&amp;blog_action=vote vos votes
=> http://ubuntu-fr.org Ubuntu

Merci d’avance pour votre soutien. Je risque encore de vous embêter jusqu’au 7 septembre, date de clôture des votes. Si vous voulez pas que je vous embête avec ce concours, c’est très simple : offrez-moi un voyage vers la Silicon Valley. (ben quoi, on peut essayer)
