# Ploum n’est pas mort, il bande encore !

Si il y a bien une chose qui m’insupporte dans la « blogosphère », c’est ce concept de méta-post, ces fameux billets où l’auteur du blog parle de son blog, arrivant parfois à la situation particulièrement amusante du blog dont le sujet est lui-même.

=> /162-toi-aussi-deviens-un-veritable-blogueur-web-20 blog parle de son blog

Mais bon, peut-être est-ce parfois nécessaire. Je m’excuse donc par avance auprès de mes lecteurs (s’il m’en reste quelqu’uns).

=> ../files/old/happyjump.jpg Happy Jumping by Ploum
Youpie, Ploum réouvre son blog !

Le blog, c’est comme le sport : une véritable drogue quand on le pratique régulièrement mais difficile à (re)commencer une fois sevré.

Il y a un an, j’ai pris la résolution de migrer mon vieux bouzin crasseux vers dotclear 2. Cette migration a toujours été bloquée par un point noir : mon incapacité notoire à faire un thème digne de ce nom. Du coup, ne postant plus sur mon vieux blog mais n’ayant pas encore de nouveau, je me retrouvais sbf, sans-blog-fixe.

=> http://fr.dotclear.org/ dotclear 2
=> /230-amis-infographistes-experimentons-le-troc-virtuel mon incapacité notoire à faire un thème digne de ce nom

Avouons-le : le microblog (sur identi.ca ou sur twitter ) ne sont pas étranger à ma faible production bloguesque. Mais, comme vous le savez, je me complais dans des phrases longues, alambiquées, ampoulées et prétentieuses, style peu compatible avec la limite de 114 caractères. Bloguer me manquait terriblement !

=> http://identi.ca/ploum identi.ca
=> /no-proprietary-service.html twitter

Et puis, je me sentais coupable vis-à-vis des quelques lecteurs qui m’ont demandé de mes nouvelles. C’est vrai, le plus grand plaisir d’un blog c’est de savoir que, quelque part, au moins une personne vous lit. Cela donne le sentiment d’exister et justifie bien des efforts.

Bref, j’ai décidé de réouvrir ce blog, malgré un thème moche et plein d’imperfections. Qu’à cela ne tienne, vous me lisez sans doute dans votre lecteur de flux !

Ça sent la peinture fraîche, j’arrondirai les angles au fil des mois mais, au moins, je pourrais continuer à abbreuver la toile de mon ineffable pédanterie et mon ennuyeuse prétention littéraire parsemée de fautes d’orthographe !

Merci à tous d’avoir attendu et en route pour de nouvelles aventures !
