# [Oneliner 18]

Un ingénieur est une personne capable de résoudre tous vos problèmes. Et si vous n’avez pas de problème, ne vous inquiétez pas, l’ingénieur arrangera ça aussi.
