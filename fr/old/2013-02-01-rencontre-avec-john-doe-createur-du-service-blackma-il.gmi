# Rencontre avec John Doe, créateur du service BlackMa.il
=> files/old/silent_angel.jpg

=> /meet-john-doe-founder-blackma-il Available in English

Si BlackMa.il fut le buzz de l’automne 2014, la startup n’en reste pas moins très discrète. Un simple panneau au nom de CrowIT sur une boîte au lettres, un couloir, un ascenseur et nous voilà dans l’antre. Ici, nulles balles multicolores jonchant le sol, nuls accessoires : quelques bureaux et une dizaine d’employés devant leurs écrans qui ne se retournent même pas à mon passage.

Avec une ironie contenue, le bureau de la direction annonce « John Doe ». Il est vrai que le créateur de cette entreprise londonienne préfère garder l’anonymat. Ce fut d’ailleurs la condition sine qua non à notre entrevue.

L’homme est grand et a le crâne dégarni. Jovial, il se lève de son fauteuil pour me saluer. Il est simple et me met tout de suite à l’aise tandis que je sors mon dictaphone. J’ai l’impression d’être face à un sympathique grand-père patron d’une petite épicerie familiale.

Bonjour Mr Doe. Merci de me recevoir. Je pense que vous n’accordez pas beaucoup d’interviews.

Pour tout vous avouer, c’est même la première fois qu’un journaliste pénètre dans nos bureaux. Mais je suppose que c’est la rançon du succès…

Le succès, parlons-en. Après seulement six mois d’existence, BlackMa.il affiche déjà plusieurs dizaines de millions d’euros de bénéfice. Et cela avec un investissement sur vos fonds propres. Comment expliquez-vous ce succès ?

Contrairement à la majorité des sociétés de l’ère du web social, je n’ai pas essayé de faire un service à la mode ni d’attirer des millions d’utilisateurs. Je me suis juste posé la question : pour quoi les gens seraient-ils prêts à payer ?

La réponse m’est apparue clairement en cette période où chacun voit son intimité dévoilée sur Google ou sur Facebook. Avant, on payait pour diffuser de l’information. De nos jours cette information est publique par défaut. Le corollaire est évident : on paiera pour garder des informations confidentielles.

Et quelle est la solution pratiquement mise en œuvre par BlackMa.il ?

Plutôt que de partir à la chasse aux informations, nous avons préféré laisser cela à nos utilisateurs et partager nos bénéfices avec eux à hauteur de 50%. Ce sont nos utilisateurs qui trouvent les informations cachées et nous les proposent.

Mais qui paie dans ce cas ?

Ceux qui souhaitent garder une information secrète. Prenons un exemple : je suis témoin du fait que vous trompez votre mari. Je prend une photo avec mon téléphone lors d’un de vos rendez-vous galant. J’envoie cette photo sur BlackMa.il avec la somme que j’exige, disons 1000€ et l’adresse email de votre mari.

À partir de ce moment, vous avez un mois pour réagir. Si au bout d’un mois BlackMa.il n’a pas de réaction de votre part, la photo et le texte original est envoyé à votre mari. Nous l’appelons le destinataire dans notre jargon. Si vous payez 1000€, l’affaire est classée et la photo n’est pas envoyée. Vous pouvez également acheter du temps de réflexion à hauteur de 10% du prix par mois. Dans notre cas, chaque tranche de 100€ vous achète un mois. Au bout d’un an, nous considérons l’affaire comme conclue. Vous ne payez donc que 2 mois supplémentaire pour étaler le paiement.

50% de la somme va directement à notre informateur, que vous ne connaîtrez jamais. Nous prenons même le soin de cacher toutes les informations cachées dans les photos ou les vidéos.

N’est-ce pas illégal ?

Cela dépend fortement des pays et de l’affaire traitée. C’est pourquoi la société qui gère le business de BlackMa.il est installée sous des cieux plus cléments. Les bureaux dans lequel vous vous trouvez sont ceux de CrowIT, la société de consultance qui fournit l’expertise informatique à BlackMa.il.

D’ailleurs, nous n’avons pas encore eu la moindre menace de procès.

Comment expliquez-vous cela ? Cela ne doit pourtant pas être de gaieté de cœur que les bourses se délient !

Tout d’abord, les gens préfèrent éviter autant que possible le scandale. BlackMa.il ne fait que l’intermédiaire. Si vous refusez de payer, BlackMa.il n’aura fait que transmettre un mail de manière entièrement automatique avec un délai d’un mois. C’est très difficile à attaquer, surtout étant donné le caractère international. Nous avons mis tout cela en place avec l’aide de plusieurs experts.

Enfin, nous offrons des garanties. Nous expliquons qu’en cas de refus nous ne sommes plus responsable de rien. Par contre, nous avons des engagements : nous n’acceptons pas de faire payer la même personne plusieurs fois pour la même information. Une fois que vous avez payé 1000€ pour votre liaison extra-conjugale, si vous recevez une nouvelle demande, vous pouvez la réfuter en nous donnant le numéro du dossier précédent et nous annulerons la transaction.

Bien que 100€ soit le paiement minimal, nous encourageons les informateurs à faire des demandes raisonnables qui ont plus de chance de recevoir une réponse positive.

Nous essayons d’être un partenaire, un intermédiaire qui ne prend pas parti.

Qu’est-ce qui empêche l’informateur de réclamer une seconde fois de l’argent mais sans passer par Blackma.il cette fois ?

En effet. Mais dans ce cas, pourquoi utiliser Blackma.il la première fois ? Blackma.il offre un risque nul d’être identifié et facilite le paiement. De plus, nous avons acquis une solide réputation : nous nous engageons à effacer toute information relative à l’informateur à la clôture de chaque affaire. De cette manière, il devient impossible de retracer l’identité d’un auteur.

Il y a un mois, un concurrent meilleur marché est apparu en Russie. Il s’est avéré qu’après la clôture de chaque affaire, les informateurs de ce site se voyaient menacés de voir leur identité révélée si il ne payait pas une somme régulière.

Je sais que Blackma.il est souvent mal perçu mais nous avons une éthique. C’est ce qui nous différencie.

Justement, comment justifiez-vous la moralité d’une telle entreprise ?

Nos vies sont de plus en plus transparentes. Nous cachons certaines facettes de notre personnalité pour de simples conventions morales dépassées. Pourquoi une femme comme vous ne pourrait-elle pas avoir plusieurs amants ? La contraception a pourtant rendu obsolète la nécessité de la monogamie.

D’une certaine manière, BlackMa.il tente d’encourager les gens à s’assumer. Nous avons par exemple beaucoup d’affaires où l’informateur prétend rendre publique l’homosexualité du sujet. Et bien près de la moitié restent lettre morte. La menace financière pousse ces personnes à faire leur coming out, à assumer leur homosexualité. Je pense que c’est une très bonne chose.

Dans certains pays, l’adultère et l’homosexualité sont encore des délits. Ne prenez-vous pas de mesures d’exception ?

Forcer les gens à cacher leur homosexualité est de l’homophobie. C’est cela qui doit être dénoncé. En reportant une partie de la culpabilité sur le messager, on justifie l’homophobie et le rejet. Chacun doit être libre de dire qu’un autre est homosexuel comme on dit qu’il a les cheveux blonds.

Il ne doit pas y avoir que des affaires d’homosexualité.

Plus de 80% de nos affaires sont liées au sexe : infidélité, consommation de pornographie, homosexualité, enfants illégitimes, fréquentation de prostituées. La réalité est que le sexe est resté un énorme tabou.

Sur Google Images, vous pouvez trouver des images de morts, des images à caractère violent ou raciste, des armes, des horreurs sans nom. Mais, par contre, les images de corps nus sont filtrées par défaut. La mort, oui. La violence, le rejet, oui. Le sexe, non !

De manière indirecte, BlackMa.il force les gens à assumer leur sexualité. Consulter un site pornographique ne fait pas de vous un monstre mais c’est pourtant l’image que nous en avons. Jusqu’au jour où nous réalisons que 75% des adultes consomment de la pornographie.

Comment justifiez-vous les affaires à caractère non-sexuel ?

Je le répète, le sexe est notre principal fournisseur. Le second sujet récurrent est l’argent. Tout particulièrement au sein des entreprises. Un employé qui a compris que son patron détournait une partie du budget. Ou une personne qui a compris qu’une de ses connaissances a été arnaquée et qui veut sa part.

Ces affaires sont nettement moins nombreuses mais elles sont beaucoup plus lucratives. Les sommes moyennes sont multipliées par mille.

Pouvez-vous nous donner un exemple ?

Je ne souhaite pas en dire trop. Mais sachez que si quelqu’un détourne beaucoup d’argent, 10% est le plus rentable. Au dessus, le destinataire hésite à payer. Or il n’est pas rare, dans certaines sphères, d’être témoin du détournement de plusieurs millions voire dizaines de millions. Il suffit de menacer d’envoyer l’information à un journaliste.

Ici BlackMa.il joue un rôle moral comparable à WikiLeaks : il décourage les actions malhonnêtes plus sûrement que la menace d’un procès en préservant l’anonymat de l’informateur.

Et BlackMa.il prend 50% sur ces sommes ?

Le fait que nous soyons fortement intéressé assure que nous souhaitons avant tout la réussite de l’affaire. Nous éduquons nos informateurs en ce sens. Et c’est pourquoi nous autorisons l’étalement du paiement. Une affaire non-payée est une perte sèche pour BlackMa.il. Ce n’est pas dans notre intérêt.

Je préfère dire qu’il s’agit d’un partenariat équitable avec l’informateur, surtout lorsqu’on considère que tous les frais sont à notre charge.

N’avez-vous jamais de demandes de négociation ? De cas particuliers ?

C’est une des grandes forces de BlackMa.il : le destinataire ne peut pas nous contacter autrement que par un formulaire. Il peut juste répondre qu’il n’accepte pas de payer, qu’il paie (par mois ou au forfait) ou demander d’assimiler cette affaire à une autre précédemment payée. Nous ne rentrons pas dans le dialogue. D’ailleurs, le processus est entièrement automatisé. Le sujet le sait et a un mois pour prendre sa décision.

Éprouvez-vous parfois des remords ?

La grande majorité des affaires que nous traitons sont en fait déjà publiques. Ainsi, la discussion en ligne de ce mari infidèle avec son amante peut être trouvée sur Google dans les archives d’un forum. Cet homme à qui on menace de révéler sa fréquentation d’un salon de strip-tease est déjà fan dudit salon sur Facebook. Pourtant, il paie pour que le mail ne soit pas envoyé à sa femme.

Nous entrons dans une société transparente. Nous pouvons soit faire les hypocrites et cacher nos différences. Ou bien s’accepter comme nous sommes et accepter les autres.

Que faîtes-vous du droit à la vie privée ?

Mais nous le respectons tout à fait. Nous ne rendons pas publiques les affaires que nous recevons. L’information n’est jamais envoyée qu’à un ou plusieurs destinataires. La vie privée c’est pour nous comme aller aux toilettes : tout le monde le fait et pourtant nous n’apprécions pas le faire en public. Cependant, si j’envoyais une photo de vous sur le cabinet à votre mari, il ne s’en offusquerait pas. Cela n’aurait aucun intérêt. Sauf si vous avez toujours prétendu à votre mari que vous n’alliez jamais à la toilette. Mais BlackMa.il n’aura rien rendu de public.

Un mot pour conclure ?

BlackMa.il est fortement décrié. Publiquement, tout le monde s’insurge et le voue aux gémonies. Pourtant nous avons des centaines de milliers d’utilisateurs. Parmi lesquels certains de nos plus ardents détracteurs.

Mais BlackMa.il aura sur la morale mondiale le même effet d’électrochoc que Wikileaks a eu sur le monde politique. L’homme s’assumera et arrêtera de porter un jugement sur les autres. Les truands craindront de s’associer de peur de devoir s’acheter le silence l’un à l’autre. BlackMa.il a donc réellement un rôle positif et pacificateur.

Merci pour cet entretien Mr Doe.

Tout le plaisir était pour moi.

Alors qu’il me raccompagne jusqu’à l’ascenseur, une idée me vient soudainement.

Que se passerait-il si je menaçais de publier votre vrai nom. Quelle somme seriez-vous disposé à payer ?

Cette affaire a déjà été traitée. Il vous faudra trouver autre chose. Mais je suis heureux de voir en vous une partenaire potentielle. Vous avez bien compris tout le potentiel de BlackMa.il.

D’un délicat mouvement du bras, il m’aide à monter dans l’ascenseur. Les portes d’acier se referment sur son sourire tandis qu’il me fait un geste de la main. Machinalement, je lui rend son sourire, comme à un vieil ami.

Photo par Marynka Egremy

=> http://www.flickr.com/photos/78745458@N00/1394756814 Marynka Egremy

=> /meet-john-doe-founder-blackma-il Available in English

Mise-à-jour du 5 février : ajout d’un paragraphe sur la concurrence
