# La vraie vie, c’est so 20ème siècle

=> ../files/old/maingsm1.jpg Et encore bonne année
Alors ? Vous avez passé de bonnes fêtes ? Vous avez passé du temps avec des amis ? Vous avez bien rigolé ? Mon dieu, mais vous êtes complètement 20ème vous alors ! C’est à dessein cet enthousiasme rétro ? Notez que ce côté has-been peut être touchant hein, j’ai pas dit le contraire. Mais la vraie vie, c’est complètement out. Fini. Atomisé. Ringardisé avec les chemises à fleurs et les coiffures Jackson Five.

=> /tag/noel fêtes

Sérieux, je ne déconne pas. Aujourd’hui, les seuls vrais amis sont ceux qui sont loin, les seuls vrais contacts sont listés électroniquement. L’important n’est plus de faire mais de faire savoir.

=> ../files/old/maingsm2.jpg Et bonne santé
Quoi ? Pourtant c’est vrai ! Vous êtes au restaurant avec une personne charmante et une vague connaissance vous appelle sur votre GSM. Des deux, qui allez vous interrompre et qui va devenir prioritaire dans vos conversations ? La vague connaissance bien entendu car elle utilise un medium électronique elle ! Et votre partenaire de restaurant ? Il va en profiter pour envoyer des SMS. Pour dire qu’il est au restaurant. D’ailleurs, votre conversation à table ne devait pas être si importante : vous vous êtes interrompu quand le serveur est venu, vous vous êtes interrompu pour aller vous laver les mains mais jamais, au grand jamais, vous n’oseriez interrompre votre conversation au GSM ni même le couper.

Tu es au cinéma, pris dans un film passionnant, ton conjoint dans tes bras. Un type te demande de sortir 5 minutes pour discuter : tu l’envoies au Caire, bien entendu. Sauf s’il te le demande par GSM. Magique non ?

Vous vous souvenez de ce que vous avez fait pour le nouvel an ? Et à la minute près du passage de l’année ? Il y a fort à parier que vous étiez en train d’envoyer vos voeux à vos amis par SMS, nonobstant les personnes qui étaient dans la même pièce que vous. Mais bon, d’un autre côté, elles-mêmes étaient en train d’envoyer des SMS pour remercier des bons voeux reçus. Finalement, on aurait pu faire le nouvel an tous dans notre pieu, vous ne trouvez pas ?

=> ../files/old/swaree.jpg Super Swarée
Vous voyez que finalement vous n’êtes pas si has-been. Et ne me dîtes pas que vous sortez pour vous amuser. Au contraire, sortir doit être une corvée, une obligation, un réflexe hebdomadaire. L’intérêt ? Mais voyons, il est évident : vous faire prendre en photo tout sourire avec des gens qui ont l’air super cool. Ensuite, publiez vos photos sur n’importe quel site « social » pour bien montrer à vos contacts à quel point vous avez une vie passionnante, trépidante. D’ailleurs les photographes de soirées se sont fait une spécialité : donner l’impression d’une ambiance de malade, l’éclate totale, quand bien même vous avez pesté toute la nuit contre des remix RnB merdiques.

=> /126-vocabulaire-sardines sortez

Pour bien enfoncer le clou, pour affirmer haut et fort que vous avez une vie sociale de malade, arrangez-vous pour avoir le plus de contacts possibles sur les sites sociaux sus-cités. En pratique, cela revient juste à ajouter à votre liste absolument toutes vos obscures connaissances, y compris des gens que vous n’avez jamais vus ou en tout cas plus depuis l’école maternelle. Mais avouez qu’un profil avec 450 contacts et remplit de photos de soirées où vous vous éclatez avec des tas de gens ça vous pose de suite un homme.

=> ../files/old/sefaitchier.jpg Faut se faire chier
Le problème de la vraie vie, la vie 1.0, c’est qu’on n’est pas immédiatement au courant si un ancien copain de classe va passer un week-end à la mer en famille. On ne sait pas immédiatement qui est online ou offline. Il faut vivre avec son temps, vivre c’est dépassé. Bon, d’accord, mais je me comprends. Je veux dire : si vous ne savez pas prendre des photos cool ou au moins envoyer un petit mot sur Twitter, alors ne le faites pas ! Les souvenirs du cerveau, c’est encore un truc de grand-pères.

La vie 2.0 mon ami, ça ne se vit qu’au travers d’un GSM ou d’un appareil photo. Pourquoi croyez-vous que les deux sont de plus en plus fusionnés alors que, conceptuellement, l’idée semble absurde ? Les yeux, les oreilles… Mouais, ça fera éventuellement un come-back rétro d’ici quelques années.

Du moins si on peut les brancher sur USB.
