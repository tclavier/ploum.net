# Printeurs 51, la fin.
=> files/old/grant-durr-1100334-unsplash.jpg

Ceci est le dernier épisode d’une aventure qui ce sera étalée sur plusieurs années. J’espère que vous avez apprécier la lecture, que vous la conseillerez à d’autres et que j’aurai l’occasion de la mettre en forme pour vous proposer un véritable livre, électronique ou sur papier. Merci pour votre fidélité à travers cette histoire !

Ce matin, le plus vieux est venu me chercher. Les bébés étaient calmes. J’étais confiant, mon pouvoir était revenu.

— Viens avec moi ! m’a dit le plus vieux. Je veux que tu racontes ton histoire à Mérissa. Je me refuse de croire qu’elle soit inhumaine à ce point là. Une future maman ne peut rester insensible, elle va comprendre, elle va agir.

Je n’ai rien dit, je l’ai suivi silencieusement à travers la ville jusque dans cette grande pièce avec une engrossée. Lorsque le plus jeune a soudainement surgit, avec une jeune femme nue, je me suis doucement mis en retrait. Je sais que mon pouvoir me permet de ne pas être remarqué, de ne pas attirer l’attention sur moi.

Ils ont parlé pendant une éternité. Mais j’ai appris la patience. Je les ai laissé. J’avais confiance. Le pouvoir me soufflerait lorsque serait venu le temps d’agir.

L’enfer s’est soudainement déchainé. Mes cauchemars sont devenus une nouvelle forme de réalité.

J’ai souris.

Elle était là, familière, présente, suintante. La peur ! Ma peur.

Sans forcer, sans colère, j’ai enfoncé la fine baguette de métal dans le dos du plus jeune. Puis du plus vieux. Une simple tige que j’avais arraché à un meuble de l’appartement dans lequel j’avais séjourné et que j’avais caché dans ma manche.

Les bébés hurlaient, dansaient mais cette fois, ce n’est pas moi qu’ils regardaient. L’engrossée regardait un écran et tentait de taper sur un clavier. La plus jeune la soutenait. Je lui ai enfoncé la baguette dans le cou.

Elle a porté ses mains à sa gorge avant de tourner vers moi un regard de surprise extrême. Ses lèvres ont articulé quelques mots.
— L’élément perturbateur, l’imprévu…
Elle s’est écroulée, renversant l’engrossée qui est tombée sur le sol en hurlant.

Je me suis approchée d’elle.

Elle gémissait, tentant de s’apaiser avec des petites respirations saccadées. J’avais déjà vu des travailleuse mettre bas, cela ne me faisait ni chaud ni froid.

D’un geste du doigt, elle me fit signe de me rapprocher. J’obtempérai.
— Comment… Comment t’appelles-tu ? haleta-t-elle.
— 689, répondis-je machinalement.

Malgré sa difficile situation, elle suintait l’autorité. Le pouvoir semblait littéralement jaillir de sa voix, de son visage. Je l’adorais, la vénérais.
— 689, murmura-t-elle, si tu appuies sur la plus grosse touche du clavier, tu détruiras le maitre du monde. La commande est tapée, il suffit de la confirmer.

Le pouvoir. L’immense pouvoir.

Lentement, je me redressai tout en contemplant le clavier, l’écran.

J’ai trouvé la touche. J’ai vu l’écran. J’ai levé le doigt. J’ai hésité.

Puis j’ai regardé la femme en train de hurler tout en se tenant le ventre. Une petite tête humide et visqueuse pointait entre ses cuisses. Les cris de la mère couvraient le cauchemar de la pièce.

— Appuie ! cria-t-elle. Appuie maintenant !

612 se tenait devant moi, le visage tordu par la douleur et le coup mais le regard pétillant de malice.

— L’un d’entre vous verra la Terre. Il la sauvera. L’Élu ! Appuie !

Ma vie se mit à défiler devant mes yeux. La douleur, l’humiliation, l’usine. Devenir G89. Tuer le vieux. Approcher le contremaître. Voir l’espace. La terre. Gagner la confiance du vieux et du jeune terrien. Tuer le jeune terrien qui était un peu trop perspicace. Rester caché dans l’appartement. Affronter mes cauchemars. Être témoin de la résurrection du plus jeune. Et puis devenir le maître du monde ?

— Accomplis ton destin ! m’ordonna le vieux 612. Appuie sur le clavier, sauve la Terre !

Au sol, la femme blonde haletait doucement, les yeux hagards, les jambes écartées. Un bébé silencieux se tortillait auprès d’elle tandis qu’un second crâne minuscule faisait son apparition dans l’enfer de la vie.

À mes pieds, ce qui avait été la maîtresse du monde se convulsait dans les affres de l’enfantement.

— Deviens… Deviens le maitre du monde ! bégaiait-elle. Appuie !
— Appuie ! me supplia 612.

Mais j’avais compris. Une mère est prête à tout pour ses enfants. Une mère ne me confierait jamais les rennes du monde.

Lentement, je m’assis devant le clavier et l’écran. Il état là, le véritable maître du monde. Celui que tout le monde craignait. Celui qui faisait suinter la peur dans les esprits, qui organisait la construction, l’achat, la destruction, la vente, infini cycle consumériste qui consumait lentement la planète.

Dans la pièce, le silence était revenu. Le cauchemar s’était tu. 612 avait disparu, chassé de mon esprit par ma nouvelle lucidité. Seuls restaient des cadavres, une parturiente agonisante et deux nouveaux-nés.

Je contemplai mon œuvre. La femme nue râla, porta la main à sa gorge et tenta de se relever. Sans succès.

Je souris.

Peur, ma fidèle conseillère, ma vieille amie. Je t’obéirai. Je suis ton humble serviteur.

Lentement, je m’éloignai du clavier et de la touche. Je vénérais l’écran, le véritable maître du monde. Mais il savait que, d’une simple pression, je pouvais l’éteindre. Le monde avait retrouvé l’équilibre. Je devais me mettre au service du maître du monde et de ma peur.

Une forme grise sauta sur le bureau, près de l’écran.
— Miaouw ! fit-elle.

Je sursautai.
— Miaouw ! insista-t-elle.

Elle retroussa ses babines, me montrant de minuscules dents blanches. Un feulement jaillit de ce petit corps poilus.
— Lancelot, murmura la femme qui accouchait. Mon petit Lancelot à sa mémère…

Incrédule, je détournai le regard. Mais, doucement, sans même avoir l’air d’y prêter attention, la bête se mit à marcher sur le bureaux. Sa patte enfonça la touche du clavier. Des lignes se mirent à défiler à toute vitesse sur l’écran avant de s’arrêter. Rien ne se passa. Était-ce un subterfuge de mon esprit où la lumière avait-elle clignoté un bref instant ?

Dans un profond borborygme, la femme nue parvint à se mettre à genoux, le corps couvert de sang.

Sur le sol, les deux bébés se mirent soudain à crier. Au dessus de moi, le plafond laissa soudain passer un voile de ciel d’un bleu trop clair, trop brillant.

Photo by Grant Durr on Unsplash

=> https://unsplash.com/photos/ag4bXTDK49E?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText Grant Durr
=> https://unsplash.com/collections/3422333/my-first-collection/bc85f85fb80db6b193fd1dcf2ffec545?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText Unsplash
