# Conserver la tradition du changement, mais pas trop

=> ../files/old/segol.jpg Pour le changement
Il est amusant de constater que les slogans politiques récurrents concernent pour une grande majorité le désir de changement. Sans que le dit changement soit explicité, des hordes d’électeurs et de supporters se ruent affamés sur le moindre populiste au sourire étincelant promettant d’une voix puissante un changement profond, une refonte complète, un remaniement du système voire, comble de l’orgasme, une réforme ! Lorsque tout cela vient d’une parti dit conservateur, je me délecte d’une touche d’ironie saupoudrant l’habituel cynisme avec lequel il convient de profiter de ces spectacles.

=> http://www.flickr.com/photos/butterflyinbabylon/412501578/ 

Au départ, je me suis amusé de ce qu’un changement non précisé pouvait bien être une mauvaise surprise. « Voilà, j’ai tout rasé, j’ai tout détruit. Vous vouliez le changement non ? ». Mais, finalement, j’ai réalisé que ce désir de changement n’était qu’un artifice rhétorique. En fait, au plus l’être humain s’enfonce dans son petit confort de voitures chauffées-séries télé américaines sur grand écran, moins il veut changer. Pire, changer devient le danger absolu, le plus grand péril. Tout est justifié afin de contrer les moindres velléités de changement. Ce conservatisme est bien plus profond dans les gênes de l’être humain que je ne l’avais cru jusqu’à présent.

Un petit exemple ? Juste un alors, car à vrai dire, je suis intarissable sur le sujet. Dernièrement, il m’est venu la lubie d’apprendre à taper suivant une disposition de clavier Dvorak. Je vous situe brièvement le contexte.

=> http://fr.wikipedia.org/wiki/Clavier_Dvorak Dvorak

En 1868, un certain Mr. Sholes invente la première machine à écrire. Il dispose les lettres dans l’ordre alphabétique. Malheureusement, la mécanique encore balbutiante fait que lorsque deux lettres consécutives sont tapées, elles coincent. Plutôt que de résoudre le problème mécanique, il décide de disposer les touches de manière aléatoires pour que les lettres tapées soient le plus souvent espacées. Et pour le fun, il décide que toutes les lettres du mot « Typewriter » seront dans la colonne du haut. Le clavier ainsi créé est appelé Qwerty, en raison des premières lettres. Pour la même raison mécanique, les touches ne sont plus alignées mais disposées en lignes décalées. C’est moins facile de taper de cette manière mais cela solutionne bien des embarras mécaniques. Pour une raison que j’ignore, chaque pays décide d’adapter le clavier à sa sauce. Le clavier français devient donc Azerty. Logiquement, cela devrait être pour la différence de langue mais dans ce cas, comment expliquer que le clavier espagnol soit Qwerty et que les claviers français et belges soient des Azerty différents ?

=> http://dvzine.org/zine/01-toc.html Mr. Sholes

Bref, le clavier Qwerty/Azerty est une horreur, Sholes lui-même le reniera et proposera de changer dès les ennuis mécaniques réglés mais trop tard, les clients s’habituent au Qwerty. Encore aujourd’hui, nous payons le prix de cette erreur. Même le non-alignement des touches a été conservé alors qu’il est démontré qu’un clavier de type « Typematrix » est plus ergonomique et plus précis !

=> http://www.typematrix.com/ Typematrix

En 1936, Mr. Dvorak décide de créer scientifiquement un clavier le plus ergonomique possible. Le résultat est une disposition qui porte son nom et qui permet d’accéder aux touches les plus fréquentes sans bouger les doigts, de réduire les mouvements des mains et d’améliorer rapidité et précision. Le record du monde de vitesse de dactylo est d’ailleurs réalisé sur un clavier Dvorak.

=> http://www.dvorak-keyboards.com/ 
=> ../files/old/dvorak.jpg Click here for Dvorak info

Comme la méthodologie de conception du clavier dépend très fort de la langue (la fréquence des lettres peut fort varier), il faudra attendre 2002 pour qu’une version Dvorak-fr soit proposée par Francis Leboutte, version ensuite légèrement modifiée par Josselin Mouette. Ce clavier, conçu uniquement à partir de statistiques de textes littéraires, des amateurs éclairés décident de se regrouper pour proposer et améliorer constamment un clavier Dovorak-fr, appelé « Bépo » car ce sont les premières lettres du claviers[1].

=> http://www.clavier-dvorak.org/wiki/Accueil « Bépo »

=> ../files/old/azerty.jpg Azerty
Bref, devant passer beaucoup de temps à taper, ressentant souvent des douleurs dans les mains et les doigts, j’ai décidé de tenter l’aventure Dvorak. Et je peux vous dire qu’on oublie pas 15 ans d’Azerty comme ça, c’est un vrai challenge mais je vous en reparlerai une fois que j’aurai récupéré ma vitesse de frappe, là je suis lâchement retourné en Azerty. Intrigué par la carte de clavier disposée en dessous de mon écran, plusieurs collègues m’ont posé des questions auxquelles j’ai répondu avec la petite histoire que vous venez de lire. Pour illustrer mon propos, je donnais deux exemples que je trouve marquant : le point-virgule sous azerty est plus facile d’accès que le point malgré sa très faible fréquence en français. Une des touches les plus faciles à trouver, dans le coin supérieur gauche, est utilisé pour symboliser… l’exposant deux ! Personnellement, je n’ai jamais jamais utilisé cette touche de ma vie. Les rares fois où le besoin d’un exposant deux s’est fait sentir, j’ai utilisé la fonction appropriée de mon traitement de texte. Incroyable non ?

=> http://www.abisource.com/ traitement de texte

Je m’attendais à des réactions du type « Ouais, mais bon, faut réapprendre un clavier, moi je n’ai pas le temps » ou « Ça me frustrerait de retaper lentement » voire « Tout le monde utilise Azerty, je préfère pas changer ». Ce sont des réactions tout à fait logiques et, étant donné que le choix d’un clavier est tout à fait personnel, je n’aurais rien trouvé à redire. Mais, à mon grand étonnement, certains de mes collègues se sont fait un devoir de défendre l’Azerty comme si leur vie en dépendait. Jusqu’à l’absurde : « Ben justement, c’est super pratique quand tu dois taper un exposant ! ».

J’avoue en être resté bouche-bée[2].

Je marque une pause là, et je vous répète pour que vous saisissiez bien : l’Azerty est pratique car justement, il permet facilement de taper un exposant deux ! Les points d’exclamation me manquent…

Un exposant deux quoi !

Alors que j’ai tendance de voir des opportunités de progrès dans chaque changement, beaucoup ne voient qu’une critique de leur comportement passé. Changer de clavier impliquerait d’accepter que, pendant 10 ou 20 ans, on a été dans l’erreur. Que l’on a investit du temps ou de l’argent à tort. Changer, c’est le danger ! Changer, c’est mauvais car sinon, j’aurais déjà changé depuis longtemps !

=> ../files/old/tradition.jpg tradition

Pour étendre le propos : rien ne sert de démontrer tous les avantages d’Ubuntu ! Aucune réelle motivation rationnelle ne peut plus défendre l’utilisation d’un système aussi mal foutu que Windows XP. Par contre, passer à Ubuntu impliquerait que tout l’apprentissage des stupidités de l’interface utilisateur de Windows, toute cette habitude, tout cela était inutile ! Il n’est donc pas question de changer. Exactement comme un conducteur de voiture refuserait de passer à des voitures volantes écologiques à alimentation illimitée sous prétexte que, si il le faisait, tout son apprentissage du changement de roue, de vidange d’huile et de plein d’essence ne servirait plus à rien. Et puis, tout ce qu’on a payé pour cette essence et ces entretiens, cela voudrait dire que c’était de l’argent jeté par les fenêtres ?

=> http://ubuntu-fr.org/ Ubuntu

15 ans de monopole de Windows, 150 ans de règne de Qwerty. Personne ne veut changer et, au contraire, se fanatise. Après 2000ans, difficile d’admettre que l’existence d’un dieu ne tient plus fort la route d’un point de vue scientifique, que si manger du porc était une mauvaise idée il y a 1500 ans dans les pays chaud, les conditions actuelles rendent sa consommation parfaitement saine, que si mutiler (légèrement) le pénis des garçons pouvait éviter des infections dans les conditions d’hygiène des nomades du désert il y a 3000 ans, rien à ma connaissance ne justifie de perpétuer cette pratique.

Depuis la nuit des temps, l’homme édicte des règles afin de prendre des dispositions pratiques utiles, attribuant ces règles à un pseudo-pouvoir divin, à la tradition ou au législateur (suivant ce qui marche le mieux auprès d’une population donnée). Quelques temps plus tard, ces dispositions n’ont plus de raison d’être, la société a évolué. Mais l’humanité trouve encore une force hors du commun pour interpréter ces règles au pied de la lettre, se battant pour pouvoir les appliquer, tuant parfois juste pour que tout le monde respecte des lois édictées pour réguler l’échange de silex en Mésopotamie il y a quelques millénaires.

C’est même devenu parodique : les avocats tentent d’interpréter subtilement des lois d’il y a 150 ans afin de justifier des actions que le bon sens réprouve, les religieux intégristes basent toute leur vie sur la traduction de la traduction de la traduction d’un ensemble de bouquins mis ensemble il y a quelques siècles, les responsables informatiques sont prêts à payer des fortunes pour avoir un système qui porte le même nom que celui utilisé depuis 10 ans. On invente même le concept d’agnosticisme pour intégrer le dévelopement de l’esprit critique sans trop vexer des millénaires de croyances. Mais le pire, c’est peut-être que tout le monde trouve ça parfaitement normal.

=> ../files/old/SNES_controller.jpg Manette
J’aime comparer l’humanité à apprenti joueur vidéo. Mais au lieu de regarder l’écran et tenter de saisir le sens, il regarde sur une très vieille vidéo les doigts d’autres joueurs sur le clavier et tente de reproduire les mouvements sans se préoccuper du sens, de la raison ni de l’interprétation. Gauche, gauche, haut, droite, les yeux rivés sur le clavier, haut, haut pendant 2 secondes, …

Et non, je n’ai aucune gêne à généraliser une disposition de touches sur un clavier au comportement de l’humanité ces 3000 dernières années. Je trouve même cela plutôt amusant. Na…

Gauche, haut, droite, droite, droite, … oups, c’est un dixième de seconde de moins, on recommence … On lui dit que le jeu a été mis à jour ?

#### Notes

[1] Le principal problème de Bépo étant qu’il évolue constamment. Un jour, faudrait leur dire qu’un clavier optimisé à 99% et disponible maintenant est préférable qu’un clavier qui change tout le temps pour être un jour optimisé à 100% alors que bon, de toutes façons, à partir du moment où on demande l’avis des gens, ils seront forcément pas contents.

[2] Je sais bien que certains lecteurs s’étonneront de mon ignorance et posteront, comme d’habitude, un commentaire pour souligner ce fait. Ils ne changent pas 😉
