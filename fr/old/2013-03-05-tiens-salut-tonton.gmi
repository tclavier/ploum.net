# Tiens, salut tonton !

Il y a quelques années, alors que je rattrapais mon inculture cinématographique en regardant la trilogie de La 7ème compagnie, voilà que, dans le troisième épisode, un individu entre entre dans la chapelle où se sont réfugiés les héros.

— Tiens, salut tonton !

Il faut dire que mon tonton Francis est coutumier de ce genre de surprises. Acteur de théâtre, il a néamoins une floppée de petits (et moins petits) rôles au cinéma à son actif, comme l’atteste sa filmographie. Il m’avait déjà fait le coup dans Les anges gardiens, lors de la scène d’ouverture du Jaguar ou dans les secrets professionnels du docteur Apfelglück où son « Bonjour docteur » est resté dans les annales. Je regarde un film sans me douter de rien et puis « Tiens, salut tonton ! »

=> http://fr.wikipedia.org/wiki/Francis_Lemaire_(acteur) sa filmographie

À chaque enterrement ou à chaque fête de famille, il en profite pour m’enseigner sa science pour faire le guignol. En une après-midi il m’a appris à jongler avec des quilles, avec un certain succès. Il a tenté de m’expliquer l’accordéon, avec moins de succès.

=> ../files/old/francis.jpg francis

Mon tonton Francis, en train de me donner une leçon de « Je fais le con dans une fête de famille ».

Appliquant les leçons, je mets en pratique lors du décès de mon tonton Éli, grand amateur de jeu de mot foireux devant l’éternel. Mon tonton Francis apprécie la performance, lui qui partage avec Éli et moi cette dévorante passion de la science-fiction et du jeu de mot foireux. Nous passons l’après-midi ensemble et je découvre une nouvelle facette de la personnalité de cet artiste aux multiples talents, celle qui l’a fait participer à l’aventure de la revue Planète dont la collection complète encombre mon grenier.

=> /196-la-vie-trepidante-des-moules-prelude grand amateur de jeu de mot foireux
=> http://fr.wikipedia.org/wiki/Plan%C3%A8te_(revue) la revue Planète

Et comme à chaque fois qu’on se voit, il me promet de m’envoyer le texte de son one-man-show. Je lui promet de lui envoyer mes nouvelles de Science-Fiction. Et puis on se quitte : « Salut tonton ! »

Ce vendredi, tonton Francis, tu sacrifieras à la mode chère à toutes les grandes stars parisiennes, à savoir te faire ensevelir dans un petit carré de terre au Père-Lachaise.

Ce n’est pas l’envie qui me manque de venir faire une dernière fois le guignol devant toi, de te réclamer ce one-man-show tant promis et de balancer quelques jeux de mots foireux. Mais je laisserai faire ceux qui te connaissaient mieux. Je me contenterai d’un petit signe de la main en murmurant :

— Tiens, salut tonton !
