# Obéir, lire, écrire : les trois apprentissages de l’humain
=> files/old/535425592_df9ed166a1_o.jpg

Comme je le soulignais dans « Il faudra le construire sans eux », l’humanité est passée par plusieurs étapes liées à l’écriture. Chaque étape a modifié notre conscience de nous-même et le rapport que nous entretenons avec la réalité.

=> /il-faudra-la-construire-sans-eux/ Il faudra le construire sans eux

## Au commencement

Durant la préhistoire, l’humanité est dans sa petite enfance. Sans écriture, l’information est mouvante, sujette à interprétation. La transmission se fait de manière floue, interprétée et adaptée. La vérité n’existe pas, tout est sujet à interprétation.

Le terme « préhistoire » n’est pas anodin. Les humains n’ont en effet pas la conscience de faire partie de l’histoire, d’une évolution. Ils vivent dans le moment présent.

Cette situation est comparable à celle d’un nouveau-né qui réagit par réflexe aux stimuli extérieurs mais sans conscientiser son existence et ni celle d’une réalité extérieure.

## Obéir, le premier apprentissage

Avec l’invention de l’écriture apparaît la notion d’une réalité extérieure immuable, la vérité. Ce qui est écrit ne peut être modifié et, contrairement aux souvenirs ou à la transmission orale, permet la création d’une vérité immuable. Comme toute innovation technologique importante, l’écriture est parfois perçue comme magique, surhumaine.

De la même manière, un enfant comprend que les adultes détiennent un savoir que lui ne possède pas. Il apprend donc à obéir à l’autorité.

Pour l’humanité, l’écriture est un instrument d’autorité à la fois géographique (les écrits voyagent) et temporel (les écrits se transmettent). L’écrit est donc perçu comme étant la vérité ultime, indiscutable. Comme pour un enfant à qui on raconte des histoires, la fiction et l’imaginaire sont des concepts incompréhensibles.

## Lire, le second apprentissage

L’imprimerie provoque un véritable bouleversement. Pour la première fois, les écrits deviennent accessibles au plus grand nombre. L’humain apprend à lire.

Le fait de lire permet de prendre conscience que tout écrit doit être interprété. Face à différentes sources parfois contradictoires, le lecteur comprend qu’il doit reconstruire la vérité en utilisant son esprit critique. L’impression de la bible, par exemple, aura pour conséquence directe la naissance du protestantisme.

Si l’imprimerie a permis de diffuser l’information, seule une minorité contrôlait ce qui était imprimé. Cette compréhension du rôle fondamental de l’écriture a d’ailleurs conduit au copyright, outil de censure créé dans le seul et unique but de s’assurer que rien de ce qui était imprimé ne remettait en cause le système (et non pas pour protéger les auteurs).

=> https://www.privateinternetaccess.com/blog/2016/11/lessons-history-rulers-rather-kill-people-allow-share-ideas Histoire de l’invention du copyright

Comme nous l’a montré la révolution française, le copyright n’a pas été un garde-fou suffisant. La technologie l’a emporté, l’autorité suprême est rejetée au profit du débat et du consensus. Du moins en apparence. L’individu devient citoyen. Il n’accepte plus une autorité arbitraire mais uniquement celle qu’il a choisie (ou qu’il a l’impression d’avoir choisie). C’est l’avènement de la démocratie représentative et de son apanage direct, la presse.

L’enfant apprend à désobéir, à questionner l’autorité et à se faire sa propre opinion. Il n’est pas encore autonome mais souvent rebelle. C’est l’adolescence.

## Écrire, le troisième apprentissage

Avec l’apparition d’Internet et des réseaux sociaux, écrire, être diffusé et lu devient soudain à la portée de n’importe quel individu.

Que ce soit pour commenter un match de football, partager une photo ou ce texte, tout être humain dispose désormais de la faculté d’être lu par l’humanité entière.

L’adolescent a grandi et partage son opinion avec d’autres. Il enrichit le collectif et s’enrichit lui-même de ces échanges. Il devient le créateur de sa propre conscience, une conscience qu’il perçoit comme s’insérant dans un réseau d’autres consciences, parfois fort différentes de la sienne : l’humanité.

L’autorité, même choisie, n’est plus tolérable. Chaque humain étant en conscience, il veut pouvoir décider en direct, être maître de son propre destin tout en s’insérant dans la société. C’est un mode de gouvernance qui reste à inventer mais dont on aperçoit les prémices dans la démocratie liquide.

Selon moi, une conscience globale ne sera donc atteinte que lorsque chaque humain sera lui-même pleinement conscient de son libre arbitre et de sa contribution à l’humanité. Qu’il aura la volonté d’être maître de son destin tout en ayant la maturité pour s’insérer dans une société qu’il co-construit.

## Le déséquilibre de l’écriture sans lecture

Obéir, lire, écrire. Trois étapes indispensables à la création d’une conscience autonome et responsable.

Le problème est que, pour l’autorité, il est préférable de garder les humains dans le stade de l’obéissance, de la croyance en une seule vérité immuable et indiscutable. La démocratie représentative n’est alors qu’une façade : il suffit de rendre les électeurs obéissants en ne leur apprenant pas à « lire ».

Avec la généralisation des réseaux sociaux, des humains accèdent directement à l’écriture sans jamais avoir appris à « lire », à être critiques. Ils sont confrontés à des débats, à des opinions divergentes, à des remises en question pour la première fois de leur vie.

Comme je l’explique dans « Le coût de la conviction », cette confrontation est souvent violente et conduit au rejet. Les personnes concernées vont se contenter de hurler leur opinion en se bouchant les oreilles.

=> /le-cout-de-la-conviction/ Le coût de la conviction

La solution la plus pertinente serait de patiemment tenter de mettre tout le monde à niveau, d’enseigner la lecture et l’esprit critique.

## Le dangereux attrait de l’autoritarisme

Malheureusement, nous sommes enclins à tomber dans nos travers. Ceux qui écrivent ont tendance à se considérer comme supérieurs et ne veulent pas que d’autres puissent apprendre à lire.

Le fait de pointer Facebook et Google dans la propagation de « Fake News » va exactement dans cette direction. Facebook et Google sont intronisés détenteur de la vérité ultime comme l’étaient les médias. En leur demandant explicitement de traiter différemment les “fake news” et les “real news”, nous leur donnons le pouvoir de contrôler ce qui est vrai et ce qui ne l’est pas, nous leur demandons de contrôler la réalité.

Est-ce souhaitable ?

Je pense que ce serait une véritable catastrophe. Au contraire, nous devons être sans cesse confrontés à des informations contradictoires, à des nouvelles qui contredisent nos croyances, à des réflexions qui démontent nos convictions.

Nous devons réaliser que les médias à qui nous avons délégué ce pouvoir de vérité sont mensongers et manipulateurs, même inconsciemment. Ils gardent jalousement l’écriture et sont donc devenus des obstacles à l’évolution de l’humanité.

Si nous voulons sortir de l’obéissance aveugle, nous devons apprendre à critiquer, à accepter nos erreurs. Nous devons apprendre à lire et à écrire !

Désobéir, lire, écrire…

=> https://www.flickr.com/photos/davidfielke/535425592 Photo par David Fielke
