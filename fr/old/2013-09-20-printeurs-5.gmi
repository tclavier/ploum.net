# Printeurs 5
=> files/old/liquid_nitrogen.jpg

Devant moi se dresse Georges Farreck, le grand, l’immense Georges Farreck, l’étoile d’Hollywood, l’archétype de la virilité tendre et romantique dans tous les blockbusters de cette dernière décennie. Georges Farreck, l’homme par qui j’ai découvert mon homosexualité. Georges Farreck sur l’image de qui je me suis masturbé durant toute mon adolescence. Georges Farreck, l’idéal masculin de toute une génération. Georges Farreck, l’homme dont les sites torche-culs se délectent à la moindre de ses incartades amoureuses. Georges Farreck, quoi !

— Bonjour Nellio.

Ma pomme d’Adam devient un sac de gravier qui me déchire la gorge en un incontrôlable mouvement de va-et-vient. Mes lèvres sont sèches, je secoue la tête. « Bonjour, je me suis beaucoup branlé sur vos films » me semble une bien piètre entrée en matière.

— Bon… jour…

De vieux réflexes prépubères sont sur le point de faire naître une érection. Alerte ! Les glandes explosent ! Mais, bon sang, c’est un homme comme un autre. Il me regarde, s’approche de moi. C’est Georges Farreck et il tient son visage à quelques centimètres du mien ! Eva éclate de rire.

— Regarde Nellio, regarde bien. Je sais ce que tu ressens.

Il me saisit la main. Mon cœur s’arrête, mon sexe se tord douloureusement dans mon pantalon. Doucement, il amène mes doigts dans ses cheveux, sur certains endroits de son visage.

— Regarde avec tes yeux, ton intelligence pas avec tes souvenirs ni tes sentiments.

Les cheveux sont grisonnants, irréguliers. Des pellicules s’effritent entre mes doigts. Près des paupières, de minuscules cicatrices disgracieuses témoignent des nombreuses retouches chirurgicales. Je découvre avec étonnement un léger strabisme. Par endroit, la peau est constellée d’irrégularités, de petites rougeurs. Je recule, effrayé.

— Vous n’êtes pas Georges Farreck ?
— Si, je suis Georges Farreck. L’humain appelé Georges Farreck. Acteur de profession et, accessoirement, très riche. Mais je ne suis pas le Georges Farreck que tu connais au visage lisse, parfait, celui qui n’apparaît que maquillé et retouché par ordinateur. Je ne suis pas le fantasme dont les publicités te martèlent le crâne. Ces publicités qui ont pris le contrôle de tes émotions, de tes glandes afin que tu dépenses ton argent dans n’importe quel film auquel je suis lié.
— Ou du café…
— Oui, il y a cette marque de café dont je suis l’égérie. Enfin, est-ce encore moi ? Ou est-ce un Georges Farreck auquel j’ai servi de modèle ?
— Mais pourquoi êtes-vous ici ? Comment connaissez-vous mon nom ?

Il me propose de prendre une chaise et se laisse lui-même tomber dans un fauteuil du salon avant de croiser ses jambes en une gestuelle élégante, calculée, presque chorégraphiée. Ses bras s’écartent sur les accoudoirs et un sourire ravageur se dessine sur son visage. Pas de doute, c’est Georges Farreck. Malgré ce qu’il vient de me dire, je me mords la lèvre inférieure et ferme les yeux. Georges Farreck !

— Cela fait longtemps que nous cherchons quelqu’un comme toi. Tu dois te douter que coordonner les résultats de dizaines d’équipes de chercheurs universitaires tout en gardant l’objectif ultime secret est un travail titanesque qui coûte très cher. Cela n’aurait pas été possible sans le soutien d’une personne ou d’une organisation extrêmement riche, quelqu’un qui profite du système mais qui, malgré tout, souhaite le changer. Ce généreux mécène, c’est moi !

Eva nous interrompt :
— Lorsque vous aurez fini de vous peloter et de vous lancer des œillades dans les fauteuils, on pourrait peut-être se mettre au travail ?

Elle est toujours aussi belle mais ses sourcils sur le point de se rejoindre semblent indiquer une contrariété. Je ne résiste pas et lui lance :
— Jalouse ? De moi ou de lui ?

Georges Farreck éclate de rire. Un rire franc et puissant qui me glisse le long de la nuque comme une coulée de cire chaude. Eva inspire profondément, faisant poindre ses petits seins sous son t-shirt. Curieusement, l’overdose de stimulation sexuelle semble s’annuler, s’équilibrer. Alors que mes gonades se battent en duel pour savoir qui de Georges ou d’Eva est le plus attirant, ma curiosité reprend le dessus.
— Au fond, je ne sais même pas pourquoi je suis là. Peut-être auriez-vous la bonté de m’expliquer à quoi rime toute cette histoire de rébellion ? Et puis, qu’est-ce qui vous prouve que je ne vais pas vous trahir ?
— Pas de soucis à ce niveau, me réplique Georges. Tu es quelqu’un de très actif sur les réseaux sociaux. Une de mes sociétés de production a envoyé aux services secrets une requête disant que tu étais soupçonné d’être en mesure de pirater mes films et demandant ton profil psychologique détaillé afin de préparer une mise en demeure préventive.
— Préventive ?
— Oui, une lettre menaçante disant que nous savions que tu n’avais pas encore piraté mais que tu étais capable de le faire et que tu étais dans notre collimateur.
— Mais c’est illégal pour une société privée d’obtenir un profil psychologique sans arrêt judiciaire !
— Oui, et alors ? Le département des renseignements coûte effroyablement chers. Le gouvernement le rentabilise en offrant ses services aux entreprises privées. Mais attention, pour respecter la loi à la lettre, les entreprises privées en question ont toujours un politicien élu dans leur conseil d’administration. De cette manière, il n’y a pas vente des données mais « synergie entre le public et le privé sous la responsabilité d’un représentant élu ». Enfin, bref, le plus important c’est que parmi tous les candidats que nous avons explorés, tu étais le plus loyal, sensible à notre cause et compétent techniquement.
— Mais de quelle cause parlez-vous exactement ?

Eva, qui était restée debout, me fait un signe de la main m’invitant à la suivre. Elle ouvre une porte qui donne sur un enchevêtrement de câbles. Quelques moniteurs éclairent la pièce d’une lueur blafarde. Un rack de serveurs clignote en une psychédélique sarabande. La surface des tables a disparu sous les claviers poisseux, les gobelets de café stratifiés et les improbables feuilles de notes. Je saisis, entre le pouce et l’index, une tasse en papier dont le premier usage doit probablement remonter au crétacé inférieur. Je la lève avec un clin d’œil vers Georges :
— Quoi d’autre ?

En réponse, il jette un regard désespéré à Eva. De concert, ils décident de faire comme s’ils n’avaient rien entendu. Se prendre un bide avec Georges Farreck : achievement unlocked. Eva retire la housse de ce que je reconnais comme étant un microscope électronique. Je siffle entre mes dents :
— Joli labo. Pour la déco, on dirait ma chambre. Vous n’avez pas un accélérateur de particules caché sous une table ?

Sans prendre la peine de me répondre, Eva dispose différentes poudres sur une surface plane parfaitement protégée et isolée du capharnaüm ambiant. Georges se tient sans rien dire derrière moi, les poings sur les hanches.
— Là, je dispose tout simplement une infime quantité de matériaux de base : du fer, de l’or, du cuivre, du silicium. Pas besoin que ce soit pur mais, dans un premier temps, c’est plus facile.

Elle déplace l’objectif du microscope, pianote sur un clavier. Une image apparaît sur un moniteur : la surface plane, agrandie des millions de fois. Eva ouvre un tiroir, une épaisse fumée en sort.
— Un accélérateur de particules, peut-être pas. Mais bien un frigo à azote liquide.

Sans un instant d’hésitation, elle enfile un épais gant, se saisit d’une petite pipette et dépose une goutte de liquide avant de le ranger et de refermer le container frigorifique. Sur l’écran, j’aperçois un point noir un peu trouble.
— Des centaines de scientifiques ont contribué, sans le savoir, à ce résultat. Ce que tu vois mesure un millier d’atomes ou à peine plus. C’est plus petit qu’une bactérie.

En quelques clics, elle règle la mise au point. Effectivement, une forme oblongue se précise. Une forme qui se déplace et qui entre en contact avec les matériaux saupoudrés par Eva. La frontière entre la forme et le matériau se fait floue.
— Il arrache des atomes, murmure Eva. Vas-y mon petit, vas-y !

L’étrangeté de ma situation me frappe. Je me tiens à côté d’une des plus grandes stars du cinéma en train de regarder la femme dont je suis éperdument amoureux, toujours affublée d’un maquillage anti-reco, encourager un assemblage d’atomes comme un chien à qui on aurait appris à faire le beau. J’avoue ne pas voir l’intérêt de tout cela jusqu’au moment où…
— Mais il grossit ! m’écrié-je.
— Non Nellio, regarde bien.

Je retiens mon souffle. Sur l’écran, le point noir me semble avoir presque doublé de surface mais je réalise qu’il s’agit de deux formes distinctes, deux formes parfaitement identiques qui commencent toutes les deux à s’attaquer au matériau restant. Je pousse un petit cri de surprise :
— Il s’est dupliqué !
— Disons plutôt qu’il a imprimé une copie de lui-même. C’est un peu différent. La duplication fait penser à une forme de mitose, ce n’est pas le cas ici.
— Mais c’est quoi ce projet ? Quel est votre objectif ? Quel rapport avec moi ?

La voix de Georges s’élève dans mon dos :
— C’est ce que nous appelons le projet von Neumann. Et c’est à ce stade que nous avons besoin de toi !

Photo par Daniel Go

=> http://www.flickr.com/photos/84172943@N00/6775649855 Daniel Go
