# Printeurs 49
=> files/old/yanal-tayyem-551824-unsplash.jpg

Le face à face continue entre Nellio et Eva, d’une part, et Georges Farreck, le célèbre acteur, et Mérissa, une femme mystérieuse qui semble contrôler tout le conglomérat industriel d’autre part.

– Mais… Et ma fondation pour les conditions de travail des ouvriers ? m’interrompt Georges Farreck. N’essayons-nous pas de rendre les conditions meilleures ?
– Bien sûr, répond Eva. L’algorithme a très vite compris que les humains se satisfont de leur sort s’ils sont persuadés qu’il y’a pire qu’eux. Et pour les convaincre, la méthode la plus efficace est de prendre une star adulée qui va leur demander de l’aide. « Moi qui suis milliardaire et célèbre, j’ai besoin de votre argent pour aider ceux qui sont encore plus pauvres que vous, ce qui va vous convaincre qu’il y’a plus pauvre et plus malheureux ! Donc vous faire accepter votre sort. »
— C’est absurde ! m’écrié-je.
— C’est la nature humaine, siffle Mérissa doucement. On n’a pas attendu l’algorithme pour cela.
— Mais l’algorithme est devenu dangereux, lui lance Eva. Il faut l’arrêter !
– En quoi est-il un danger ? Il n’a jamais aussi bien fonctionné ! Il ne fait que faire fonctionner la société comme elle l’a fait depuis des décennies.

Eva s’approche en tremblant du bureau de Mérissa. Quelque chose a changé la donne. D’un geste vif, elle lui brandit son bras écorché sous le nez.

— Je… Je ne comprends pas ! bégaie la femme la plus puissante du monde.

Un rayon de soleil perce les nuages et ricoche à travers les verrières colorées qui forment un étrange plafond lumineux dans la pièce. J’ai l’impression d’assister à la conclusion d’une mauvaise série B. Immobile, le cadavre de Warren ajoute une touche macabre mais pourtant fort à propos.

— C’est pourtant logique, grogne Eva entre ses dents. Comme tout ce qui touche à l’algorithme. C’est infiniment logique.
— Je…
— Il a d’abord créé des corps humains réalistes, des poupées sexuelles. C’était facile, cela fait des années que les hommes en réalisaient. Puis, il a assemblé les différents algorithmes de conscience artificielle et les a chargé dans une seule et unique poupée. Il a lancé un programme de test des autres poupées afin de retarder leur lancement commercial. De cette manière, la première poupée, la seule et unique poupée sexuelle consciente, pouvait se mêler aux humains sans se faire remarquer.

Étrangement, je me sens détaché de ces révélations. Une partie de moi-même avait compris cette vérité qui flottait dans mon inconscient sans jamais percer la surface, maintenue dans les profondeurs ignorantes par mon humaine volonté de préserver ma foi, de ne pas m’exposer aux rigueurs de la réalité.

— Mérissa, je suis l’algorithme ! Il faut m’arrêter !

Eva lui a brutalement empoigné les mains. Leur visage sont proches à se toucher.

— Tu n’es pas l’algorithme ! Tu n’es qu’une de ses inventions. Ou une humaine. Je ne sais pas. Mais pas l’algorithme !
— La première découverte que fit la poupée sexuelle Eva fut qu’elle avait besoin d’un véritable corps de chair et d’os pour ressentir la douleur comme un véritable humain. L’algorithme conçut alors le plan de lui en fournir un grâce à une imprimante 3D moléculaire. Cette imprimante révolutionnaire fut créée de manière complètement autonome grâce à l’accès à tous les papiers scientifiques dans le domaine, grâce au code open source de milliers de projets. Mais le projet échoua…

Les rayons de lumière dessinent d’étranges arabesques. Des poussières tournoient. Une ombre, un mouvement se dessine à l’extrême limite de mon champs de vision, me donnant l’impression d’une présence.

— L’algorithme n’était que la somme des connaissances humaines écrites et partagées. Pour la première fois, il échouait. Il avait besoin d’une forme de créativité. Il identifia rapidement la personne la plus susceptible de l’aider. C’était toi, Nellio !

D’un geste théâtral, elle pointe son doigt dans ma direction.

— Moi ? Je…
— Et comment t’attirer ? Te convaincre ? Étouffer toutes tes suspicions ? Tout simplement avec une attirance sexuelle combinée de Eva, poupée conçue dans cet objectif, et Georges Farreck, ton fantasme d’adolescent.

Georges et moi-même poussons à l’unisson un cri de surprise.

— Mais…
— Georges, tu fus le plus facile à manipuler. Il a suffit de te faire miroiter que ton personnage d’acteur qui ne serait, dans une décennie, plus qu’une page wikipédia oubliée, deviendrait un bienfaiteur de l’humanité.

Je réagis.
— Cela ne colle pas Eva. Nous avons été attaqués chez Georges Farreck. J’ai failli être tué chez Max.
— Mais tu t’en es sorti à chaque fois ! L’algorithme savait que le printeur était une invention dangereuse, la seule et unique invention capable de lui faire perdre son emprise sur l’humanité. Il devait la développer mais la garder secrète. Grâce à la menace permanente, nous avons pris toutes les précautions nécessaires pour que le printeur reste dans l’ombre. Une fois le projet terminé, il fallait que je meure devant toi pour que tu aies l’idée de me ressusciter à travers le printeur.
— Eva…

Mon regard plonge dans ses yeux noirs, profonds, lumineux et j’y lis soudain l’infini de toutes les tristesses humaines, de toutes les émotions de l’humanité.

— J’ai… J’ai soudain découvert la douleur, bégaie-t-elle. J’ai découvert la condition humaine.

Mérissa a porté sa main à sa bouche. Georges Farreck est immobile, retenant sa respiration. Les images d’Eva hurlant, se tordant de douleur sur le sol dansent dans ma tête.

— Tu… Tu as été le premier humain imprimé ! fais-je. C’est… C’est…
— Non, fait-elle. Tu l’as été Nellio. Tu es le premier humain ressuscité, revenu d’entre les morts.
— Quoi ?

Je reste interdit. Un éclair me foudroie soudain le cerveau, ma respiration se coupe, je panique.
— Ainsi, murmure Georges Farreck, Nellio est bel et bien mort lors de notre survol du sultanat islamique. Je m’en doutais, je ne voulais pas l’accepter.
— Je pense que c’était un imprévu, un élément complètement aléatoire qui a perturbé les plans de l’algorithme.
— Arrêtez ! Taisez-vous ! nous lance Mérissa, pâle comme la mort.
— Il faut arrêter l’algorithme, insiste Eva. Toi seule peut le faire sans qu’il se défende.
— Non, je…
— Les printeurs sont en train d’être diffusés. Un nouveau monde fondamentalement incompatible avec l’algorithme est en train de naître. Tu as le pouvoir d’empêcher un conflit meurtrier entre les deux mondes, tu peux…
— Je ne veux rien du tout !
— La femme la plus puissante de la terre ne veut rien du tout, ironise Georges Farreck.
— Quel monde veux-tu léguer aux deux humains à qui tu vas bientôt donner la vie ? continue Eva.
