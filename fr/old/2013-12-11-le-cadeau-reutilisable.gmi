# Le Cadeau réutilisable
=> files/old/giftbox.jpg

Vous ne savez pas quoi offrir à Noël ? L’idée de vous entasser dans des centres commerciaux surchauffés vous donne des boutons ? Après une journée de shopping en ligne, vous vous sentez sale et souillé à cause de tous ces emails de spam qui remplissent déjà votre boîte et de tous ces suppléments livraisons/garanties/arnaques diverses qui surchargent votre carte bleue ?

=> /76-voyage-au-bout-de-l-enfer/ des centres commerciaux surchauffés

Pourquoi ne pas faire comme des milliers de hipsters à travers le monde et opter pour le ForeverGift ? Le ForeverGift, c’est la sensation de cet hiver 2015. Tout San Francisco se l’arrache ! Pourtant, beaucoup restent dubitatifs sur ce concept que certains n’hésitent pas à qualifier d’arnaque pure et simple.

« ForeverGift est un enfant de la crise » nous confie Pablo Sanchez, son concepteur. « Il est né dans une Espagne attachée à ses traditions mais où la crise économique et un taux de chômage de 50% dans la tranche des 20-30 ans rend le moindre achat extrêmement difficile. »

Pour être honnête, le Madrilène ne s’attendait pas à un tel succès. Il est parti d’un constat pourtant simple et maintes fois observé.

« Nous n’avons plus les moyens, ni économiques ni écologiques, de nous payer des biens matériels. Mais la technologie les rend également obsolètes. La vraie richesse, de nos jours, c’est de vivre léger, avec le matériel strictement nécessaire. Nous virtualisons tout ce qui peut l’être. À quoi bon offrir un livre ou une bande dessinée ? C’est un gaspillage de papier ! Quant à un CD ou un DVD, n’en parlons même pas. Du coup, afin de garder un budget cadeau raisonnable, nous avons tendance à offrir des babioles, des petits gadgets parfaitement inutiles. Ces cadeaux étant inutiles, ils sont eux-mêmes offerts l’année d’après ou revendus sur des sites de seconde main. Quand ils ne finissent pas tout simplement à la poubelle après un passage plus ou moins long dans un grenier poussiéreux. Malgré cela, il est impensable de ne pas offrir un cadeau à Noël ou lors d’un anniversaire ! C’est un symbole important. »

Un constant que les grands du web ont bien compris, tentant d’attirer le marché avec des bons d’achat sur Google Play, iTunes ou Amazon. Mais pour Pablo Sanchez, ce n’est pas la même chose. Un bout de papier avec un code ne remplace pas un cadeau. Sans compter que, la crise aidant, la plupart de ses amis se fournissent en livres électroniques, musique, films et logiciels sur les circuits pirates. Un peu moins pratique mais gratuit.

« Totalement à court d’idées pour le réveillon de Noël de l’année passée, sans un sous, j’ai décidé d’offrir à chacun une belle boîte avec un bel emballage. La boîte était vide mais j’avais collé une étiquette dessus. »

Cette étiquette est désormais devenue célèbre et est la marque de fabrique de ForeverGift :

« Ce cadeau est un cadeau réutilisable. Il peut vous sauver de l’embarras. Gardez-le précieusement afin de l’offrir lorsque vous n’aurez pas d’idée de cadeau. »

Alors qu’il avait peur de vexer ses amis, Pablo Sanchez est étonné de l’accueil fait à son cadeau réutilisable. Le succès est tel qu’à peine trois semaines plus tard, il reçoit, pour son anniversaire deux ForeverGift qu’il avait lui-même conçus et qui avaient déjà voyagé de main en main. Grâce à l’aide d’un ami, il monte un site de vente et commence à livrer ses ForeverGift dans le monde entier, tout en se diversifiant.

« Je me suis rendu compte que tout le monde n’avait pas la même idée de la valeur idéale d’un cadeau. Certains aiment offrir des petites choses à 5 € ou 10 €. D’autres, au contraire, considèrent qu’un cadeau doit être au minimum de 50 €. Du coup, j’ai créé des ForeverGift de différentes tailles, pour satisfaire tous les goûts. Inconsciemment, nous accordons beaucoup d’importance au volume d’un cadeau. C’est d’ailleurs, à mon avis, l’une des raisons de l’échec des cartes Google Play/iTunes/Amazon. »

Si le succès commercial est au rendez-vous, les critiques s’élèvent. Beaucoup voient en Pablo un arnaqueur qui vend des boîtes vides à 50 €. D’autres tentent de copier le concept.

« Je ne suis pas un arnaqueur. Les gens savent très bien ce qu’ils achètent, il est marqué en grand sur le site que les boîtes sont vides. Je ne suis pas un vendeur de boîtes : je vends un service, le confort de ne plus devoir se prendre la tête avant de faire un cadeau. D’ailleurs, n’importe qui peut faire la même chose avec une boîte et de l’emballage. Mais le logo ForeverGift a acquis de l’importance. Lorsqu’on reçoit un ForeverGift, on sait que, directement ou non, la personne qui nous l’offre a fait une dépense. Si vous recevez une bête boîte en carton avec une copie de ma lettre, cela fait un peu radin. »

Le coût d’un cadeau serait donc un élément essentiel. ForeverGift ne serait que la cristallisation de ce concept sociétal.

« Pendant des millénaires, les biens matériels ont été un signe de richesse, poursuit Pablo. Ces dernières décennies ont vu un véritable bouleversement de nos codes de valeur. Lorsque vous voyez une maison de milliardaire à la télé, ce qui frappe c’est l’espace, le vide, la place. Si par contre vous voyez une maison remplie à ras bord d’objets et de biens matériels, vous savez inconsciemment que vous êtes chez des gens relativement pauvres. Un bien demande de l’entretien, de la place. Il faut de plus en plus payer pour s’en débarrasser. La véritable richesse, c’est le vide. Au fond, ForeverGift, c’est un symbole, c’est le cadeau du 21e siècle. »

ForeverGift, idée du siècle ou arnaque ?

« Si vous pensez que c’est une arnaque et que vous recevez un ForeverGift, mettez-le au recyclage directement. Tant pis. Mais vous verrez. Vous serez très tenté de le garder pour une occasion où vous n’aurez pas d’idée. À ce moment-là, vous changerez peut-être d’avis ! »

Photo par Asenat29. Relecture par François Martin et Sylvestre.

=> http://www.flickr.com/photos/72153088@N08/6510934443 Asenat29
=> /no-proprietary-service.html François Martin
=> http://sylvestre.org Sylvestre
