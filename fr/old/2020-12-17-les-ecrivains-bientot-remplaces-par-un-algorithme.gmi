# Les écrivains bientôt remplacés par un algorithme ?
=> files/old/possessed-photography-U3sOwViXhkY-unsplash.jpg

Je suis convaincu que des romans très populaires seront bientôt écrits par des intelligences artificielles. Je n’exclus pas la possibilité que ce soit déjà le cas. Pourtant, je réalise que mon métier d’écrivain est essentiel et je n’ai pas peur de la concurrence des algorithmes. En fait, je l’accueille même à bras ouverts.

Car, sous le vocable « Intelligence Artificielle » se cache une réalité plus prosaïque : de simples algorithmes nourris par des quantités astronomiques de données et qui ne font que nous offrir une variation sur ces mêmes données. Tout ce qui est généré par l’intelligence artificielle est obsolète, ressassé. Il n’y a pas d’idée nouvelle. Parfois, on peut trouver une nouvelle manière de voir les choses anciennes, ce qui n’est pas dénué d’intérêt. Mais l’intelligence artificielle considère le passé comme un ensemble de règles immuables, infranchissables.

C’est la raison pour laquelle les intelligences artificielles sont régulièrement accusées de racisme, de sexisme. Elles ne sont que le reflet de notre société, un simple miroir. Elles ne pourront pas nous faire évoluer.

L’écrivain, au contraire, apporte sa sensibilité, sa vision, sa créativité. Son écriture est une relation humaine avec le lecteur, par delà la distance, par delà les siècles.

Le texte est porté par son contexte. Pour le lecteur, une connaissance même succincte de la vie de l’auteur transformera son interprétation personnelle, la manière dont il se l’appropriera. Le texte n’est, au fond, que le début d’une conversation. Une conversation qui peut parfois s’étendre par delà les siècles, englobant des dizaines d’écrivains, des milliers de lecteurs. Une conversation qui peut avoir l’apparence de s’achever, ne faisant que planter une graine invisible, graine qui germera des années plus tard sans que personne ne se souvienne de son origine.

Présent sur ce blog depuis plus de 16 années, j’ai la chance d’expérimenter l’impact de l’écriture sur la création de relations humaines. Depuis la lectrice que j’ai épousée, les lecteurs qui m’écrivent régulièrement à ceux qui m’avouent me lire depuis 10 ans et avoir l’impression de me connaitre intimement alors que je n’ai jamais entendu parler d’eux.

Avec la publication de Printeurs au format papier, je redécouvre cette intimité, cette proximité avec les lecteurs. Je me suis glissé dans leur lit le soir, je leur ai tenu compagnie plusieurs heures voire plusieurs jours. Je les ai empêchés de dormir. Ils ont pesté contre moi au petit matin, m’accusant d’être responsable de leur fatigue. Avant de me retrouver le soir même et de me glisser contre leur oreiller.

Cette relation intime, sensuelle, a forcément un impact sur moi. Je ne me sens pas dans la peau d’un écrivain dans sa tour d’ivoire dont les livres sont des blocs de marbre indépendants. Tout comme quand je suis sur scène et que je « sens » le public, que je m’adapte à lui, j’écris avec mes lecteurs, je me nourris de ces interactions impalpables qui vont complètement faire évoluer mes écrits futurs.

Le paroxysme de cette influence revenant à ma première lectrice, mon épouse. Non contente de remettre régulièrement en question mes croyances, mes habitudes par ses réflexions, elle n’hésite pas à me renvoyer à machine à écrire. À titre d’anecdote, elle a totalement transformé le chapitre 000110 de Printeurs pour en faire un texte qu’une femme peut lire sans pester sur la prétention des hommes blancs à connaitre la physiologie féminine. Elle me transforme et, par la même occasion, transforme mes futurs écrits.

Aucun algorithme d’intelligence artificielle ne peut ni ne pourra jamais évoluer de cette manière. Tout ce qu’un algorithme produit n’est qu’un artefact du passé. Si demain les écrivains venaient à disparaitre pour être remplacés par des logiciels, aucune nouvelle idée n’apparaitrait. Aucun risque littéraire. Aucune remise en question. Nous serions condamnés à lire et à relire toujours la même chose, à végéter dans le marasme de l’immobilité intellectuelle.

Certes, les algorithmes seront certainement meilleurs pour produire des textes « qui se vendent ». Après tout, c’est déjà le cas avec la musique. Mais ces écrits seront dans l’impossibilité de créer des liens humains. Tout simplement parce qu’un lien implique deux humains. Par définition.

À ce jeu, l’écrivain de best-seller, le philosophe académique ou le blogueur anonyme suivi par une poignée de lecteurs seront toujours meilleurs que le plus pointu des algorithmes. Leurs écrits sont des briques essentielles à la construction de l’humanité.

L’écriture est un métier magique, car la concurrence y est toujours la bienvenue. Les lecteurs s’enrichissent de leurs lectures, lisent de plus en plus, découvrent. Tout nouvel écrit, loin d’entrer en compétition avec les autres, est au contraire une porte d’entrée qui élargira le champ des lecteurs.

Le lecteur d’un seul livre se perd dans un fanatisme religieux. Le lecteur de plusieurs livres découvre graduellement la finitude de sa connaissance. À force de lire, il développe une identité, une analyse propre et un appétit insatiable. J’ai confiance que les lecteurs découvriront très vite les limites des textes générés par une intelligence artificielle.

Le seul réel danger, le seul ennemi des écrivains étant ce qui nous empêche de lire, ce qui nous pousse à passer du temps devant les écrans et les publicités plutôt qu’à réfléchir, méditer, apprendre. À consommer du contenu plutôt qu’à apprécier des idées.

Cet ennemi n’est pas seulement celui des écrivains. C’est celui de l’humanité tout entière !

Photo by Possessed Photography on Unsplash

=> https://unsplash.com/@possessedphotography?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText Possessed Photography
=> https://unsplash.com/?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText Unsplash
