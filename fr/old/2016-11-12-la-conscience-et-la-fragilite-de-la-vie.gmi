# La conscience et la fragilité de la vie
=> files/old/3123510430_799e9bca0f_o.jpg

Si on prend la peine d’y réfléchir, notre vie est un incroyable équilibre, une formidable symbiose entre des milliards de cellules.

Chacune est unique, périssable, remplaçable et pourtant toutes sont dépendantes des autres pour exister. Les cellules n’existent que grâce à l’entité supérieure qu’elles forment : un être humain !

Que cette fragile harmonie entre des milliards d’individus se rompe et, aussitôt, c’est le corps tout entier qui souffre. Que quelques cellules décident de ne plus collaborer, d’entrer dans une égoïste croissance incontrôlée et c’est la personne entière qui est atteinte d’un cancer.

Lorsque le déséquilibre devient trop grand, que la symphonie se transforme en fausses notes, vient l’heure de mourir.

La mort signifie la fin de la coopération. Les cellules vivent encore mais ne peuvent plus compter sur les autres. Elles sont condamnées à disparaître, à pourrir.

Le corps devient rigide, froid. Les cellules qui le composent s’éteignent petit à petit. Certaines ne veulent pas l’accepter mais il est trop tard. La conscience a disparu, le corps est condamné.

L’agonie se transforme en repos, le râle en silence, les convulsions en immobilité. La moite et étouffante chaleur est devenue un courant d’air. La vie s’en est allée.

Attendez !

Si on prend la peine d’y réfléchir, l’humanité est un incroyable équilibre, une formidable symbiose entre des milliards d’individus.

Que quelques personnes décident de ne plus collaborer, d’amasser au détriment des autres et c’est la planète entière qui est atteinte d’un cancer.

Mais contrairement à un corps, l’humanité n’est pas encore arrivé au stade de la conscience. Nous pouvons encore la sauver, soigner les terribles cancers qui la rongent.

Si nous pouvons compter l’un sur l’autre, si nous pouvons collaborer, considérer chaque être humain comme une cellule indispensable d’un seul et unique corps, alors, l’humanité survivra.

Pour survivre, nous n’avons sans doute pas d’autre choix que de créer et devenir la conscience de l’humanité.

À ma mamy, décédée le 15 octobre 2016. Photo par Sarahwynne.

=> https://www.flickr.com/photos/goddess-arts/3123510430 Sarahwynne
