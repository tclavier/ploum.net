# Printeurs 26
=> files/old/13479433474_b967478f99_z.jpg

Le travailleur 689, devenu le gardien G89, s’est vu confier la mission d’embarquer avec le chargement afin de convaincre les commanditaires d’envoyer du ravitaillement. Aucun garde ni contre-maître ne souhaitant l’accompagner jusqu’au vaisseau, il a pénétré seul dans le sas.

Respirer ! Je dois respirer ! J’ai le souffle coupé comme lorsque je reçois des coups de pieds dans le ventre. Malgré mes paupières fermées, mes yeux scintillent et restent éblouis. Que se passe-t-il ? De quel incroyable mystère suis-je le témoin ? À ma gauche, le vaisseau de transport, tel que je l’imaginais. Des bras robotisés sont en train de le remplir de caissons. Ils contiennent certainement la marchandise que nous produisons. Sur ma droite, je m’appuie sur la paroi lisse et métallique qui prolonge le sas dont je viens de m’extirper. À travers les gants de ma combinaison, la solidité inébranlable me rassure.

Mais devant ! Devant !

Est-ce pour cette raison que les contre-maîtres ont peur du couloir ? Les arguties politiques afin de ne pas envoyer l’un d’entre eux n’étaient-elles que de simples excuses pour éviter d’être confronté à ce qui se dresse devant moi ? L’horreur, la joie ultime. La peur et le soulagement. L’infini. Le néant.

Je titube tout en tentant de m’approcher du vaisseau. La mission ! Je dois me concentrer sur la mission ! Mais cet infini, ce gigantesque univers peuplé d’une scintillante noirceur m’aspire, m’attire. J’ai peur. Je n’ai plus peur. Une onde traverse mon corps, un sentiment inconnu fait frissonner mes membres. Des images inconnues me traversent l’esprit, des souvenirs, des sensations. Je suis encore un enfant et 612, le vieux, raconte des histoires. Je n’en comprends pas un traître mot. Je ne sais pas de quoi il parle. Mais la base de ma nuque se détend, frémit. Mes yeux se plissent, les coins de ma bouche sont irrémédiablement attirés vers le haut. L’air qui sort de mes poumons me semble plus chaud, plus doux, enrobant, envoûtant.

Le vieux ! Il savait ! Il tentait de nous préparer. Il se tient à présent devant le vaisseau et me fait signe d’avancer d’un geste apaisant. Mais pourquoi me montre-t-il l’infini en souriant ? Pourquoi son doigt est-il dressé vers le vide angoissant ? Je ne comprends pas. Les images du passé et du présent s’entrechoquent.

Je tombe à genoux. Un mot du vieux me revient à l’esprit. Un mot bizarre, incompréhensible. Beau !
— C’est… c’est beau, articulé-je en rampant vers le vaisseau.
Le vieux me regarde. Il continue à parler mais aucun son ne me parvient. Ses lèvres s’agitent dans le silence absolu du vide. Il s’arrête et se tient immobile. Et puis, soudain, sa voix surgit de l’éther, résonnant à travers mes souvenirs, se mélangeant au bruit des machines et de l’usine, décor sonore immuable et inévitable de mon enfance.
— Nous ne sommes qu’une poussière dans un infini. Nous volons à travers l’espace sur une simple pierre. Cet espace est le plus beau spectacle qu’il ne m’aie jamais été donné l’occasion de contempler.
— 612, pourrons-nous aussi voir un jour ce fameux espace ?
— G89 ! G89 ! Je crois que nous avons perdu G89 !
— Oui, vous pourrez un jour le voir ! L’un d’entre vous vous libérera et vous ouvrira les portes de l’espace.
— G89 ! Je vous avais bien dit que c’était une mauvaise idée d’utiliser un ancien travailleur. Ils ne sont pas mentalement aptes.
— Mais, 612, cet espace infini, il doit être très effrayant, non ?
— Oui. Et beau à la fois. La liberté n’est belle et désirable que parce qu’elle est terrifiante.
— G89 !

En titubant, je m’appuie sur le vaisseau pour me relever. Dans le casque de mon scaphandre, mes oreilles grésillent.
— G89 ! Bon, il va falloir désigner l’un d’entre-nous !
— …
— G89 !
— Ou… oui, balbutié-je. Je… je suis arrivé au vaisseau.

À l’autre bout de la communication, un silence s’est installé. La voix reprend, incertaine, hésitant entre le soulagement et l’inquiétude.
— Vous… Tu es arrivé au vaisseau ?
— Ou… oui chef !
— Je… C’est bien G89 ! Maintenant embarque dans le compartiment de voyage.
— Il va étouffer ! Tout un voyage dans un espace aussi exigu !
— Très chère, pour un ancien travailleur, cet espace représente un luxe inimaginable. Ne vous inquiétez donc pas pour lui !
— Tu es installé G89 ?

À travers les tremblements de leurs voix, j’ai reconnu les accents familiers et encourageant de la peur. Ma chère et indispensable peur, mon guide, mon compagnon. J’ai senti la volonté et l’adrénaline affluer dans mes muscles. D’un pas assuré, j’ai franchi les derniers mètres qui me séparent de l’ouverture dans la coque du vaisseau. Grâce à un effort de volonté suprême, j’ai réussi à détacher mes yeux de l’espace et de l’infini. Le vieux a arrêté de sourire. Il a ébauché un geste dans ma direction mais je l’ai ignoré. Je ne l’ai même pas bousculé, je me suis contenté de continuer mon chemin, de nier son existence. Un vent de panique a soufflé dans son regard. Sa main s’est tendue. J’ai continué et il a disparu de ma conscience.

Suivant les instructions, j’ai refermé la porte du vaisseau derrière moi. Après m’être glissé avec difficulté dans l’étroit compartiment, je me suis sanglé contre une paroi. J’ai à peine assez d’espace pour respirer. Sans même tendre le bras, je peux toucher le hublot de verre qui s’ouvre devant mes yeux et qui plonge à travers l’espace. L’espace !
— Tu es sanglé G89 ?
— Oui chef !
— N’oublie pas ta mission ! Tu dois convaincre les commanditaires d’envoyer du ravitaillement. Il y va de leur propre intérêt !
— Oui chef !

Sans avertissement, un choc violent m’enfonce dans la paroi. Un grondement sourd agite la structure du vaisseau. Je suis parti ! Les moteurs me propulsent à travers l’espace. Une force d’attraction me fait glisser vers la gauche. Tournant la tête, je constate que le vaisseau est en train d’effectuer un demi-tour. En me contorsionnant, j’arrive à apercevoir sous mes pieds un informe et gigantesque caillou grisâtre. L’usine. Mon monde. Mon univers. Et encore, pensé-je, je n’ai jamais vécu que dans une toute petite partie. Cette grosse pierre contient des dizaines d’usines, chacune contrôlée par son propre contre-maître. Le vieux appelait l’ensemble “l’astéroïde”. L’astéroïde vient de disparaître sous moi et j’ai l’intime conviction que jamais je ne le reverrai.

Une boule brillante m’éblouit un instant. Après quelques secondes, je distingue du blanc, du bleu et du jaune se découpant sur sa surface. La voix du vieux me parvint, affaiblie.
— La Terre est la plus belle planète. Les humains s’évertuent à la détruire et à la quitter. Mais dès qu’ils en sont éloignés, ils n’aspirent qu’à y revenir. La Terre est belle ! Belle !

Je me frappe la tête et les écouteurs du casque pour éteindre ce dernier fantôme qui me rattache à l’astéroïde, à l’usine. Je me sens mal. Cette boule bleue et blanche brillante… Non, je ne dois rien éprouver ! Je dois me concentrer sur mon super-pouvoir, oublier tout le reste !
— L’un d’entre vous verra la Terre. Il la sauvera. L’Élu !
Je me tappe violemment la tête contre la paroi.
— Ta gueule le vieux ! Ta gueule ! Ta gueule !

Photo par European Southern Observatory.

=> https://www.flickr.com/photos/esoastronomy/13479433474 European Southern Observatory
