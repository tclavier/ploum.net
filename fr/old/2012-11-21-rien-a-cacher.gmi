# Non, je n’ai rien à cacher

On entend régulièrement la formule « Celui qui n’a rien à se reprocher n’a rien à cacher ». Cet aphorisme devrait vous rassurer lorsque vous vous inquiétez de l’invasion des technologies dans votre vie.

Je voudrais illustrer la tromperie de cette phrase au travers d’une anecdote réelle, vécue à une époque où Internet était si rare qu’on pouvait le considérer comme essentiellement inexistant. Un temps où les affres de la puberté trouvaient encore un exutoire sur les pages en papier glacé des magazines.

=> http://falkvinge.net/2012/07/19/debunking-the-dangerous-nothing-to-hide-nothing-to-fear/ la tromperie

=> ../files/old/hide_seek.jpg Un chat se cachant à moitié

Adolescent dans une grande école catholique, je me fais un jour approcher par un condisciple.
— Lio, il faut que je montre un truc trop drôle !

Ce camarade me révèle qu’il a trouvé, dans une revue porno, une photo ressemblant fortement à un de nos éducateurs. Intrigué, je demande bien sûr à voir la photo en question. Publiée dans la rubrique « courrier des lecteurs », elle représente un homme nu en érection. Contrairement aux autres photos de cette rubrique, le visage n’est pas flouté. Et la ressemblance est, il est vrai, frappante.

Éclatant de rire, nous avons vite fait de nous adjoindre une petite troupe goguenarde autour de la photo. Je remarque alors une chevalière très particulière et un pendentif en or au cou de notre exhibitionniste.

Ni une ni deux, la petite troupe décide de passer « discrètement » devant le bureau des éducateurs pour vérifier et, stupeur, notre éducateur porte la même chevalière, le même pendentif. Il n’y a donc plus aucun doute.

Quand on est adolescent, ce genre d’aventures est grisante. Mon condisciple prend peur et décide de ne plus montrer l’image. Après tout, il est coupable d’avoir emmené un magazine porno dans l’enceinte de l’école. Et tout le monde lui pose des questions sur la possession du dit magazine.

De mon côté, intrépide et inconscient, je lui demande de me découper la photo et la fait passer sous le manteau dans l’école. C’est rigolo. Les élèves jasent.

Le lendemain, l’éducateur n’est pas là. Il ne reviendra jamais.

Cet éducateur avait-il quelque chose à se reprocher ? Non, il échangeait une photo où il apparaissait nu avec un public majeur consentant et demandeur. C’était tout à fait légal et on ne peut lui reprocher cela.

Par contre, le magazine est arrivé dans les mains d’un lecteur non-majeur. La personne ayant permis cela est donc coupable car la photo, bien que parfaitement légale, mine l’autorité de l’éducateur. De plus, elle va à l’encontre des valeurs morales affichées par l’employeur. Deux raisons qui font qu’il était impossible de garder l’éducateur en poste.

Il est donc important de souligner un point : le problème n’est pas que l’éducateur aie posé pour des photos pornographiques ni même qu’elles aient été publiées mais bien que les élèves subordonnés à l’éducateur en prirent connaissance. Ce n’est pas le fait ni l’information qui pose problème mais bien que certaines personnes particulières aient accès à cette information.

La phrase « Celui qui n’a rien à se reprocher n’a rien à cacher » est donc fausse car ce n’est pas vous qui choisissez ce que vous vous reprochez. C’est le public qui a tout pouvoir pour décider ce qu’il va décider de vous reprocher. Afin d’illustrer la nécessité de la vie privée, on prend souvent l’exemple du régime totalitaire qui contrôle les citoyens. Mais, plus pragmatiquement, que penser de la relation élèves/enseignant ou employeur/employé ? Un enseignant doit de nos jours avoir une image publique à laquelle tout élève, avec sa morale propre, ne puisse rien avoir à reprocher ! Vous savez comme moi qu’un adolescent trouvera toujours de quoi se moquer. Et si ce n’est pas le cas, les parents s’en chargeront au premier échec de leur génie.

Pendant longtemps, la diffusion de l’information était limitée de manière physique. On pouvait donc se permettre de semer des tranches de vie privée un peu partout. La (mal)chance que cela arrive au mauvais public était minime. C’est le principe de la sécurité par l’obscurité : on espère que les mauvaises personnes ne vont pas tomber sur la mauvaise information.

=> http://fr.wikipedia.org/wiki/S%C3%A9curit%C3%A9_par_l%27obscurit%C3%A9 sécurité par l’obscurité

La technologie, les réseaux sociaux, la reconnaissance de visage ne font que rendre cette malchance de plus en plus probable. À tel point qu’il faut la considérer comme évidente. Face à cela, nous devenons tous des personnages publics. Nous allons tous devoir apprendre à gérer notre image. D’une certaine manière, les choses se simplifient : le facteur chance disparaît. Mais la société va également évoluer. Les codes moraux risquent d’être bouleversés.

=> http://www.zdnet.com/porn-companies-adopt-facial-recognition-technology-encourage-instagram-photos-7000007631/ reconnaissance de visage
=> /noyez-le-poisson devoir apprendre à gérer notre image

Vous vous demandez certainement si je n’ai jamais eu le moindre remord. Après tout, j’ai très probablement causé le renvoi de cet éducateur innocent.

Et bien non. Il avait compté sur la chance de ne pas être vu par un de ses élèves, il avait négligé de se flouter le visage, il avait joué. Il avait perdu. Déjà à l’époque, je n’éprouvais aucune pitié pour ceux qui reposaient sur la sécurité par l’obscurité.

Photo par Aftab Uzzaman

=> http://www.flickr.com/photos/72093892@N00/2640901551 Aftab Uzzaman
