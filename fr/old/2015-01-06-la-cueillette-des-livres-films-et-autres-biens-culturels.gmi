# La cueillette des livres, films et autres biens culturels
=> files/old/164453373_a508b978a5_z.jpg

N’avez-vous jamais soupiré en refermant un livre parce que vous ne saviez pas quoi lire ensuite ? N’avez-vous jamais eu envie de voir des dizaines de films classiques pour vous retrouver, le soir avec des amis, sans aucune idée d’un film à suggérer si ce n’est le blockbuster dont la publicité passe en boucle dans tous les médias ?

Je suis un consommateur particulièrement vorace de livres, de films et de bandes dessinées. Historiquement, mon choix se limitait à ma bibliothèque, laquelle était enrichie ponctuellement par les achats, les cadeaux et les découvertes aléatoires dans les bouquineries.

À l’heure du web, la bibliothèque d’un pirate comme moi est virtuellement illimitée. Et, pourtant, je ne regardais que très rarement des « grands films ». Je lisais des livres « faciles » en série. L’immensité de ma bibliothèque illimitée me terrorisait et me faisait me replier dans mon petit univers connu.

=> /je-suis-un-pirate/ un pirate comme moi
=> /accueillons-labondance/ bibliothèque illimitée

J’ai donc décidé d’appliquer la consommation cueillette à ma consommation culturelle.

Étape 1 : la cueillette

Après moultes essais de solutions diverses, j’ai fini par établir mon panier de cueillette sur le site SensCritique, sous forme d’une « liste d’envies ».

=> http://www.senscritique.com/ SensCritique

J’y marque comme « Envies » tout film, livre, bande dessinée ou série télévisée qui m’est conseillé par une connaissance ou par un article.

Mais là où SensCritique se démarque, c’est par sa capacité de compiler des sondages. Par exemple, chaque membre du site qui le souhaite sélectionne ses 10 films de SF préférés. Les résultats sont agrégés et une liste des meilleurs films de SF est publiée. Il est également possible de « suivre » des utilisateurs qui deviennent nos « éclaireurs ». Vous pouvez, par exemple, m’ajouter comme éclaireur.

=> http://www.senscritique.com/top/resultats/Les_meilleurs_films_de_science_fiction/272590 meilleurs films de SF
=> http://www.senscritique.com/Lionel_Dricot m’ajouter comme éclaireur

Toutes ces fonctionnalités, au final, ne servent qu’à une et une seule chose : ajouter des éléments à ma liste d’envies. Élargir mon domaine de cueillette ! Mais tout autre panier de cueillette peut bien entendu faire l’affaire.

Étape 2 : la consommation

Lorsque j’ai envie de regarder un film, je consulte ma liste d’envies. Lorsque j’ai fini un livre, je consulte ma liste d’envies. Lorsqu’un ami me demande quelle bande dessinée me ferait plaisir, je consulte ma liste d’envies.

Parfois, je retrouve des éléments qui sont dans ma liste pour une raison que je n’explique pas. Je n’ai pas envie de les consommer. Les critiques semblent généralement négatives, surtout chez mes éclaireurs. Alors je n’insiste pas : je supprime cet élément sans remord.

Et si, au cours d’une soirée cinéma, on me propose un film qui n’était pas dans mes envies, je me pose consciemment la question : pourquoi n’y était-il pas ? Et l’ajouterais-je à mes envies ? Si non, ne puis-je pas proposer une alternative ?

Étape 3 : l’action

Après la consommation, je note l’œuvre sur SensCritique et, parfois, je me risque à rédiger une critique. Le but n’est pas tant d’être lu que de marquer mon passage et de pouvoir, dans quelques années, me remémorer une œuvre particulière.

À cette étape, j’aimerais également pouvoir remercier le ou les auteurs, s’ils sont encore vivants. Malheureusement, les artistes acceptant des prix libres sont encore de rares exceptions. Dommage !

=> /a-mes-ecrivains-favoris/ les artistes acceptant des prix libres

Au final

Tout cet effort mais pour quels résultats ? Des résultats tout simplement inespérés !

Depuis que j’applique cette méthode, mon quotient de « cinéphilité » a effectué un bond magistral. J’ai enfin pris le temps de regarder de vieux classiques que « je me devais de voir » depuis des années. Mieux : j’ai pris énormément de plaisir à les découvrir et j’ai pris goût aux films de qualité.

Le fait de noter négativement un film parce que j’estime avoir perdu mon temps est une sorte de rappel, de marqueur inconscient. J’ai envie de regarder des films que je vais noter positivement ! Une fois désintoxiqué, je réalise à quel point les blockbusters décérébrés sont ennuyeux et écœurants.

L’effet est identique sur les livres : je lis beaucoup plus de classiques qu’auparavant. J’éprouve un besoin de qualité, de profondeur. Si je ne referme pas un livre en m’en sentant grandi, je suis déçu.

Un petit blockbuster ou un petit polar pour se vider l’esprit ? Non merci ! Je ne veux pas me vider l’esprit, au contraire, je veux le remplir, le construire, le travailler, le faire progresser ! Je ne veux pas entraîner mon cerveau à ne plus réfléchir ni l’habituer à la bêtise ! Le cerveau est un muscle qui s’atrophie quand on ne s’en sert pas.

Après les médias, la consommation cueillette s’est donc révélée particulièrement adaptée à la culture. Il ne manque que les spectacles, concerts et expositions ! Mais pourrait-on appliquer cette méthode à des biens matériels ? La réponse au prochain épisode !

Photo par Matt McGee. Relecture par le gauchiste.

=> https://www.flickr.com/photos/pleeker/164453373 Matt McGee
=> http://gauchiste.fr le gauchiste
