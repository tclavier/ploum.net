# Le minibus et la discothèque
=> files/old/minibus.jpg

Tous les matins, Gérard se mettait au volant de son minibus et parcourait les rues de la ville, depuis les lointains faubourgs aux quartiers chics du centre-ville. Lorsqu’un piéton lui faisait un signe de la main, Gérard s’arrêtait, embarquait le nouveau passager et continuait sa tournée. Lorsqu’il voulait descendre, étant arrivé à destination, le passager tapait sur l’épaule de Gérard et lui glissait un billet dans la poche.

Gérard était très fier de son métier. « Je permets à tout le monde de se déplacer, disait-il. Une vraie démocratie doit offrir à chacun le droit de se rendre où il veut. Sinon, ce n’est qu’une prison aux barreaux dorés. » Petits et grands, jeunes et vieux, tous bénéficiaient des services de Gérard.

À quelques kilomètres de la ville, dans une banlieue mal fréquentée, Janine avait ouvert une discothèque. Afin de faire venir du monde, elle contacta Gérard.

— Je te propose de mettre sur ton véhicule une grande affiche pour ma discothèque.
— Pourquoi ferais-je cela ? demanda Gérard.
— Sur l’affiche sera placé un code donnant droit à un prix d’entrée réduit dans ma discothèque. Pour toute personne utilisant ce code, tu toucheras 10% du prix de l’entrée. Et puis, ça ne te coûte rien.
— Et tu exiges autre chose de moi ?
— Absolument pas. Tu es entièrement libre de continuer à faire ce que tu as toujours fait. Tu vas où tu veux, comme tu veux.
— Alors, marché conclu !

À vrai dire, la publicité pour la discothèque de Janine ne modifia en rien le travail de Gérard. Tout au plus cela lui permettait de mettre un peu de beurre dans les épinards.

Et puis, comme toujours, le monde changea. Les mentalités évoluèrent. Le réchauffement planétaire avait transformé le climat de la ville qui baignait à présent dans un soleil permanent. Les habitants préféraient éviter autant que possible tout véhicule à essence. Le vélo et la marche à pieds redevenaient extrêmement populaires. Quelques habitués continuaient à faire appel à Gérard mais ils avaient tendance à oublier de verser un billet ou une petite pièce. Ils ne voyaient d’ailleurs pas pourquoi il était nécessaire de payer un trajet qu’ils auraient aussi bien pu faire à vélo.

Voyant le prix de l’essence augmenter et son compte en banque diminuer, Gérard s’assit un soir à sa table pour faire ses comptes. Il fut étonné de constater que ses revenus du vendredi étaient toujours nettement supérieurs à ceux du reste de la semaine. « Bizarre ! murmura-t-il. » Il comprit soudain. Le vendredi, il tournait dans le quartier mal-famé où se trouvait la discothèque de Janine. Il n’embarquait jamais beaucoup de monde le vendredi mais les habitants devait sans doute le voir passer et noter le code de réduction qu’ils utiliseraient le soir à la discothèque.

Ayant réalisé cela, Gérard décida de modifier sa tournée du mardi. Au lieu d’embarquer les personnes âgées de la maison de retraite pour les emmener au centre culturel, il décida d’aller tourner dans le quartier de la discothèque. Les personnes âgées étaient de toutes façons chiches et le mardi était son jour le moins rentable. Il n’embarqua pas un seul client ce jour-là et rentra chez lui, déçu.

Le lendemain, à sa grande surprise, il constata que Janine lui avait envoyé de l’argent. Plus d’argent que n’importe quel mardi !

Il décida alors de remplacer sa tournée du mercredi (les élèves de maternelle qui vont à la plaine de jeu et qui ne paient qu’en boutons de culotte) et sa tournée du jeudi (les travailleurs de l’usine qui vont chercher des frites sur le temps de midi). La stratégie s’avéra payante. Certes, ce n’était pas le Pérou. Mais, au moins, Gérard pouvait continuer à vivre. Mieux : en restant immobile quelques dizaines de minutes à différents endroits stratégiques du quartier près de la discothèque, il économisait de l’essence sans pour autant diminuer son impact sur les habitants. C’était d’ailleurs plus facile pour eux de noter le code. Il pouvait même partir manger à midi en laissant son véhicule bien en vue !

Après quelques semaines, Gérard avait complètement abandonné ses tournées traditionnelles. Il se contentait de tourner en rond dans le quartier de la discothèque. Une présence permanente était devenue indispensable car il devait lutter contre la concurrence. D’autres véhicules avaient fait leur apparition. Ils portaient des codes différents pour la discothèque de Janine. Afin d’attirer l’attention sur lui, Gérard équipa son minibus d’un gyrophare et d’une sirène. Lorsqu’il croisait un piéton, il donnait un grand coup de klaxon et montrait le code désormais inscrit en lettres de néon clignotantes sur chaque côté du véhicule.
La dernière fois que je vis Gérard, il buvait une bière avec Janine à la terrasse d’un café. Gérard était amaigri, les traits tirés. Il semblait quémander quelque chose à Janine. Je m’approchai et, après les présentations d’usage, posai les questions qui me brûlaient les lèvres depuis plusieurs semaines.

— Dis-moi Janine, tu n’as pas l’impression de pervertir le travail de Gérard ?
— Mais pas du tout ! Je n’ai aucun contrôle sur ce qu’il fait, il est entièrement libre. C’est plus pour moi une manière de le soutenir. Son travail est important.
— En quoi est-ce tellement important ?

Gérard me darda un regard noir. Il posa son verre brutalement et déclara avec force :
— Je permets à tout le monde de se déplacer. Je suis un pilier de la démocratie car une vraie démocratie doit offrir à chacun le droit de se rendre où il veut. Sinon, ce n’est qu’une prison aux barreaux dorés. Si je disparais, c’est la fin de la démocratie !

J’acquiesçai poliment, réglai mes consommations et enfourchai mon vélo pour rentrer chez moi.

Photo par JosEnrique.

=> https://www.flickr.com/photos/16871926@N00/9378627908 JosEnrique
