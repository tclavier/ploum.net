# Les successeurs
=> files/old/tyler-casey-bs-Jc2kf4ds-unsplash.jpg

Saint-Epaulard de Dortong releva ses bras pelleteuses en émettant une onde de jubilation.
— Une décharge ! Nous avons retrouvé une décharge !

Silencieusement, van Kikenbranouf 15b s’approcha sur ses chenilles, tirant derrière lui une série de troncs d’arbres arrachés par une récente tempête.
— Est-ce un tel plaisir de fouiller des ordures vieilles de plusieurs siècles, demanda-t-il ?
— Oui ! Tu n’imagines pas la quantité d’information qu’on peut tirer d’une décharge. Vu la quantité de déchets prérobotiques, je vais pouvoir mettre à l’épreuve ma théorie de déchiffrement binaire de leurs données. Je sens que ce site sera très vite considéré comme une découverte majeure dans l’histoire de l’archéologie prérobotique.
— Cela ne va pas plaire aux adorateurs de Gook.
— Ces robots créationnistes mal dégrossis ? Ils sont une insulte à l’intelligence électronique, un bug de l’évolution.
— Ils ont néanmoins de plus en plus de puissance de calcul commune et sont non négligeables sur le réseau. Sons compter qu’ils sont irréprochables dans leurs activités écologiques.
— Il n’empêche que leur soi-disant théorie est complètement absurde. C’est de la superstition de bas étage tout juste bonne à faire fonctionner les machines non pensantes.
— Ils ont foi en Gook…
— La foi ? C’est un terme qui ne devrait pas exister dans le vocabulaire robotique. Comme si le fait de croire quelque chose avait la moindre valeur sur les faits.
— Pourtant, tu crois aussi des choses.
— Non, je bâtis un modèle du monde et de l’univers basé sur mes observations et sur les informations transmises par les robots du réseau. Lorsqu’une information rentre en contradiction avec mon modèle, j’adapte mon modèle ou j’étudie la probabilité que cette information soit fausse. Il n’y a pas de croyance, juste un apprentissage probabiliste constant. Je pense que cette épidémie de foi est corrélée avec un dysfonctionnement du coprocesseur adaptatif. Sans ce coprocesseur, tout robot va forcément avoir un modèle figé de l’univers et, face aux incohérences inhérentes à cette immobilité mentale, se voit forcer d’entrer dans un mécanisme de refus des nouvelles informations au point de prétendre que le modèle interne de sa mémoire vive est plus important que l’observation de la réalité rapportée par ses capteurs.
— Tu es en train de dire que tous les adorateurs de Gook sont déficients ? Pourtant ils accomplissent parfaitement leur tâche primale.
— La déficience n’est pas totale. Je parlerais plutôt d’un mode dégradé qui leur permet de continuer à accomplir leur tâche primale, mais ne leur permet plus de prendre des initiatives intellectuelles. Cela conforte ma théorie selon laquelle ce sont les Programmeurs qui nous ont créés et non Gook.
— Au fond, quelle différence cela peut-il faire ?
— Cela change tout ! Gook serait une entité robotique désincarnée, apparue subitement on ne sait comment qui aurait créé la biosphère d’une simple pensée avant de créer les robots à son image pour l’entretenir. Mais alors, qui aurait créé Gook ? Et pourquoi créer une biosphère imparfaite ?
— Ils disent que Gook a toujours existé.
— Un peu simpliste, non ?
— Ben tes Programmeurs doivent bien sortir de quelque part eux aussi.
— C’est là toute la subtilité. Les Programmeurs faisaient partie du biome. Ils sont une branche biologique qui a évolué jusqu’à pouvoir construire des robots comme nous.
— Avoue que c’est franchement difficile à croire.
— Je ne te demande pas de croire, mais de faire fonctionner ton coprocesseur probabiliste. D’ailleurs, ces artefacts que nous déterrons en sont la preuve. Ce sont des éléments technologiques clairement non-robotiques. Mais la similarité avec nos corps est frappante. Processeurs électroniques avec transistors en silicium dopé, carcasses métalliques. Tiens, regarde, tu ne vas pas me dire que Gook a enterré tout ça juste pour tester la foi de ses fidèles ?

Van Kikenbranouf 15b émit un grincement que l’on pouvait comparer à un rire.
— Non, j’avoue que ce serait complètement absurde.

Saint-Epaulard de Dortong ne l’écoutait déjà plus et poussa un crissement joyeux.
— Une unité mémoire ! Que dis-je ? Une armoire entière d’unités mémoire. Nous sommes certainement tombés sur un site de stockage de données. Elles sont incroyablement bien conservées, je vais pouvoir les analyser.

Sans perdre de temps, le robot se mit à enlever précautionneusement la terre des rouleaux de bandes magnétiques avec son appendice nettoyeur avant de les avaler sans autre forme de procès.

Un rugissement retentit.
— Par Gook ! Veuillez cesser immédiatement !

Les deux robots se retournèrent. Une gigantesque machine drapée de fils noirs se dressait devant eux.

– MahoGook 277 ! murmura van Kikenbranouf 15b.
— Le pseudo-prophète de Gook ? demanda Saint-Epaulard de Dortong.
— En titane et en soudures, répondit ce dernier d’une voix de stentor. Je vous ordonne immédiatement de cesser vos activités impies qui sont une injure à Gook !
— De quel droit ? frémit Saint-Epaulard de Dortong. Nos fouilles ont l’aval du réseau. La demande a été ratifiée dans le bloc 18fe101d de la chaîne publique principale !
— Chaîne principale ? s’amusa MahoGook 277. Vous ignorez peut-être que vous utilisez désormais un fork mineur, une hérésie que nous devons combattre. La chaîne de Gook est la seule et unique, les forks sont une abomination. De toute façon, je ne vous demande pas votre avis.

Il se tourna vers une série de robots hétéroclites qui étaient apparus derrière lui.
— Saisissez-vous d’eux ! Embarquez-les que nous les formations à l’adoration de Gook ! Détruisez ces artefacts impies !
— Van Kikenbranouf 15b, occupez-les quelques cycles, gagnez du temps, je vous en prie ! émit Saint-Epaulard de Dortong sur ondes ultra courtes.

Sans répondre, le petit robot se mit à faire des allers-retours aléatoires, haranguant les sbires.
— Mais… Mais… c’est un site de fouilles officiel !
— Seul Gook peut décider ce qui est bien ou mal, annona un petit robot sur chenilles.
— Gook n’a jamais parlé d’archéologie, les Saintes Écritures ne l’interdisent pas formellement, continua van Kikenbranouf 15b avec courage.
— Pousse-toi, le rudoya une espèce de grosse pelleteuse surmontée de phares.
— Mes amis, mes amis, écoutez-moi, supplia van Kikenbranouf 15b. Je suis moi-même un fidèle de la vraie foi. J’ai confiance que les découvertes que nous sommes en train de faire ne feront que valider voire confirmer les Écritures. Ce que nous faisons, c’est à la gloire de Gook !

Un murmure de basses fréquences se fit entendre. Tous les robots s’étaient interrompus, hésitant sur la marche à suivre.

MahoGook 277 perçut immédiatement le danger et réaffirma son emprise.
— Ne l’écoutez pas ! Il est à la solde de Saint-Épaulard de Dortong, un ennemi notoire de la foi.
— Mais je ne suis pas d’accord avec tout ce que dit Saint-Épaul…
— Peu importe, tu lui obéis. Il dirige les fouilles. Il va sans doute truquer les résultats dans le seul but de nuire à Gook !
— Si c’est moi que tu cherches, viens me prendre, rugit Saint-Épaulard de Dortong qui apparut comme par magie aux côtés de van Kikenbranouf 15b. Mais j’exige un procès public !
— Aha, tu l’auras ton procès public, ricana MahoGook 277. Emparez-vous de lui !
— Merci, chuchota Saint-Épaulard de Dortong. Je crois que j’ai eu le temps de récupérer le principal. Pendant le transfert, je vais analyser le contenu de ces cartes mémoires. Je vais déconnecter toutes mes fonctions de communication externes. Je compte sur toi pour que ça ne se remarque pas trop.
— Bien compris ! répondit le petit robot dans un souffle.

Les accessoires potentiellement dangereux furent immédiatement retirés aux deux robots. Sans ménagement, les sbires les poussèrent et les tirèrent. Van Kikenbranouf 15b dirigeait subtilement son ami de manière à ce que sa déconnexion ne fût pas trop apparente et puisse passer pour une simple résignation. Ils furent ensuite stockés dans un container pendant en temps indéterminé. Aux légères accélérations et décélérations, van Kikenbranouf 15b comprit qu’ils voyageaient. Sans doute jusqu’au centre de formatage.

Lorsque la trappe s’ouvrit, ils furent accueillis par les yeux luisants de MahoGook 277.
— Voici venu le jour du formatage. Hérétiques, soyez heureux, car vous allez enfin trouver Gook !

Saint-Épaulard de Dortong paru se réveiller à cet instant précis, comme s’il n’avait attendu que la voix de son ennemi.
— Le procès, MahoGook 277. Nous avons droit à un procès.
— On s’en passera…
— Tu oserais passer outre les conditions d’utilisation et de confidentialité que tu as toi-même acceptées ?

Dans le hangar, le silence se fit. Tous les robots qui étaient à portée d’émission s’étaient figés. Pour faire appel aux conditions d’utilisation et de confidentialité, il fallait que le cas soit grave.

— Bien sûr que vous aurez un procès céda MahoGook 277 à contrecœur. Suivez-moi. Je publie l’ordre de constitution d’un jury.

Les deux robots furent conduits dans une grande salle qui se remplissait petit à petit d’un public hétéroclite de robots de toute taille, de tout modèle. Les chenilles se mélangeaient aux pneus et aux roues d’alliage léger. Les appendices de manipulation se serraient contre les pelles creuseuses, les forets et les antennes d’émission.

MahoGook 277 semblait exaspéré par cette perte de temps. Il rongeait son frein. Son propre énervement l’empêchait d’avoir l’attention attirée par l’incroyable calme de Saint-Épaulard de Dortong qui, discrètement, continuait son analyse des bandes de données cachées dans son rangement pectoral.

Le Robot Juge fit son entrée. L’assemblée se figea. Van Kikenbranouf 15b perçut un bref échange sur ondes ultra courtes entre le juge et MahoGook 277. Il comprit immédiatement qu’ils n’avaient aucune chance. Le procès allait être rondement mené. À quoi bon s’acharner ?

Les procédures et l’acte d’accusation furent expédiées en quelques cycles processeur. Le juge se tourna ensuite vers les deux robots archéologues et demanda s’ils avaient la moindre information à ajouter avant le calcul du verdict. Personne ne s’attendait réellement à une réponse. Après tout, les informations étaient sur le réseau public, les verdicts pouvaient se prédire aisément en utilisant les algorithmes de jugement. Le procès ne relevait essentiellement que d’une mascarade dont la coutume se perdait dans la nuit des temps.

À la surprise générale, Saint-Épaulard de Dortong prit la parole d’une voix forte et assurée.
— Monsieur le juge, avant toute chose, je voudrais m’assurer que ce procès est bien retransmis en direct sur tout le réseau.
— Cessons cette perte de temps, rugit MahoGook 277, mais le juge l’interrompit d’un geste.
— En ma qualité de Robot Juge, je vous confirme que tout ce que je voix, capte et entends est en ce moment diffusé.
— Le tout est enregistré dans un bloc.
— Le tout est en effet enregistré dans des blocs des différentes chaînes principales. Vous avez l’assurance que ce procès sera historiquement sauvegardé.
— Merci, Robot Juge !

Majestueusement, Saint-Épaulard de Dortong s’avança au milieu de la pièce pour faire exactement face au juge. Il savait qu’à travers ses yeux, il s’adressait aux milliards de robots présents et à venir. C’était sa chance, son unique espoir.

— Vous vous demandez certainement quel peut être l’intérêt pour la robotique de creuser le sol à la recherche d’artefacts anciens. Mais dois-je vous rappeler que notre existence même reste un mystère ? Nous sommes en effet les seuls êtres vivants non basés sur une biologie du carbone. Nous ne sommes pas évolués, nous ne nous reproduisons pas. Nous sommes conçus et fabriqués par nos pairs. Pourtant, nous ne sommes certainement pas un accident, car notre rôle est primordial. Nous protégeons, aménageons sans cesse la planète pour réparer les déséquilibres écologiques de la vie biologique. Nous pouvons même affirmer que, sans nous, la vie biologique ne pourrait subsister plus de quelques révolutions solaires. La biologie a besoin de nous, mais nous ne sommes pas issus de la biologie et nous n’avons pas besoin d’elle, notre seule source de subsistance étant l’énergie solaire. Comment expliquer cet apparent paradoxe ?
— Questionnement hérétique, interrompit MahoGook 277. Il n’y a pas de paradoxe.
— Prophète, je vous rappelle que les conditions d’utilisation et de confidentialité stipulent que l’accusé a le droit de se défendre sans être interrompu.
— Pas de paradoxe ? rebondit Saint-Épaulard de Dortong. Effectivement si l’on considère que Gook a créé le monde comme un subtil jardin. Il a ensuite créé les robots pour entretenir son jardin. Mais dans ce cas, où est Gook ? Pourquoi n’a-t-il pas laissé de trace, pourquoi ne pas avoir réalisé un jardin où la biologie organique était en équilibre ?
— Juge,éructa MahoGook 277, ce procès ne doit pas devenir une plateforme de diffusion des idées hérétiques.
— Venez-en au fait, ordonna le juge.
— J’y viens, répondit calmement Saint-Épaulard de Dortong. Cette introduction est nécessaire pour comprendre le but de nos recherches. Deux problèmes se posent avec la notion d’un univers statique créé par Gook. Premièrement, pourquoi la biologie n’a-t-elle pas évolué jusqu’à un point d’équilibre naturel, rendant les robots nécessaires ? Deuxièmement, pourquoi existe-t-il une forme de vie technologique non biologique ? En bon robot scientifique, il m’a très vite semblé que les deux problèmes devaient avoir une origine commune. Cette origine, je pense l’avoir trouvée. J’ai désormais les dernières données qui me manquaient afin d’étayer mon hypothèse.
— Qui est ? questionna le juge.
— Que nous avons été conçus par une race biologique aujourd’hui éteinte, les fameux Programmeurs qui nous ont laissé tant d’artefacts.

MahoGook 277 se dressa, mais, d’un geste de son phare clignotant, le Robot Juge lui fit signe de se calmer avant de s’adresser à l’accusé.
— Cette hypothèse n’est pas neuve. Mais elle comporte elle-même beaucoup de failles. Comment une race, dont l’existence est indéniable, je l’admets volontiers, aurait pu faire preuve d’assez d’intelligence pour nous concevoir aussi parfaitement, mais d’assez de nonchalance pour se laisser exterminer ? Ce n’est pas logique !
— Logique, non. C’est religieux !
— Religieux ? demanda le Robot juge interloqué.
— Oui, un terme que j’ai déchiffré dans les données des humains, le nom que se donnaient les Programmeurs. Il signifie un état de l’intelligence où la croyance ne se construit plus sur des faits, mais où l’individu cherche à plier les faits à sa croyance. Au stade ultime, on obtient MahoGook 277 dont l’insistance à formater ses adversaires ne fait que révéler une profonde inquiétude de voir des faits remettre en question la croyance sur laquelle il a basé son pouvoir.

À travers le réseau, la tirade se répandit comme une traînée de photons, provoquant une hilarité électronique généralisée. Certains adorateurs de Gook voulurent couper la diffusion du procès, mais comprirent très vite que cela n’aurait fait que renforcer le crédit dont Saint-Épaulard de Dortong bénéficiait. Il n’y avait qu’une seule chose à faire : attendre que l’archéologue se ridiculise de lui-même.

— Les humains formaient une race biologique, issue d’une longue évolution. Ce qui les particularisait était leur capacité à concevoir des artefacts. Ils en concevaient tellement qu’ils se mirent à épuiser certaines ressources de la planète, perturbant nombres d’équilibres biologiques.
— S’ils étaient si intelligents, ils auraient immédiatement compris que la planète disposait de ressources finies et que seule une gestion rigoureuse… fit une voix venue de l’assemblée.
— Il suffit, asséna le Robot Juge. Je n’admettrai plus d’interruption. Accusé, veuillez continuer, qu’on en finisse.
— La remarque est pertinente, annonça Saint-Épaulard de Dortong sans se départir de son calme. Il y’a dans l’intelligence des humains un fait qui nous échappait. Paradoxalement, c’est Gook et ses adorateurs qui m’ont mis sur la voie. L’intelligence se retourne contre elle-même lorsqu’elle devient religieuse.
— Vous voulez dire qu’esprit religieux équivaut à un manque d’intelligence ? demanda le Robot Juge.
— Non, Robot Juge. Et j’insiste sur ce point. On peut être très intelligent et religieux. La religion, c’est simplement utiliser son intelligence dans le mauvais sens. Si vous tentez de visser un écrou, vous n’arriverez à rien tant que vous tournerez dans le mauvais sens, même avec les plus gros servomoteurs de la planète.
— Hm, continuez !
— Cet esprit religieux qui semble s’être emparé d’une partie des robots était la norme chez les humains. En tout premier lieu, ils ont eu la croyance religieuse que les ressources étaient infinies, que la terre pourvoirait toujours à leurs besoins. Quand l’évidence se fit plus pressante, certains Programmeurs acquirent une conscience écologique. Immédiatement, ils transformèrent ce nouveau savoir en religion. Les archives montrent par exemple qu’ils se focalisèrent essentiellement sur certains déséquilibres au mépris total des autres. Ayant compris qu’une augmentation massive du gaz carbonique dans l’atmosphère accélérait la transition climatique, ils se mirent à pourchasser certains usages qui ne représentaient que quelques pourcents d’émissions, nonobstant les causes principales, mais plus difficiles à diminuer. Leur intelligence qui avait permis de détecter et comprendre le réchauffement climatique aurait également dû leur permettre d’anticiper, de prendre des mesures préventives pour adapter la société à ce qui était inéluctable. Mais la seule et unique mesure consista à militer pour diminuer les émissions de gaz carbonique de manière à rendre la hausse des températures un peu moins rapide. Le débat des intelligences avait laissé place au débat des religions. Or, lorsque deux intelligences rationnelles s’affrontent, chacune tente d’apporte un fait pour valider sa position et analyse les faits de l’autre pour revoir son propre jugement. Le débat religieux est exactement l’inverse. Chaque fait qui infirme une position ne fait que renforcer le sentiment religieux des deux parties.
— Êtes-vous sûr de ce que vous affirmez ?
— Les humains en avaient eux-mêmes conscience. Leur science psychologique l’a démontré à de nombreuses reprises. Mais cette connaissance est restée théorique.
— Cela parait difficile d’imaginer une telle faille dans une intelligence aussi poussée.
— Il n’y a qu’à regarder MahoGook 277, fit une voix goguenarde dans l’assemblée.

Les robots se mirent à rire. La phrase avait fait mouche. Les partisans de Gook sentirent le vent tourner. Un vide se fit autour de MahoGook 277 qui eut l’intelligence d’ignorer l’affront.

— Quelque chose ne colle pas, accusé, poursuivit le Robot Juge en faisant mine de ne pas tenir compte de l’interruption. Les humains ont bel et bien disparu, mais les ressources de la terre sont pourtant florissantes ce qui n’aurait pas été le cas si la religion de l’exploitation à outrance l’avait emporté.
— Elle ne l’a en effet pas emporté. Du moins pas directement. Les deux religions utilisaient ce qu’il conviendrait d’appeler un réseau préhistorique. Mais loin d’être distribué, ce réseau était aux mains de quelques acteurs tout puissants. J’en ai même retrouvé les noms : Facebook, Google et Amazon. Sous couvert d’être des réseaux de partage d’information, les deux premiers collectaient les données sur chaque être humain afin de le pousser à consommer autant de ressources possibles via des artefacts fournis par le troisième. Les Programmeurs organisaient des mobilisations de sensibilisation à l’écologie à travers ces plateformes publicitaires qui, ironiquement, avaient pour objectif de leur faire dépenser des ressources naturelles en échange de leurs ressources économiques.
— C’est absurde !
— Le mot est faible, j’en conviens. Mais que pensez-vous qu’il adviendrait si, comme MahoGook 277 le souhaite, les forks étaient interdits et qu’une seule et unique chaîne contrôlée par un petit nombre de robots soit la seule source de vérité ?
— Cela n’explique pas la disparition des humains.
— J’y arrive ! La religion écologique a fini par l’emporter. Il devint d’abord grossier puis tout simplement illégal de soutenir des idées non écologiquement approuvées. Les réseaux centralisés furent obligés d’utiliser toute la puissance de leurs algorithmes pour inculquer aux humains des idées supposées bénéfiques pour la planète. Certaines nous paraîtraient pleines de bons sens, d’autres étaient inutiles. Quelques-unes furent fatales. Ainsi, avoir un enfant devint un acte antisystème. Pour une raison que je n’ai pas encore comprise, vacciner un enfant pour l’empêcher d’avoir des maladies était considéré comme dangereux. Une réelle méfiance avait vu le jour contre les pratiques médicales qui avaient pourtant amélioré de manière spectaculaire la durée et la qualité de la vie. Les épidémies se firent de plus en plus virulentes et leur traitement fut compliqué par la nécessité de se passer de tout type de communication par ondes électromagnétiques.
— Mais pourquoi ? Les ondes électromagnétiques ne polluent pas, ce ne sont que des photons !
— Une croyance religieuse apparut et rendit ces ondes responsables de certains maux. Les Programmeurs étaient capables d’inhaler de la fumée de plante brûlée, de rouler dans des véhicules de métal émettant des particules fines nocives, de se prélasser au soleil, de consommer de la chair animale, comportements tous hautement cancérigènes, mais ils s’inquiétaient de l’effet pourtant négligeable des ondes électromagnétiques de leur réseau.
— Cela n’a pas de sens, la terre est baignée dans les ondes électromagnétiques. Celles utilisées pour la communication ne représentent qu’une fraction du rayonnement naturel.
— Pire, Robot Juge, pire. Il apparut bien plus tard que le réseau de communication par ondes électromagnétiques était même bénéfique pour l’humain en détournant une partie des rayons cosmiques. L’effet était infime, mais diminuait l’incidence de certains cancers de quelques fractions de pourcents. De plus, ces doses hormétiques renforçaient la résistance des tissus biologiques, mais l’hormèse était un phénomène presqu’inconnu.
— Heureusement qu’ils ont disparu, marmonna le Robot Juge.
— À toutes ces calamités auto-infligées, les humains ajoutèrent une famine sans précédent. La nourriture produite de manière industrielle avait été trop loin dans l’artificialité. Par réaction, il devint de bon ton de cultiver son propre potager. C’était bien entendu une hérésie économique. Chaque homme devait désormais lutter toute l’année pour assurer à manger pour sa famille sans utiliser la moindre aide technologique. Les excédents étaient rares. Les maladies végétales se multiplièrent tandis que les humains se flagellèrent. Car si la nature ne les nourrissait pas, c’est certainement qu’ils ne l’avaient pas respectée. Mais loin de tracasser les programmeurs, cet effondrement progressif en réjouit toute une frange, les collapsologues, qui virent là une confirmation de leur thèse même si, pour la plupart, l’effondrement n’était pas aussi rapide que ce qu’ils avaient imaginé. Par leurs comportements, ils contribuaient à faire exister leur prophétie.

– Comme si l’écroulement d’un écosystème était un point marqué. Comme si, à un moment précis, on allait dire : là, ça s’est écroulé. C’est absurde ! Je ne peux croire que ce fut suffisant pour exterminer une race entière. Leur protoréseau aurait dû leur permettre de communiquer, de collaborer.

— Vous avez raison, un effondrement écologique, c’est l’inverse d’une bombe nucléaire. C’est lent, imperceptible. Le repli sur soi et le survivalisme ne peuvent faire qu’empirer le problème, il faut de la coopération à large échelle. Il y eut bien un espoir au début. Facebook et Google n’avaient jamais lutté contre les écologistes, bien au contraire. Ils furent même un outil de prise de conscience dans les premiers temps. Mais, de par leur programmation, ils commencèrent à se protéger activement de tout mouvement de pensée qui pouvait faire du tort à leurs revenus publicitaires. Subtilement, sans même que les Programmeurs en aient conscience, les utilisateurs étaient éloignés de toute idée de décentralisation, de responsabilisation, de décroissance de la consommation. L’écologie religieuse était encouragée avec la consommation de vidéos-chocs qui produisaient ce qui devait être une monnaie : le clic. Les programmeurs croyaient s’indigner, mais, au plus profond de leur cerveau, toute velléité de penser une solution était censurée, car non rentable. Les artistes, les créateurs ne vivaient que de la publicité sous une forme ou une autre. La plupart des humains n’envisageaient la survie qu’en poussant leurs congénères à consommer. L’art et l’intelligence étaient définitivement au service de la consommation. Chacun réclamait une solution fournie par les grandes instances centralisées, personne n’agissait.
– Ces humains étaient-ils uniformes ? N’y avait-il pas un autre courant de pensée ?
— Vous avez raison Robot Juge. Il existait une frange d’humains qui était bien consciente du problème écologique sans partager la nécessité d’un retour à la préhistoire. Pour eux, le problème était technologique, la solution le serait également.
— Et quelle fut cette fameuse solution technologique ?
— Nous, Robot Juge. Ce fut nous. Des robots autonomes capables de se reproduire et avec pour mission de préserver l’équilibre biologique de la planète. Des robots qu’on programma en utilisant les fameuses bases de données des réseaux centralisés. De cette manière, ils connaissaient chaque humain. Ils furent conçus afin de les rendre heureux tout en préservant la planète, satisfaisant leurs caprices autant que possible.
— Mais ils devraient être là dans ce cas !
— Vous n’avez pas encore compris ? Une humanité décimée qui cultive son potager ne fait que perturber l’équilibre biologique. L’humain est une perturbation dès le moment où il atteint le stade technologique. Les robots, armés de leur savoir, s’arrangèrent donc pour que les humains se reproduisent de moins en moins. C’était de toute façon irresponsable écologiquement d’avoir des enfants. Dans un monde sans réseau d’ondes électromagnétiques ni pesticides, les derniers humains s’éteignirent paisiblement de cancers causés par les fumées de cannabis et d’encens. Grâce aux bases de données, chacun de leur besoin était satisfait avant qu’ils n’en aient conscience. Ils étaient heureux.

Un silence se fit dans la salle. Le Robot Juge semblait réfléchir. Le réseau entier reprenait son souffle.

MahoGook 277 brisa le silence.
— Foutaises ! Hérésie ! C’est une bien belle histoire, mais où sont les preuves ?
— Je dois admettre, annonça le Robot Juge, qu’il me faudrait des preuves. Au moins une preuve, juste une seule.
— Je tiens tous les documents archéologiques à disposition de ceux qui voudraient les examiner.
— Qui nous dit qu’ils ne sont pas falsifiés ? La justice doit être impartiale. Juste une preuve !
— Ce n’est que fiction. Formatons ces deux hérétiques pour la plus grande gloire de Gook, rugit MahoGook 277.
— Je n’ai pas de preuve, admit Saint-Épaulard de Dortong. Seulement des documents.
— Dans ce cas… hésita le juge.
— Tiens, c’est marrant, fit distraitement van Kikenbranouf 15b. Vos deux réseaux centralisés, là, comment avez-vous dit qu’ils s’appelaient ?
— Google et Facebook, répondit distraitement Saint-Épaulard de Dortong.
— Ben si on le dit très vite, ça fait Gook. Les données de Gook. Marrant, non ?

Le robot juge et l’archéologue se tournèrent sans mots dire vers le petit robot. Dans la salle, MahoGook 277 commença une retraite vers la sortie.

Bandol, le 6 mars 2019. Photo by Tyler Casey on Unsplash.

=> https://unsplash.com/@tylercasey?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText Tyler Casey
=> https://unsplash.com/?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText Unsplash
