# Regagnez votre vie, éteignez votre télé !
=> files/old/broken_tv.jpg

Il y a quelques années, alors que je passais des entretiens d’embauche, les recruteurs lisant mon CV me posaient invariablement la question : « Vous faites vraiment tout ça ? Comment trouvez-vous le temps ? ». La première fois, je m’y attendais pas et j’ai répondu sans réfléchir : « Je n’ai pas la télévision ! ». La réaction a été immédiate : « Oh ben alors, je comprends ».

Votre vie, c’est le temps

Une technique PNL souvent utilisée pour prendre conscience de l’importance du temps, c’est de le remplacer par le mot vie, utilisé comme un synonyme. Après tout, votre vie entière n’est qu’une période de temps. « Je n’ai pas le temps de m’occuper de cela » devient « Je n’ai pas la vie pour m’occuper de cela ». Faites l’exercice, vous verrez à quel point cela permet de recentrer ses priorités.

Or, la télévision est, dans son principe même, un dévoreur de temps. Placé devant une télévision, vous n’avez plus aucun contrôle sur le temps et, par extension, sur votre vie.

Lorsque je vois des parents installer leurs jeunes enfants devant une télévision, je ressens une bouffée de rage. Je vois ces vies pleines d’énergie se faire happer, aspirer par l’ogre télévision.

Garder le contrôle au plus profond de la procrastination

Les opposants à la télévision sont nombreux à souligner l’ineptie des émissions. Les partisans répondent en parlant des programmes culturels que, soit dit en passant, personne ne regarde mais que tout le monde utilise comme argument pour justifier l’existence du téléviseur chez soi. Je suis certain que la moitié de l’audimat du Juste Prix affirme, en public, « qu’il y a souvent des documentaires intéressants sur ARTE ».

Mais, en toute honnêteté, je peux passer un temps fou à regarder des vidéos stupides sur Youtube. J’adore regarder un film le soir. La différence fondamentale, c’est que je garde toujours le contrôle de ce que je fais : je passe les vidéos qui ne m’intéressent pas (celles où il n’y a pas de chats), je mets sur pause quand je le désire. Avant de décider de regarder un film, je regarde la durée afin de me faire une idée de l’heure à laquelle j’irai dormir. Même lorsque je suis absorbé par la procrastination et que je ne vois pas le temps passer, mon cerveau reste aux commandes.

=> http://www.youtube.com/user/faireset2 il n’y a pas de chats

Le dictateur idéal

Votre horaire personnel s’adapte à la télévision. Votre feuilleton commence à telle heure ? Vous adapterez toute votre vie de famille pour vous y plier ou pour ne pas rater le sacro-saint journal. Vous irez jusqu’à refréner vos besoins naturels les plus basiques afin de n’aller au petit coin que pendant la page de publicités.

Si vous tombiez, au hasard de Youtube ou Dailymotion, sur la vidéo d’un Star Academy, d’un Question pour un champion ou d’un Secret Story, le regarderiez-vous plus de deux minutes ? Probablement pas. Pourtant, lorsque ce choix nous est retiré, nous sommes fascinés, hypnotisés. Et notre vie s’écoule dans un passif abrutissement que nous n’avons pas choisi.

Ce qu’aucun dictateur n’a réussi, ce qui nous paraissait absurde dans les pires dystopies, la télévision le fait quotidiennement. Le contrôle total de nos vies jusque dans ses aspects les plus intimes, à un degré qui renvoie PRISM au rang de boy scout bienveillant.

Le vecteur de désinformation

Outre son contrôle sur nos vies, la télévision est également un mensonge. Par définition, un journal télévisé est une des sources principales de désinformation.

Nul besoin de tomber dans la théorie du complot, c’est la structure même du journal qui vous contrôle et vous assène l’importance de chaque information. De nouveau, le maître mot est le contrôle du temps. Si trois minutes sont consacrées à la guerre au Machinkistan et cinq minutes sont consacrées à la fête des grand-mères, inconsciemment cela vous donnera l’impression d’être au courant tout en vous convainquant que la fête des grand-mères est plus importante.

Un artiste est invité à parler de son dernier album ce qui lui donnera une importance hors du commun, près du triple du conflit Machinkistanais. L’information est donc de la promotion et du divertissement. Quand aux sujets qui ne sont pas abordés, il n’existent tout simplement plus. Regarder un journal télévisé, même réalisé par des journalistes honnêtes et compétents, est un formatage total, absurde et dangereux.

Internet offre toutes les informations dont vous pouvez rêver. Passez du temps sur ce qui vous intéresse, tâchez de trouver des sources divergentes. Ne vous inquiétez pas : si une information est vraiment importante, elle vous arrivera d’une manière ou d’une autre. Mais vous serez surpris de constater qu’en réclamant le contrôle de votre vie, des tas d’informations qui vous paraissaient primordiales deviennent une perte de temps. Lorsque je jette un œil sur la couverture d’un magazine people, je suis toujours surpris de constater que, souvent, je ne connais pas une seule des personnes dont la vie est étalée. Je n’ai aucune idée de qui ils sont. Et pourtant, je n’ai pas l’impression de rater quoi que ce soit de fondamental.

Redevenir citoyen

Lorsque je critique la société, nombreux sont ceux qui me demandent quelle solution j’ai à proposer. Aussi étonnant que ça puisse paraître, la première chose que je recommande est de se débarrasser de la télévision. Ne me faites pas le coup du « Mais moi je ne la regarde jamais », comme un fumeur qui prétend arrêter mais qui a un paquet dans sa poche. Si vous l’avez chez vous, alors vous l’utilisez.

=> /la-premiere-guerre-civile-mondiale/ je critique la société

Continuez à regarder tout ce que vous voulez, vos séries, vos documentaires mais faites-le par le web. Vous découvrirez à quel point il peut être difficile de retourner à une vie où vous devez prendre les décisions, choisir les moments à consacrer à vous divertir. Certains vont trouver des excuses, contrer mes arguments par tous les moyens. Mais, après tout, c’est votre temps, c’est votre vie…

Avant de changer la société, il faut avoir le courage de se changer soi-même. Nous sommes la société. Nous devons réclamer le contrôle de nos propres vies. Se passer complètement de télévision est l’un des premiers actes citoyens.

Image par Talkingplant

=> http://www.flickr.com/photos/51035770426@N01/125764636 Talkingplant
