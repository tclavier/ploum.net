# Stagiaire au spatioport Omega 3000
=> files/old/Screen-Shot-2017-01-29-at-15.03.03.png

Cette nouvelle, illustrée par Alexei Kispredilov, a été publiée dans le numéro 4 du magazine Amazing que vous pouvez dès à présent commander sur ce lien.

=> http://www.bedetheque.com/auteur-35344-BD-Kispredilov-Alexei.html Alexei Kispredilov
=> http://studiobrou.com/portfolio/amazing4/ sur ce lien

Un léger choc métallique m’informe que la navette vient de s’amarrer à la station spatiale. Je ne peux réprimer un sentiment de fierté, voire de supériorité en observant mes compagnon·ne·s de voyage. Que ce soit ce triplet verdâtre d’alligodiles de Procyon Beta, ce filiforme et tentaculaire Orionais ou ce couple d’humains à la peau matte, tou·te·s ne sont que de simples voyageur·euse·s qui ne feront que transiter dans la station pour quelques heures.

Alors que moi, j’ai été engagé! Je suis stagiaire!
À pleines narines, je déguste les relents tout particuliers du spatioport Omega 3000. Un remugle de mélange, de mixité, de races, d’espèces. Une odeur indéfinissable, bâtarde, propre à tous les spatioports. L’odeur de la vie dans la galaxie.

— Dis moi, jeune homme, tu n’aurais pas vu dans ta navette une dame répondant au nom de Nathan Pasavan? Je suis certaine qu’elle devait être dans ton vol mais…
Étonné, je suis sorti de ma rêverie par une imposante dame aux cheveux gris et frisés. Ses lèvres semblent s’agiter plus vite que la vitesse à laquelle me parviennent ses paroles.— Je suis Nathan Pasavan, fais-je en tentant de prendre mon ton le plus assuré.
Son corps semble se figer un instant et elle pose sur moi un regard amusé.— Toi? Tu es le stagiaire?
— Oui, j’ai été sélectionné après les années de formation, les examens et les tests physiques. Je suis très fier d’avoir été assigné à Oméga 3000.
— Mais… Tu es un homme! Voire même encore un garçon. Je ne m’attendais pas à un jeune homme. Enfin bon, après tout, pourquoi pas. Combien de spatioports as-tu déjà visité ?— Et bien… Un seul!
— Un seul! Lequel?
— Celui-ci… fais-je en bégayant un petit peu.
Elle éclate soudainement de rire.
— T’es mignon. Allez stagiaire, au boulot !
D’une poigne ferme, elle me traîne à travers les méandres de la station avant de me planter une brosse dégoulinante dans les mains.
— Tiens, me dit-elle en pointant du doigt une porte munie d’un hiéroglyphe abscons. Au boulot stagiaire ! Montre-nous qu’un homme peut accomplir un véritable boulot de femme !

Surpris, je reste tétanisé, la brosse à la main. Derrière moi, j’entends la porte se fermer alors qu’une odeur pestilentielle m’assaille violemment les narines. Dans une semi-pénombre, j’arrive à observer un incroyable carnage: une large toilette débordante d’une matière verdâtre, émettant des effluves nauséabondes miroitant à la limite du spectre visible. Une toilette bételgienne, visiblement peu entretenue et utilisée par une armée de blobs visqueux en transit entre la terre et leur planète natale.

Serrant la brosse dans ma main, je déglutis. Est-ce ainsi que sont accueillis les stagiaires? Avec les tâches les plus indignes, les plus avilissantes? Est-ce parce que je suis un homme ?

Retroussant mes manches, je pousse un soupir avant de… Des souvenirs de mon entraînement me remontent à l’esprit. Ce scintillement est caractéristique des excréments de Bételgiens femâles. Une caste sexuelle qui ne mange et n’excrète qu’une mousse organiquement friable. C’est un piège! Si je trempe ma brosse, la mousse va la coloniser et utiliser les particules de savon pour se nourrir et grandir. C’est certainement ce qui s’est passé dans cette toilette. Les excréments de Bételgiens femâles sont cependant particulièrement sensibles à la lumière. Obéissant à mon intuition, je cherche l’interrupteur et inonde la pièce d’une lumière blanche et crue. Aussitôt, l’immonde matière visqueuse se met à ramper et à fuir vers la seule issue obscure : la toilette. En quelques secondes, la pièce brille comme un sou neuf. La porte s’ouvre et ma mentor me dévisage avec un sourire étonné.

— Pas mal pour un mâle, me dit-elle. Bienvenue sur Omega 3000, je suis Yoolandia, Madame Pipi en chef de tout le spatioport. Te voici désormais Madame Pipi stagiaire. Solennellement, elle me tend le cache poussière rose à fleur traditionnel et l’assiette à piécette, insigne historique de la fonction. Ne pouvant réprimer un immense sentiment de fierté, je me mets aussitôt au garde-à-vous.

***

— Alerte ! Alerte ! Situation au box 137.
Mon écran digital vient de s’allumer et je bondis immédiatement. Cela fait à peine quelques mois que je suis sur Omega 3000 mais mon corps a déjà acquis les réflexes de ma fonction. Yoolandia me toise d’un regard narquois.

— Encore une mission dont notre petit génie va s’extirper avec les félicitations du jury. Tu vas bientôt obtenir le grade officiel de Madame Pipi et me faire de l’ombre si tu continues !

— Pourquoi n’ai-je pas le droit à être appelé Monsieur Pipi ?

Elle soupire…

— Jamais le terme n’a été utilisé. C’est une fonction trop importante pour un homme. Allez, va! Le box 137 a besoin de toi!

Tournant les talons, je m’engouffre dans les méandres de la station. Pourquoi les hommes ne pourraient-ils pas occuper de hautes fonctions? Car le rôle de Madame Pipi est primordial dans une station spatiale! Lorsque les races de l’univers sont entrées en contact, nous nous sommes très vite aperçu·e·s que le concept de sexe était extrêmement variable voire indéfini d’une race à l’autre. Par contre, excréter semblait être une constante de la nature.

Les êtres humains étaient la seule race à proposer des toilettes séparées pour les différents sexes. Alors que même les compétitions sportives avaient depuis longtemps aboli toute catégorisation par sexe, les toilettes restaient encore et toujours un lieu de ségrégation. Le reste de la galaxie trouva cette mode tellement excentrique qu’elle décida de l’adopter. Mais comme la plupart des races ne se divisent pas entre mâles et femelles, il fallut instituer des toilettes séparées pour chaque cas pouvant se présenter. Le lépidoptère amurien, par exemple, passe par 235 modifications de sexe au cours de son existence. Dont 30 au cours d’une seule et unique heure. Heureusement, ils ne voyagent que dans 17 de ces situations.

Le rôle de Madame Pipi est donc primordial dans un spatioport. Il requiert de longues années d’études et un entraînement physique à toute épreuve. Pour, par exemple, résoudre le cas qui se présente à moi au box 137. Un box réservé normalement aux surfemelles. Mais dans laquelle est entrée par erreur une limace tronesque agenrée.

=> ../files/old/Screen-Shot-2017-01-29-at-14.24.28.png 

Les excréments d’une limace tronesque ressemblent à des billes transparentes qui croissent au contact de l’eau si elles ne sont pas auparavant dissoutes. En tirant la chasse, les surfemelles Odariennes se sont donc retrouvées coincées au milieu de gigantesques sphères translucides.

Un cas d’école assez routinier.

***

— Alerte! Alerte! Situation au box 59!
Je pousse un profond soupir. Tous ces problèmes me semblent si simples à résoudre, si artificiels. En vérité, je m’ennuie !

***

– Bravo Nathan!
Tout en m’embrassant sur les deux joues, Yoolandia colle symboliquement une pièce dans mon assiette. La directrice du spatioport Omega 3000, en personne, s’adresse à l’assemblée.

— Nous sommes très fières, aujourd’hui, de nommer Nathan «Madame Pipi certifiée».
— Monsieur Pipi, grommelé-je entre mes dents.
— La pièce que vient de coller Yoolandia dans l’assiette de Nathan est une véritable pièce de monnaie préhistorique. Cet acte symbolique rappelle l’importance historique de la fonction…

Mais déjà je n’écoute plus. Je suis impatient d’exposer mon idée à Yoolandia. Une idée qui va révolutionner la fonction de Madame Pipi. Tandis que la directrice continue sa harangue, je tire Yoolandia un peu à l’écart.
— Alors, tu es prêt pour ton discours ? me fait-elle.
— Oui, justement, je compte présenter une idée incroyable !
— Quelle idée?
— Eh bien j’ai imaginé supprimer la ségrégation des toilettes…
— Comme c’est original, fait-elle en éclatant d’un rire jaune. Tu crois que tu es le premier? N’as-tu pas compris que chaque type d’excrément doit être traité différemment ? Que la séparation est nécessaire?
— Justement, fais-je, je m’attendais à cette objection. J’ai développé une méthode très simple qui évacue tous les types d’excréments sans produire la moindre réaction indésirable. Un système basé sur de la lumière pulsée, des vibrations et un assortiment d’enzymes tronesques dont…
— Malheureux !
Comme par réflexe, elle m’a couvert la bouche. Son regard est terrifié. Je tente de me dégager.

=> ../files/old/Screen-Shot-2017-01-29-at-14.26.33.png 

—Non, tais-toi ,me fait-elle. Tu ne comprends pas ? Tu ne vois pas que tu es en train de tuer notre métier ! Notre prestige !
—Hein?
— Si tu fais cela, c’est la fin des Madames Pipi! Tu n’es certainement pas le premier à arriver à cette solution. Mais celleux qui l’ont trouvée ont vite compris où était leur intérêt.
— Que veux-tu dire?
— Regarde-les, me dit-elle en pointant la foule hétéroclite des employé·e·s du spatioport. La moitié d’entre elleux nous obéissent. Iels sont plombier·e·s, nettoyeur·euse·s, spécialistes en sanitaires différenciés. Leur job dépend de nous! Tu ne peux pas simplifier la situation! Pense aux conséquences!
— Mais…
— Et maintenant, entonne la directrice dans son micro, je vous demande un tonnerre d’applaudissements pour Nathan, notre nouvelle Madame Pipi!
— Monsieur Pipi, murmuré-je machinalement !
Alors que je me dirige vers l’estrade, la voix de Yoolantia me parvient.
— Pense aux conséquences, Nathan. Pour ton emploi et ceux des autres.
À mi-chemin, je me retourne. Elle me darde de son regard perçant.
— N’oublie pas que tu es un mâle. Un mâle du box 227. Je suis une femelle du box 1. Alors, réfléchis bien aux conséquences…
