# L’Hoary, ma meilleure amie

=> ../files/old/boss.gif ZeBoss
Votre ordinateur vous permet d’accomplir différentes tâches grâce à des programmes ou logiciels. Généralement, un logiciel est dédié à une tâche précise. Mais pour pouvoir faire tourner plusieurs logiciels en même temps et gérer les interactions avec le matériel (hardware), votre ordinateur a besoin d’un système d’exploitation, un gros programme central qui est plus ou moins l’équivalent de votre chef de service : tout seul il ne sait rien faire, mais sans lui personne ne fait rien.

Le système d’exploitation le plus courant est Microsoft Windows. Un vrai chef de service d’opérette : habillé d’un costume bleu et vert avec une cravate orange, passant son temps à changer d’avis, à vous mettre des bâtons dans les roues lorsque vous tentez de faire correctement votre boulot, accariâtre, hypocondriaque[1], bref votre chef de service habituel !

Comme tout bon chef de service, tout le monde s’en plaint par derrière mais personne n’ose lui dire en face ses quatre vérités et on le considère comme indispensable. Que feriez-vous sans lui ?

=> ../files/old/hoary.jpg Mlle Hoary
J’ai une proposition pour vous : que diriez-vous de le remplacer par une jeune et jolie chef de service, sympathique, attirante et bronzée ? Rassurez-vous, votre productivité ne fera que s’accroître mais surtout vous irez dorénavant travailler avec le sourire aux lèvres, sans crainte des sautes d’humeur imprévisibles de l’autre zouave.

Interessé ?

Laissez moi donc vous présenter Mlle Ubuntu, Hoary de son prénom. Je suis sûr que vous allez très bien vous entendre si vous prenez la peine d’apprendre à la connaître.

=> http://www.ubuntulinux.org Ubuntu

#### Notes

[1] il est vrai qu’il attrape le moindre virus passant à sa portée

=> ../files/old/ubuntu.png Ubuntu Logo

### Inviter Mlle Hoary sur son ordinateur

Plutôt que de longs discours, le mieux est sans aucun doute d’essayer par soi-même. Pour cela, procurez-vous le »Live CD »[1]. Live CD signifie qu’il suffit de mettre le CD dans votre ordinateur et de redémarrer ce dernier. Aucun changement ne sera fait à vos données. En fait, votre disque dur ne sera même pas touché ! Après un chargement un peu plus long que la normale, vous pourrez tester en Live Ubuntu Hoary sur votre ordinateur. Ce CD-rom fournit en plus des logiciels pour Windows. Si vous comptez encore garder votre Windows, ces logiciels sont indispensables.

=> http://se.releases.ubuntu.com/5.04/ procurez-vous le »Live CD »
=> http://framasoft.net/article3555.html Live CD

En seconde étape, vous pouvez passer à l’installation proprement dite, en vous procurant le CD d’installation[2].

=> http://se.releases.ubuntu.com/5.04/ le CD d’installation

L’installation d’Ubuntu Hoary se fait très simplement. Les seuls problèmes que vous pourriez rencontrer sont avec le matériel un peu exotique. Si vous n’êtes pas à l’aise avec le concept de patitionnement du disque dur, demandez donc à quelqu’un d’autre de vous aider voire même d’effectuer à votre place cette partie du travail[3]. Il faut en effet toujours un minimum de prérequis avant d’installer un système d’exploitation.

=> http://www.ubuntu-fr.org/articles/faqubuntu/faqubuntustart.html un minimum de prérequis

Remarquons que vous pouvez tout simplement télécharger les CDs d’Ubuntu Hoary mais aussi les commander gratuitement par la poste.

=> http://se.releases.ubuntu.com/5.04/ télécharger
=> http://shipit.ubuntulinux.org/ commander gratuitement par la poste

=> ../files/old/login.png login

### Travailler en coopération avec Mlle Hoary

Travailler avec Ubuntu Hoary comme chef de service vous demandera peut-être un certain temps d’adaptation. Tout d’abord, Mlle Hoary est issue de la communauté OpenSource. C’est une grande humaniste et le partage des connaissances est une composante essentielle de sa philosophie. Si vous avez besoin d’un programme qui n’est pas disponible par défaut, perdez absolument l’habitude de courir en acheter au magasin ou, pire, de le voler dans le bureau voisin. Demandez simplement à Hoary, via l’outil Synaptic, de vous installer le logiciel requis.

Il faut préciser que, majoritairement, Ubuntu Hoary ne travaille pas avec les mêmes outils que Microsoft Windows. Il faudra donc passer un peu de temps à réapprendre les nouveaux outils. Par exemple, Evolution deviendra votre lecteur de courriels[4], Gimp votre éditeur de photos mais Firefox restera votre navigateur web[5].

=> http://www.gnome.org/projects/evolution/ Evolution
=> http://gimp.org/ Gimp
=> http://www.mozilla-europe.org/fr/products/firefox/ Firefox

Après quelques jours en compagnie de Ubuntu Hoary, vous serez, j’en suis sûr, conquis par son charme et sa facilité de caractère. Ubuntu Hoary est complètement insensible aux virus ou aux agressions extérieures. Elle n’est fait que ce que vous voulez qu’elle fasse car vous l’avez décidé[6].

=> ../files/old/linux.pinguin.png Tux

### Mais qui est Mlle Ubuntu Hoary ?

Ubuntu est l’un des nombreux projets de l’astronaute Sud-Africain Mark Shuttleworth. J’avais donné quelques explications à ce sujet lors de la sortie de Ubuntu Warty, la version précédente de Ubuntu. Ubuntu est sponsorisé par la société Canonical qui emploie une bonne partie des meilleurs programmeurs de la communauté OpenSource. Un gage de qualité et de pérénité !

=> http://www.firstafricaninspace.com/ l’astronaute Sud-Africain Mark Shuttleworth
=> /2004/10/21/4-ensemble-les-yeux-leves-vers-le-ciel quelques explications à ce sujet
=> http://www.canonical.com/ Canonical

Mais Ubuntu ne saurait être réduit au travail des programmeurs de Canonical ! Ubuntu, c’est avant tout l’extraordinaire résultat du travail des milliers de volontaires et professionnels de la communauté Debian, des projets Gnome, OpenOffice.org, Mozilla et j’en oublie… Qu’il me soit permit ici de remercier les programmeurs, traducteurs, graphistes, testeurs, rédacteurs aux yeux rougis par les nuits blanches pour leur extraordinaire travail.

=> http://www.debian.org/ Debian
=> http://gnome.org/ Gnome
=> http://fr.openoffice.org/ OpenOffice.org
=> http://www.mozilla-europe.org/fr/ Mozilla
=> http://www.gnu.org/ j’en
=> http://www.fsf.org/ oublie

Ubuntu Hoary fait partie de la grande famille Linux. Pour installer Linux, il faut choisir une version de Linux. Ubuntu Hoary est l’une de ses versions et une des plus faciles et agréables pour le grand public. En installant Ubuntu Hoary, vous installez donc Linux et bénéficiez donc de toutes les possibilités de Linux. Vous n’avez donc plus besoin de Windows sur votre ordinateur. Par contre, les logiciels tournant sous Windows ne tournent généralement pas sous Linux et vice-versa. Par exemple, les virus ne tournent pas sous Linux. Dommage… 😉

### Rejoignez la communauté

=> ../files/old/circle.jpg Ubuntu Circle

Comme tout ce qui concerne l’informatique, il est probable que vous rencontriez des petits problèmes ou que, tout simplement, vous vous posiez des questions sur la meilleure manière d’acomplir une tâche. Avec Ubuntu Hoary, vous n’êtes plus tout seul face à votre ordinateur ! En effet, Ubuntu c’est aussi une communauté francophone très accueillante. N’hésitez pas à venir y poser vos questions[7].

=> http://forum.ubuntu-fr.org/ une communauté francophone très accueillante

La documentation est abondante. Citons le très bon Starter Guide qui sera très prochainement traduit en français.

=> http://ubuntuguide.org/index.html Starter Guide
=> http://www.ubuntu-fr.org/articles/ubuntuguide/ traduit en français

### Plongez

Ubuntu est partout ! Et les témoignages des nouveaux utilisateurs vont dans ce sens :

=> /gallery/fourre-tout/oh-my-geek/image1154#gallery partout

> moi content que ubuntu fonctionne
> (Xavier W., étudiant)

Alors ?

Envie de faire de faire des infidélités au train-train quotidien, à votre chef de service grisonnant et poussièreux ? N’hésitez plus ! Plongez et venez goûter au doux soleil de la liberté…

=> http://framasoft.net/article1821.html liberté

Vous verrez, votre système d’exploitation ne sera plus votre odieux dictateur, mais deviendra votre meilleure amie ! 
=> ../files/old/plongeon.jpg Plongeon

#### Notes

[1] si vous ne savez quelle version choisir, il est probable que vous ayez besoin de la x86

[2] de nouveau, en cas de doute, utilisez le x86

[3] Il faut signaler aussi qu’Ubuntu Hoary et Microsoft Windows peuvent cohabiter sur le même ordinateur, ce qui vous permet de ne prendre aucun risque.

[4] le remplacer par Mozilla Thunderbird ne pose aucun problème si c’est ce que vous souhaitez

[5] car vous n’utilisez plus Internet Explorer n’est-ce pas ?

=> /2004/11/09/19-surtout-ne-vous-laissez-pas-avoir-cest-une-arnaque vous n’utilisez plus Internet Explorer n’est-ce pas ?

[6] cela ne signifie pas qu’il faille abandonner toute prudence. En effet, une personne mal intentionnée pourrait vous conseiller d’effectuer une série d’actions menant, sans que vous le sachiez, à la perte de vos données.

[7] il existe aussi bien sûr une communauté anglophone

=> http://ubuntuforums.org/ communauté anglophone
