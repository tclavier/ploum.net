# Printeurs 40
=> files/old/7082202349_4a861c85f9_z.jpg

Le taxi dans lequel Nellio, Eva, Max et Junior s’enfuient est soudainement balayé par une explosion terroriste.

Très vite, je repère les journalistes qui filment l’événement. Les terroristes font toujours très attention de ne pas les blesser afin de donner une visibilité maximale à leur action.

— Par ici ! crié-je.

Mais les terroristes semblent de plus en plus nombreux. Tous les clients sortant du centre commercial de l’autre côté de la rue sont abattus sans pitié. Des grenades sont lancées à travers les vitrines des magasins.

— C’est pas un petit attentat de misère, murmure Junior. On n’a pas intérêt à se faire remarquer.

Trop tard ! Un terroriste isolé nous a aperçut et pointe son arme vers notre petit groupe.

— Nous sommes journalistes ! crie Eva.
— Continuez ! renchérit Junior. Vous êtes en train de devenir le hashtag top trending du jour.

Le terroriste baisse son arme, indécis, mais continue à se rapprocher de nous. Il est vêtu comme n’importe quel étudiant, il est jeune et son regard ne respire aucune haine particulière.

— On est vraiment le hashtag top trending ? demande-t-il d’une voix hésitante.
— Oui, répond Eva. Faut dire que vous avez fait du beau boulot.
— Fait voir tes chaussures ? demande Junior. Génial, c’est justement la marque qui sponsorise notre émission ! Tu peux prendre une pause un peu aggressive ? Je vais te filmer avec mes implants rétiniens.

Flatté, le terroriste obtempère. Je l’entends murmurer en pointant le doigt vers le ciel :
— Tu es vengé mon frère ! Toi dont l’attentat n’avait eu aucun retentissement médiatique, c’est à toi que je dédie ce hashtag top trending !

Prudemment, nous opérons une retraite stratégique. La voix chaude et grave de Max me parvient, comme un souffle d’été :

— Reculons progressivement. Les collègues de Junior ne vont pas tarder. Abritons-nous derrière les journalistes.
— Tiens, ajoute Junior, je n’ai jamais compris pourquoi certains blogueurs étaient appelés des journalistes et d’autres des blogueurs. Y’a une raison particulière ?
— Les journalistes reçoivent de l’argent public en plus de la publicité traditionnelle, fais-je.
— Mais pour quelle raison ?
— C’est historique. C’est pour s’assurer de leur objectivité.
— Ah ? Et ça marche ?
— À ton avis ?
— À mon avis, c’est complètement stupide.
— Ça s’appelle une tradition.
— C’est ce que je dis, c’est complètement stupide. Mais sinon, je trouve ça cool de voir un attentat autrement qu’à travers un avatar. Avec mon vrai corps !
— Ça change quelque chose ?
— Rien du tout ! C’est juste cool.
— L’euphorie de l’implant… intervient Max.

À pas prudents, nous nous sommes éloignés de l’attentat de manière suffisante pour pouvoir nous mêler aux badauds qui, à distance prudente, apprécient le spectacle.

— Bon, et bien il nous reste à trouver un moyen de rejoindre le siège du conglomérat. Le tout sans utiliser le moindre ordinateur ni appareil électronique. Ça va être du gâteau. Je ne sais même pas dans quelle direction il faut aller.
— Et si on demandait ?

Je reste un instant étonné de ne pas y avoir tout simplement pensé. Le sentiment d’être traqué m’a transformé en coupable. J’ai peur de toute interaction. Selon tous les algorithmes de surveillance, mon comportement doit être éminemment suspect pour la seule et unique raison que je sais être suspect. Prenant une profonde inspiration, je m’approche d’une des passantes qui se met sur la pointe des pieds pour tenter de filmer l’attentat et, qui sait, obtenir une vidéo qui deviendrait virale.

— Excusez-moi…
— Vous avez vu ? Vous venez de là-bas, non ? C’est dangereux ? Ça vaut la peine de filmer ?
– C’est plein de journalistes mais…
– Oh, les journalistes, vous savez ce que c’est. Ils filment tout puis ne montrent que ce qui les arrange après montage. De toutes façons, les journalistes, ils sont tous complices.
– Est-ce que vous pourriez…
– Oh, je sais ce que vous allez dire. Ils ne sont pas tous pareil, il y’en a des biens. Mais à partir du moment où ils sont payés par l’état, que voulez-vous, c’est la porte ouverte au clientélisme !
– Je cherche à…
– Alors je ne cautionne pas du tout les journalistes subventionnés mais si je pouvais faire une vidéo dont ils me rachèteraient l’exclu, ça m’arrangerait, vous comprenez ? Ou alors je la revends aux sites privés. Ils paient mieux !
— Mais…
— Je sais ce que vous allez dire : pourquoi ne pas mettre moi-même la vidéo en ligne et toucher les royalties des publicités ? J’avoue m’être posé la question mais si jamais je ne fais pas le buzz, je perds tout ! C’est un fameux risque, vous ne trouvez pas ?

Impuissant face à l’intarissable torrent de paroles de ma volubile interlocutrice, je cherche machinalement de l’aide dans le regard de sa voisine qui s’est rapprochée, interpellée par cet étrange monologue. Celle-ci réagit à mon appel silencieux en brandissant une caméra montée en bague sur son poing serré.

— Quoi ? Vous préférez donc les pseudo-journalistes publicitaires aux journalistes d’état ? Les journalistes d’état, eux au moins, n’ont pas pour métier de nous abrutir avec de la publicité !
— Ah non ? répond ma première interlocutrice. Pourtant ils utilisent également de la publicité !
— Pas tous ! Et ce n’est qu’une aide supplémentaire.
— Donc vous voulez dire que ce sont des publicitaires sponsorisés par l’état ?
— Ils ont une éthique ! Et je préfèrerais cent fois leur vendre ma vidéo même si cela signifie en toucher un prix inférieur !
— Une éthique ? Quelle vaste blague !
— Parfaitement ! Et ils ont au moins la décence de désactiver les publicités joyeuses pour les événements dramatiques, eux ! Il y a des gens qui se font tuer à quelques mètres de nous et des charognards comme vous ne cherchent qu’à les filmer pour faire vivre des publicitaires !
— Vous faites pareil, madame !
— Non, moi je cherche à fournir du matériel à des journalistes responsables afin d’informer les citoyens, c’est complètement différent !

Je recule prudemment. Max me touche l’épaule de sa main mi-écorchée, mi-métal. Personne ne semble faire attention à lui. Tout au plus lui demande-t-on s’il a été blessé dans l’attentat.

Par gestes, il me montre une automobile semi-blindée un peu à l’écart dans laquelle Eva et Junior sont en train de s’afférer. Emboitant le pas à Max, je m’y engouffre sans poser de questions.

Une rafale retentit, suivit d’un léger bruit de réacteur. À la place où je me tenais, les deux cadavres de mes interlocuteurs gisent, déchiquetés.

— Un drone de combat, ai-je le temps de murmurer.
— Oui, m’explique Max. Ils sont programmés pour détecter les comportements pré-terroristes en se basant sur les données comportementales des individus, leurs utilisations des réseaux sociaux, etc. La conversation que ces deux là viennent d’avoir a sans doute du activer une série de mots clés.
— Mais ils n’étaient pas terroristes !
— Un faux positif… La rançon de la technologie.
— Et les terroristes eux n’ont pas été arrêtés !
— Un faux négatif…

Je ne réagis même pas. Tout cela me semble normal. Je me contente de regarder l’intérieur de la voiture dans laquelle je viens de m’asseoir.

Photo par Elizabeth Amore Bradley.

=> https://www.flickr.com/photos/mseab/7082202349/ Elizabeth Amore Bradley
