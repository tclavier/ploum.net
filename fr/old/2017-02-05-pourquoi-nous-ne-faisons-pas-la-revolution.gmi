# Pourquoi nous ne faisons pas la révolution ?
=> files/old/8040612708_64fabc6e92_k.jpg

Autour de moi, nombreux sont ceux qui s’indignent sur les différentes injustices, sur la malhonnêteté flagrante et indiscutable des politiciens qui nous gouvernent. Et de s’interroger : « Comment se fait-il qu’on tolère ça ? Pourquoi n’y a-t-il pas de révolutions ? »

La réponse est simple : car nous avons trop à perdre.

Depuis la corruption à peine voilée et bien connue de nos dirigeants aux injustices inhumaines de notre système, nous dénonçons mais n’agissons pas.

## Nous avons peur de perdre…

Grâce à la productivité accrue, notre société pourrait nous nourrir et nous loger confortablement sans soucis. Cependant, nous sommes encore tous entretenus dans la superstition qu’il est nécessaire de travailler pour mériter le droit de survivre. Nous avons peur de la pauvreté voir même de demander de l’aide, d’être assistés.

=> /les-5-reponses-a-ceux-qui-veulent-preserver-lemploi/ il est nécessaire de travailler pour mériter le droit de survivre

Le travail est une denrée de plus en plus rare ? Le travail est le seul moyen de survivre ?

Ces deux croyances sont si profondément ancrées qu’elles rendent difficile de prendre le moindre risque de changer les choses. Car tout changement pourrait être pire. Et induire le changement est un risque individuel !

## Nous avons trop à perdre…

Emprunt hypothécaire, grosse voiture, smartphone assemblé en Chine, ordinateur portable, chemises de marque produites par des enfants au Bangladesh. La société de consommation nous pousse à trouver dans l’achat et le luxe ostentatoire une réponse à tous nos maux.

Nous soignons l’hyperpossession par des achats compulsifs. Nous ne sommes même pas intéressés par les objets en question mais par le simple fait d’acheter, de posséder. Sinon, nous achèterions de la qualité.

=> http://www.raptitude.com/2017/01/not-materialistic-enough/ même pas intéressés par les objets

Et le résultat est que nous possédons énormément de brols, de produits de mauvaises qualité que nous n’utilisons pas mais que nous entassons et refusons de jeter pour ne pas remettre en question notre acte d’achat. Nous possédons tellement que tout changement nous fait peur. Serions-nous prêts, comme nos arrières-grand-parents, à partir sur les routes en n’emportant qu’une simple valise ? Nous avons travaillé tellement d’heures pour acheter ces biens que nous stockons, souvent sans les utiliser une fois l’effet de nouveauté passé.

=> /le-cout-de-la-conviction/ remettre en question notre acte d’achat

## Prendre des risques

Au plus nous possédons, au plus nous avons à perdre. Ajoutons à cela que, dans le crédo sociétal, toute déviation de la norme est perçue comme une prise de risque incroyable, vitale. Si vous n’avez pas votre maison, vous risquez d’être sans ressource à la pension. Si vous n’avez pas de travail, vous êtes sociétalement un paria et vous serez demain à la rue. Demander de l’aide à des amis ou à la famille serait le comble du déshonneur.

Il est donc préférable de perpétuer le système tel qu’il est, de ne surtout rien changer.

Alors, nous râlons devant les injustices flagrantes, les manquements. Nous nous attacherons à des petits avantages, des augmentations salariales.

Nous voterons pour “le changement” en espérant de tout cœur que rien ne change.

## Les révolutions

Il faut se rendre à l’évidence : les révolutions se font par des gens qui sont prêts à sacrifier leur vie.

Or nous ne sommes même pas prêts à sacrifier notre nouvelle télévision et notre carte essence.

Tant que nous nous évertuerons à protéger notre emploi et nos petits avantages, fut-ce au mépris de nos valeurs et de nos propres règles morales, nous nous condamnons à entretenir le système.

Par contre, nous pouvons apprendre à acheter de manière responsable, à ne plus sacrifier nos valeurs pour un emploi, à sortir de nos aliénations. Nous pouvons apprendre à remettre en question ce que nous achetons, ce que nous faisons pour gagner notre vie. Nous pouvons apprendre à refuser de nous laisser manipuler.

=> /echappez-a-la-manipulation-de-vos-emotions/ de nous laisser manipuler

Peut-être que c’est tout simplement cela la prochaine révolution : prendre conscience des conséquences de nos actions individuelles, reprendre le pouvoir sur nos vies au lieu de se contenter de remplacer régulièrement ceux à qui nous déléguons le pouvoir.

Photo par Albert.

=> https://www.flickr.com/photos/campra/8040612708/ Albert
