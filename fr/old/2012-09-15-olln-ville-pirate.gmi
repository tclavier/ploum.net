# Ottignies-Louvain-la-Neuve, ville pirate

Ola, du bateau ! Oui toi, le moussaillon fatigué ou le coureur des mers, toi qui fait escale dans l’interlope port d’Ottignies-Louvain-la-Neuve où se retrouve la fine fleur de la flibuste, la pègre des océans.

Tu auras à cœur de lire le petit manuel de politique d’Ottignies-Louvain-la-Neuve avant de pénétrer dans les bas-fonds de notre ville, là où, parmi les relents de rhum et de tabac exotique se trament les plans les plus audacieux, se préparent les abordages les plus cruels.

=> /politique-a-olln petit manuel de politique d’Ottignies-Louvain-la-Neuve

Tu y croiseras certainement Jean-Luc Sparrow, gouverneur de la ville, dont la cruauté fait verdir même ses compagnons les plus hardis.

=> http://www.olln.be/fr/hotel-ville-politique/bourgmestre-echevins/bourgmestre.html Jean-Luc Sparrow
=> http://www.ecololln.be/2012/?site=ecololln ses compagnons

=> ../files/old/pirate_jean_luc_roland.png Jean-Luc Roland le Pirate

Il tient le pouvoir, d’une main de fer, grâce à l’aide d’Annie la Rouge, capitaine du fier Sanguinaire. La légende prétend qu’à cause de son refus, Johnny Depp fut forcé de se rabattre sur Keira Knightley. « Une vraie pirate ne participe pas à ces mascarades hollywoodiennes » aurait-elle dit.

=> http://www.ps.be/Pagetype1/PS/Vos-Elus/Annie-GALBAN---LECLEF.aspx Annie la Rouge

=> ../files/old/pirate_annie_galban.png Annie Galban la pirate

Mais, écoutez ! Les rumeurs du port montent jusqu’à nous. Un complot se trame. Long John Cédric, le premier conseiller de Jean-Luc Sparrow manigancerait en effet une prise de pouvoir. Des témoins affirment que, de nuit, des canons et des armes sont chargés à bord de l’Avenir, la frégate de Long John Cédric.

=> http://avenir-lln.be/ l’Avenir

=> ../files/old/pirate_cedric_dumonceau.png Cédric du Monceau le pirate

Toutes ces intrigues laissent de marbre le vénérable Barbe Blanche. Sa fière caravelle fend l’azur et il ne fait point mystère de ses projets d’abordage. Il se battra avec l’énergie farouche et le rhum dont il abreuve son équipage.

=> http://www.olln20.be/index.php?page=presentation-des-candidats Barbe Blanche

=> ../files/old/pirate_jacques_otlet.png Jacques Otlet le pirate

Barbe Blanche fourbit ses armes, harangue ses troupes. Il espère compter sur le soutien de Ploum le Sanguinaire-sauf-pour-les-petits-chats, capitaine du Transparent. Mais celui-ci mène ses pirates au combat sans se préoccuper d’alliances ou d’intrigues.

=> /pourquoi-voter-lionel-dricot Ploum le Sanguinaire-sauf-pour-les-petits-chats
=> http://www.ppbw.be/ ses pirates

=> ../files/old/ploum_le_pirate.png Ploum le pirate

La bataille s’annonce terrible, beaucoup de braves y laisseront leur vie. Déjà, la tension est palpable, la nervosité gagne les équipages. Ils savent qu’au soir du 14 octobre, des épaves fumantes surgira un de ces cinq pirates, le nouveau maitre d’Ottignies-Louvain-la-Neuve.
