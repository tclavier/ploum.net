# 1 mois de déconnexion, premier bilan
=> files/old/will-van-wingerden-87463-unsplash.jpg

Cela fait un mois que je suis déconnecté, 30% du temps initial que je m’étais fixé en démarrant l’expérience. Puis-je déjà tirer des conclusions ?

=> /je-suis-deconnecte/ je suis déconnecté

Oui, clairement. La première c’est que je ne suis pas du tout impatient de me reconnecter. En fait, j’ai même peur de le faire. Il y’a quelques jours, après avoir fini une tâche, j’ai machinalement tapé l’URL d’un site bloqué, un site d’informations générales.

Ce simple fait est en soi incroyable : mon cerveau est tellement formaté que même après un mois d’abstinence, les réflexes musculaires agissent encore. Je n’ai pas voulu aller sur ce site. Cela fait un mois que je n’y suis plus allé. Je n’avais aucune envie, aucun intérêt à y aller. Et, pourtant, mes doigts ont tapé l’URL.

Or, il s’avère que Adguard était désactivé. Pour la petite histoire, un site ne chargeait pas correctement et j’avais voulu vérifier si ce n’était pas Adguard qui interférait. J’avais oublié de le réactiver juste après.

Je me suis donc retrouvé sur le site d’actualités, à ma grande surprise. Premièrement, car je ne m’étais pas rendu compte que j’avais tapé l’URL et, en deuxième lieu, car j’étais persuadé que le site était bloqué. J’ai donc décidé de fermer le site et de réactiver le blocage. Mais, auparavant, j’ai quand même lu tous les grands titres et j’ai ouvert deux articles. Je n’ai littéralement pas pu m’en empêcher.

C’est effrayant.

Le pire est que, sans ma déconnexion, je ne me serais probablement jamais rendu compte de ces automatismes incontrôlables. Il y’a également de fortes chances pour que mon cas soit relativement représentatif d’une certaine frange de la population. Même si je suis un cas extrême, nous sommes certainement tous atteints à des degrés différents. La seule chose qui change est la conscience.

Mais la question primordiale reste de savoir si cette addiction est vraiment nocive.

Pour moi, la réponse est sans conteste un oui généralisé.

En un mois seulement, j’ai déjà l’impression d’être en train de changer profondément et durablement. Je suis plus patient, plus calme. Mes colères sont moins fréquentes. J’écoute les autres sans éprouver de l’ennui (ou moins, selon mon interlocuteur). Je savoure le temps avec mes proches. Je suis moins stressé, j’ai moins l’impression qu’il y’a des millions de choses à faire. Ma todo list et mon inbox email sont sous contrôle permanent. Mieux : ça vaut ce que ça vaut, mais j’ai accompli en octobre autant de tâches de ma liste de todos qu’en 3 mois habituellement.

En fait, c’est simple : lorsque je lance mon ordinateur, au lieu d’aller dans mon navigateur, je vais dans ma todo list et je choisis ce que j’ai envie de faire à ce moment.

Je recommence à écrire de la fiction, même si ce n’est encore que le tout début. Je tiens un journal de manière régulière et ça me plait.

Pour procrastiner, je réfléchis à mon workflow général de travail, j’ai eu une phase où j’ai testé pas mal de logiciels. Voire j’écris dans mon journal. Ce n’est certes pas le plus productif, mais c’est au moins une procrastination qui n’est pas nocive. Autre procrastination : je teste le fait de m’autoriser un lecteur de flux RSS, mais avec seulement quelques sources sélectionnées. Si un flux a un volume trop important ou poste des articles trop génériques, je le supprime immédiatement. D’ailleurs, je cherche des flux RSS d’individus qui partagent leurs idées. Pas de news, pas d’actualités, mais des lectures un peu fouillées qui poussent à la réflexion. Le but n’est pas d’être informé, mais d’être inspiré. Vous avez des recommandations ?

Si je lis toujours autant de fiction qu’avant, j’ai par contre ajouté à mon régime des véritables livres sur arbres morts de non-fiction, livres qui trainent un peu partout dans la maison et que je saisis quand l’envie m’en prend.

Bon, évidemment, tout n’est pas rose. Je ne publie pas beaucoup plus qu’avant. Je ne suis toujours pas un parangon de patience et, si je me distancie de mon téléphone, je reste accro à mon clavier. En fait, je deviens de plus en plus accro à l’écriture, ce qui est une bonne chose, mais les tâches ménagères ne m’intéressent pas plus qu’avant.

Un autre chantier en cours, outre le fait que je commence à lâcher prise sur les opportunités manquées, c’est celui de l’ego. J’ai toujours eu beaucoup d’égo et le fait d’être parfois appelé « blogueur influent » n’aide pas à rester modeste. Mon blog est une quête de visibilité personnelle, de reconnaissance. J’ai le désir profond de devenir célèbre grâce à mon travail. Les réseaux sociaux exploitent cette faiblesse en me faisant guetter les followers, les likes, etc.

Me déconnecter me permet de m’affranchir de cela. Je ne sais pas si ce que je poste a du succès ou non, je ne sais pas si je suis en train de devenir (un tout petit peu) célèbre ou non. Et, du coup, une partie de mon cerveau commence à lâcher prise sur cet objectif irrationnel. L’effet net est que je sens grandir en moi l’envie de me concentrer sur des projets à plus long terme plutôt que de tenter d’obtenir une gratification immédiate. Mais je ne suis clairement qu’au début.

D’ailleurs, en parlant de gratification, je n’ai jamais reçu autant de mails de lecteurs et de dons sur Tipeee, Paypal ou en Bitcoin. Cela me touche très profondément de voir que certains prennent le temps pour réagir à mes billets, pour me recommander des lectures, pour me remercier. Je suis reconnaissant d’avoir cette chance. Merci !

Enfin, je constate également que j’ai moins envie d’acheter des gadgets sur Kickstarter ou du matériel de vélo. Je ne sais pas si c’est ma volonté de minimalisme ou bien si c’est un effet de ma déconnexion (ou bien les deux), mais, clairement, je prends conscience que le shopping, même en ligne, est un passe-temps, une procrastination pour éviter de faire autre chose. La déconnexion a donc un impact positif sur notre objectif de famille de dépenser moins pour devoir gagner moins.

D’ailleurs, mon cerveau perd de la tolérance face à toutes les sollicitations sensorielles. Voir un réseau social sur un écran m’agresse par les couleurs, les notifications, les publicités. Les écrans publicitaires me font mal aux yeux dans la rue. Bon, c’était déjà un peu le cas avant.

Cette déconnexion me permet également d’apprécier la chance que j’ai d’avoir ma femme et ma famille.

Car, soyons honnête, mon misanthrope égoïsme est peu compatible avec la vie de famille. Si j’adore ma famille et mes enfants, il m’est impossible de ne pas imaginer que si j’étais resté célibataire, j’aurais tout le temps que je veux pour écrire, regarder des films, faire plein sport, écouter du métal à fond. Que je n’aurais pas dû annuler en catastrophe l’enregistrement d’un épisode du ProofOfCast pour éponger du vomi dans toute la chambre.

Mais cette déconnexion, je l’ai souhaitée parce que je voulais améliorer ma vie de famille. Parce que je voulais améliorer la qualité de nos relations et que ma femme avait pointé du doigt mon addiction à mon smartphone.

Aujourd’hui, cette déconnexion m’apporte bien plus que ce que je n’imaginais. Du coup, je réalise que sans femme et sans enfants, je n’aurais sans doute pas entrepris cette expérience. J’aurais certainement continué à me perdre, à courir après ma petite gloriole, à guetter les opportunités pour avoir quelques miettes de gloire.

Bref, cette déconnexion me permet d’apprendre à apprécier qui je deviens tout en me rendant compte que ce que je suis est désormais indissociable de ma femme et de mes enfants, que le Ploum alternatif resté célibataire m’est de plus en plus étranger.

Et tout ça après seulement un mois sans réseau sociaux…

Photo by Will van Wingerden on Unsplash

=> https://unsplash.com/photos/cZVthlrnlnQ?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText Will van Wingerden
=> https://unsplash.com/?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText Unsplash
