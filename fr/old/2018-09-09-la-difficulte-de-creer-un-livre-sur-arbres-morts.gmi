# La difficulté de créer un livre sur arbres morts
=> files/old/Aristide-campagne-3.jpg

*Petit exercice de transparence comptable pour l’impression des aventures d’Aristide.*

Grâce à votre soutien, notre projet Ulule pour soutenir les aventures d’Aristide est un franc succès avec plus de 200 livres commandés alors que nous sommes tout juste à la moitié de la campagne !

=> https://www.ulule.com/aristide-lapin-cosmonaute/ notre projet Ulule pour soutenir les aventures d’Aristide

Je suis un fervent partisan de la transparence, que ce soit en politique ou dans une démarche qui est, il faut l’avouer, commerciale. J’avais donc envie de vous détailler pourquoi ce cap de 200 exemplaires est un soulagement financier : c’est à partir de 200 exemplaires que nous commençons à rentrer dans nos frais !

Pour imprimer 200 exemplaires, quantité minimale d’impression, l’imprimeur demande 12€ par exemplaire. Ajoutez à cela les frais d’envoi (entre 1€ et 2€ pour l’emballage plus 5€/6€ d’envoi en Belgique et près de 10€ pour l’envoi en France), cela fait que chaque exemplaire nous coûte un minimum de 18€. C’est pour cette raison que j’ai décidé de livrer une partie en vélo : c’est à la fois un beau challenge mais cela permet aussi de faire de sérieuses économies pour financer l’envoi à des lecteurs de l’hexagone !

Rien que l’emballage nous coûtait 2€ pièce si nous commandions 150 cartons mais passe à 1€ pièce si nous en commandons 450. 450 cartons ou un mètre cube à stocker à la maison en attendant les livres !

À cela ajoutez les 8% de commission Ulule et différents frais fixes (comptable, frais administratifs uniques, etc). Il apparait du coup qu’avec 100 exemplaires, nous aurions du mettre de notre poche. Mais Vinch et moi avions tellement envie de voir Aristide exister entre les mains des lecteurs !

=> https://www.vinch-atelier.be Vinch

Au-dessus de 200 exemplaires, nous passons chez l’imprimeur dans la tranche supérieure (500 exemplaires). Et là, miracle, chaque exemplaire ne nous coûte plus que 7€ (les autres frais étant identiques). Au total, cela revient à dire que quelque part entre 200 et 300 exemplaires, la campagne s’équilibre financièrement et finance un excès de livres. Nous ne gagnerons pas d’argent directement mais nous pourrons peut-être nous rémunérer en vendant le trop plein d’exemplaires après la campagne (ce qui, après un bref calcul, ne paiera jamais le centième du temps passé sur ce projet mais c’est mieux que d’y aller de notre poche).

À partir de 1000 exemplaires, le livre ne couterait plus que 4€ à fabriquer. Là ça deviendrait vraiment intéressant. Mais encore faut-il les vendre…

C’est peut-être pour ça que j’aime tant publier mes textes sur mon blog en vous encourageant à me soutenir à prix libre sur Tipeee, Patreon ou Liberapay. Vendre, c’est un métier pour lequel je ne suis pas vraiment taillé.

=> /no-proprietary-service.html Tipeee
=> /no-proprietary-service.html Patreon
=> https://liberapay.com/ploum/ Liberapay

Mais Aristide avait tellement envie de connaître le papier, de se faire caresser par les petites mains pleines de terre et de nourriture, de se faire déchirer dans un excès d’enthousiasme, d’exister dans un moment complice d’où les écrans sont bannis.

Merci de lui permettre cela ! La magie d’Internet, c’est aussi de permettre à deux auteurs d’exister sans avoir le moindre contact dans le monde de l’édition, de permettre à tout le monde de devenir auteur et créateur.

PS: comme nous sommes désormais sûr de le produire 500 exemplaires, n’hésitez pas à recommander Aristide à vos amis. Cela me ferait trop de peine de voir quelques centaines d’exemplaires pourrir dans des cartons ou, pire, être envoyés au pilon ! Plus que 20 jours de campagne !

=> https://www.ulule.com/aristide-lapin-cosmonaute/ recommander Aristide à vos amis
