# Reliez Ottignies et Bruxelles en vélo grâce au VER
=> files/old/20160624_091041.jpg

Une piste cyclable parfaitement sécurisée et sur site propre pour relier Ottignies à Bruxelles en seulement 16km ? Le tout entièrement financé par l’argent du contribuable ?

Un rêve ?

En fait, c’est déjà une réalité que vous avez déjà financé à hauteur de plusieurs milliards d’euros.

Seul petit problème à régler : les contribuables qui ont financé cette merveille sont interdits d’accès.

Car cette merveilleuse piste cyclable, c’est le tracé du futur RER. Un chantier qui a déjà englouti des milliards d’euros d’argent public pour un résultat qui serait, au mieux, utilisable en 2024. Mais les prévisions les plus réalistes tablent pour une arrivée du RER aux alentours de 2030. Si jamais il est finalement terminé et n’est pas déjà périmé avant même sa mise en service.

De Ottignies à Bruxelles (gare de Boitsfort), il existe donc une véritable route goudronnée, lisse, plate, sans aucune côte et sans aucun trafic. Cette route en parfait état ne s’approche jamais à moins de trois mètres des voies de chemin de fer et en est toujours séparé par une bordure et un écran minimal de végétation. Nous l’avons baptisé le VER, Vélo Express Régional.

=> ../files/old/20160624_085512-ANIMATION-400x226.gif 20160624_085512-ANIMATION
Cliquez pour voir l’animation


L’association cycliste Gracq a très récemment annoncé que certains de ses membres utilisaient certains tronçons du VER. La réaction d’Infrabel, gestionnaire des voies, ne s’est pas fait attendre : l’accès à cette route est strictement interdit voire serait dangereux.

=> http://www.rtbf.be/info/regions/detail_l-homme-qui-pedalait-sur-le-chantier-du-rer?id=9326877 a très récemment annoncé que certains de ses membres utilisaient certains tronçons du VER

Cette route en parfait état devrait donc rester inutilisée et se dégrader inutilement pendant au minimum une décennie.

C’est pour en avoir le cœur net que cinq cyclistes ont décidé de relier Ottignies à Boitsfort en vélo, un jour de grève générale : Stéphane, Nils, Natacha, Yves et votre serviteur.

=> ../files/old/20160624_053113.jpg 

Preuve que l’idée est dans l’air du temps : nous préparions notre action alors qu’aucun de nous n’était au courant de l’action très similaire du Gracq.

Le résultat est sans appel : seul le tronçon entre les gares de Genval et La Hulpe (2km) n’est pas encore aménagé. Le passage est strictement impossible sans s’approcher dangereusement des voies ou en les traversant (l’aménagement étant fait de l’autre côté des voies). Il est donc impératif de quitter le VER avant la gare de Genval et de le reprendre à la gare de La Hulpe, impliquant un détour de 15 minutes.

Le reste du trajet se fait de manière entièrement sécurisée sur une route large et dégagée. Deux passages d’une centaine de mètres sont en sable et en terre mais restent praticables en VTT, le premier à Profondsart et le second dans la gare de Boitsfort même.

=> ../files/old/20160624_084618-400x225.jpg 20160624_084618
Passage boueux à Profondsart


Au total ? Un VER d’un peu plus de 16km sur un terrain absolument plat. Pour un cycliste entraîné, ce trajet est réalisable en une demi-heure. Et pour ceux qui préfèrent prendre le temps et admirer le cadre très agréable, 45 à 50 minutes semble un grand maximum. Tant que la jonction Genval vers La Hulpe n’est pas finalisée, une petite heure semble un temps raisonnable, même pour un cycliste néophyte.

=> ../files/old/IMG_20160624_081417-400x225.jpg IMG_20160624_081417
Une partie du trajet est même couverte


Autre obstacle imprévu : une étendue de verre brisé dans la gare de Rixensart qui déchirera le pneu de votre serviteur, le forçant à faire demi-tour tandis que les quatre autres continuaient vers Boitsfort.

Mais rien de mieux pour vous convaincre qu’une petite vidéo (d’où il ne manque que les derniers kilomètres).

=> https://indymotion.fr/w/7pqQVv6SRtSJG4c3HGpKaY Remplaçons le RER belge par le VER, sur Peertube

Alors, est-ce dangereux ?

Oui, clairement. Le fait de devoir faire un détour entre Genval et La Hulpe nécessitant de passer par des rues ouvertes au trafic automobile et sans pistes cyclables est certainement la partie la plus dangereuse du trajet. Un danger que les cyclistes vivent au quotidien mais qui pourrait désormais être évité grâce au VER.

En dehors du tronçon Genval/La Hulpe, les trains restant toujours à une bonne distance ne peuvent en aucun cas représenter le moindre danger.

Est-ce légal ?

Non. Bien qu’il n’y ait ni dégâts matériel, ni victimes, cette action que nous avons entreprise est illégale.

Cette illégalité est-elle justifiable ?

Suite à l’action du Gracq, la réaction d’Infrabel ne s’est pas fait attendre : des bacs de ciment ont été volontairement placés pour bloquer l’accès aux cyclistes. Cette réaction vous semble-t-elle responsable et utile ?

=> ../files/old/IMG_20160624_081549-400x225.jpg IMG_20160624_081549
Infrabel ne supporte pas la concurrence intolérable du vélo


Le pouvoir politique qui lutte pour la mobilité, la réduction des polluants peut-il légitimement décider que les cyclistes n’ont pas le droit d’être protégés et ne doivent en aucun cas bénéficier du VER ?

Ces politiciens ne seront-ils pas moralement responsables si un cycliste se fait renverser par une voiture car il a décidé de respecter l’interdiction d’utiliser le VER et roule au milieu de routes pensées pour l’automobile ?

Un état démocratique qui a financé le VER avec l’argent du contribuable a-t-il le droit d’interdir ces mêmes contribuables de l’utiliser ?

Ne devrait-on pas au contraire finaliser au plus vite la jonction Genval/La Hulpe et inaugurer une formidable voie verte sur laquelle pourrait naître une véritable économie de proximité : buvette pour cyclistes assoiffés, ateliers de réparation, salles de réunions et espaces de travail.

La créativité est sans limite. Il ne reste plus qu’à finaliser l’effort accompli.

Mesdames et Messieurs les politiciens, vous avez aujourd’hui l’opportunité de transformer le plus grand des travaux inutiles belges, véritable gabegie d’argent public (le RER) en un formidable investissement écologique et économique, le VER.

Mesdames et messieurs les politiciens, il suffit d’une impulsion pour finaliser le VER. La balle est dans votre camp !

Photo de couverture : départ du VER depuis le pont de Jassans à Ottignies. Photos et vidéos par Stéphane Vandeneede et moi-même.
