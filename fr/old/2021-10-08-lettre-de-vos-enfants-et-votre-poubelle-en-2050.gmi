# Lettre de vos enfants et votre poubelle en 2050
=> files/old/simon-hurry-Fv-Y6lnl6pA-unsplash.jpg

Chers parents,

Nous sommes le 8 octobre 2051. J’ai aujourd’hui 30 ans et un peu de recul sur mon enfance, mon éducation et le monde dans lequel j’ai grandi.

Ma génération est confrontée à une problématique sans précédent dans l’histoire de l’humanité : devoir gérer les déchets de la génération précédente.

Jusqu’aux années 1970, la planète se régénérait naturellement. Les déchets humains étaient absorbés et recyclés spontanément. À partir de votre génération, ce ne fut plus le cas. Vous fûtes la toute première génération de l’histoire à produire et consommer plus que ce que la terre ne le permettait.

Vous nous laissez sur les bras l’excédent de déchets.

Le pire, c’est que vous le saviez.

Quand je me réfère aux archives et à mes souvenirs de prime jeunesse, votre époque n’était guère accueillante. Vous aviez des voitures consommant de l’énergie fossile et des fumeurs au cœur des villes ! Aujourd’hui, la voiture électrique ne sert que pour se déplacer entre les centres citadins. Elles sont strictement interdites dans les zones urbaines où tout se fait à pied, à vélo, à trottinette ou en taxi-tram autonome. Malgré tout, notre air est moins respirable que le vôtre !

Merci d’avoir œuvré à cette transformation. Peut-être était-ce le minimum à faire pour que nous survivions. Car si vous avez agi, souvent avec beaucoup de bonne volonté, c’était rarement dans le bon sens.

Comme cette manie que vous aviez de vouloir économiser l’électricité. J’ai du mal à croire que, même à votre époque, l’électricité n’était pas abondante et peu polluante pour les individus. Si j’en crois les archives, les années 2020 voyaient de réguliers pics de surproduction d’électricité dus aux panneaux solaires et vous démanteliez des centrales nucléaires parfaitement fonctionnelles. Vous perdiez vraiment votre temps à vous convaincre de mettre des ampoules économiques si polluantes à produire ? Un peu comme le coup de faire pipi dans la douche ou de ne pas imprimer les emails. Vous pensiez sérieusement que nous allions vous remercier pour cela ?

Vous semblez avoir dépensé tellement d’énergie et de temps pour tenter, parfois vainement, d’économiser 10% de votre consommation privée de ce qui n’était de toute façon qu’une goutte d’eau face à l’industrie. Vous culpabilisiez les individus alors que votre consommation personnelle représentait le quart de l’électricité consommée globalement (dont le tiers uniquement pour le chauffage). Même si vous aviez arrêté de consommer complètement de l’électricité à titre individuel, cela n’aurait eu qu’un impact imperceptible pour nous.

Par contre, vous nous laissez sur le dos des gigatonnes de déchets de ces appareils dont plus personne ne voulait, car ils consommaient un peu trop. Chaque année, culpabilisés par le marketing, vous vous équipiez d’une nouvelle génération d’appareils qui consommaient « moins », de vêtements « fair trade », de gourdes prétendument recyclables et de vaisselle en bambou. Le tout ayant fait le tour du monde pour rester brièvement dans vos armoires avant de combler les décharges sur lesquelles nous vivons désormais.

Vous semblez vous être évertué à acheter le plus de gadgets inutiles possibles, mais en vous rassurant, car, cette année, la fabrication du gadget en question avait émis 10% de CO2 en moins que celui de l’année précédente et que l’emballage était « presque entièrement recyclable ». Ses composants avaient fait trois fois le tour du globe, mais, rassurez-vous, deux arbres avaient été plantés. Aujourd’hui encore, nous avons du mal à comprendre comment vous aviez matériellement le temps de faire autant d’achats. Il semblerait que vous deviez passer plus de temps à faire « du shopping » et à remplir vos armoires qu’à réellement utiliser vos achats. Armoires pleines à craquer que nous devons vider les jours qui suivent votre décès, moitié pleurant votre perte, moitié râlant sur votre propension à tout garder.

Consommer des gadgets était peut-être la seule façon que vous pouviez imaginer pour poursuivre la lubie de votre génération : créer des emplois. Toujours plus d’emplois. Une partie de ces emplois consistaient d’ailleurs explicitement à vous convaincre d’acheter plus. Comment avez-vous moralement pu accomplir ces tâches explicitement morbides ? Parce que c’était votre travail, certainement. L’histoire démontre que les pires exactions furent commises par des gens dont « c’était le travail ». Pousser les autres à consommer fait désormais partie de ces crimes historiques contre l’humanité. Utiliser le prétexte écologique pour consommer encore plus ne fait qu’aggraver la culpabilité de ceux qui furent impliqués.

Pendant 40 ans, vous avez eu comme politique de créer autant d’emplois que possible, emplois dont le rôle premier était de transformer les ressources en déchets. Pendant 40 ans, vous vous êtes démenés pour remplir le plus vite possible votre poubelle planétaire : nous, l’an 2050.

Nous, vos enfants, sommes votre poubelle. Ce pays lointain qui vous semblait abstrait, nous vivons dedans.

Il a fallu attendre notre génération pour décider que tout vendeur d’un bien ou d’un emballage ni immédiatement consommable ni naturellement dégradable était tenu de racheter ses produits à la moitié du prix, quel que soit l’état. De faire ainsi remonter la chaîne à chaque pièce, chaque composant. Au final, le producteur est en charge de l’évacuation et forcé de gérer son impact.

Bien sûr, il y’eut une énorme perturbation dans les services logistiques qui ont, soudainement, dû fonctionner dans les deux sens. Les industries se sont adaptées en tentant de développer des produits qui dureraient le plus longtemps possible et en favorisant la réparabilité ou la démontrabilité. Soudainement, c’était un argument de vente. Le marketing n’a pas mis longtemps à retourner sa veste et à tenter de vous convaincre que la location, même à très longs termes, était une liberté par rapport à la possession. La réparation a créé une activité économique que vous assimileriez peut-être à des emplois. Paradoxalement, une activité économique naturelle s’est développée le jour où nous avons arrêté de tenter de la créer artificiellement. Où nous avons considéré qu’il devait être possible de vivre sans travail. Nous espérons, de cette manière, redevenir une génération qui ne produit pas plus de déchet que ce que la planète peut absorber. Que ce soit en CO2, en microparticules, en métaux lourds.

Le réchauffement climatique et les feux de forêt ne nous aident pas, mais nous avons bon espoir d’y arriver.

Il n’empêche que, même si on y arrive, on doit toujours se coltiner vos 50 ans de déchets. Ils ne sont pas prêts de disparaitre vos jouets en plastique bon marché pas cher achetés pour calmer le petit dernier dans le magasin ou le téléphone super révolutionnaire devenu un presse-papier has-been 2 ans plus tard. Sans compter que le prix de leur fabrication et de leur transport nous accompagne à chacune de nos inspirations dans l’air chargé de CO2.

Chacune de nos respirations nous rappelle votre existence. Nous fait nous demander pourquoi vous n’avez pas agi ? Pourquoi avons-nous dû attendre de vous enterrer ou vous mettre à la retraite pour pouvoir faire quelque chose ?

Et puis certains d’entre nous me racontent qu’ils ont eu des parents qui fumaient. Qu’il était normal de fumer dans les rues à proximité des enfants voir dans les maisons ou les voitures.

Votre génération dépensait donc de l’argent dans le seul et unique but de se détruire la santé, de détruire la santé de ses propres enfants tout en polluant l’atmosphère, tout en polluant l’eau ? Vous financiez une florissante industrie dont le seul et unique objectif était la destruction de la santé de ses clients, des enfants de ses clients, de l’entourage de ses clients et de la nature ? On estime aujourd’hui que près de 1% du CO2 excédentaire dans l’atmosphère est dû à l’industrie du tabac. On s’en serait bien passé.

Par contre, il faut le reconnaitre, nous avons plein de photos et de documents historiques qui prouvent que vous étiez militants, que vous signiez des pétitions et que vous « marchiez pour le climat ». En fumant des clopes.

C’est devenu une moquerie récurrente quand on parle de vous. La génération des écolos-fumeurs. L’image est devenue célèbre pour illustrer ce mélange de bonne volonté collective inutile et paresseuse, cette propension à culpabiliser les individus pour des broutilles, à accomplir des actions collectives symboliques sans enjeu et à se voiler la face devant les comportements réellement morbides.

Vous hurliez « Priorité à la sauvegarde de la planète ! ». Ce à quoi les politiciens répondaient « Tout à fait ! Priorité à l’économie et la sauvegarde de la planète ! ». Puis, la gorge un peu enrouée, chacun rentrait chez soi, satisfait. Avant d’organiser un grand atelier participatif « Méditation transcendentale et toilettes sèches » où vous vous faisiez passer un joint de tabac industriel mélangé d’herbe bio issue du potager partagé.

Notre génération est permissive. Dans beaucoup de parties du monde, l’usage de drogue récréative est autorisé ou toléré. Par contre, toute émission de particules toxiques est strictement interdite dans les lieux publics. Ce n’était vraiment pas difficile à mettre en place et la seule raison que nous voyons pour laquelle vous ne l’avez pas fait c’est que vous ne le vouliez pas.

Malgré vos discours, vous ne vouliez absolument pas construire un monde meilleur pour nous. Il suffisait de vous poser la question : « est-ce que j’ai envie que mes enfants fument ? » Même parmi les fumeurs invétérés, je pense que très peu auraient répondu par l’affirmative. « Ai-je envie que mes enfants subissent le poids écologique de vingt téléphones portables pour lesquels j’ai, au total, dépensé un an de salaire ? De milliers de kilomètres de diesel et de cinq voitures de société ? ». Il aurait suffi de vous poser la question. Interdire la cigarette dans l’espace public aurait été une manière toute simple d’affirmer que vous pensiez un peu à nous.

Mais vous ne pensiez pas à nous. Vous n’avez jamais pensé à nous. Vous avez juste voulu vous donner bonne conscience en ne changeant strictement rien à vos habitudes, même les plus stupides. Pour votre décharge, vous n’avez pas hérité non plus d’une situation facile de vos propres parents, cette génération qui après une gueule de bois post-mai 68, s’est accaparé toutes les richesses et les a gardées en votant Reagan/Tatcher et allongeant son espérance de vie. Sans jamais vous laisser votre place.

Quand nous en discutons entre nous, nous pensons que, finalement, nous avons de la chance d’être là. On doit gérer vos poubelles, mais vous auriez pu, pour le même prix, nous annihiler. Vous nous avez traités comme un pays vierge, un pays lointain à conquérir pour en exploiter les ressources à n’importe quel prix. Un pays qui vous appartenait de droit, car les autochtones n’offraient aucune résistance active.

Ce qui est fait est fait. Il nous reste la tâche ardue de ne pas faire pareil et tenter d’offrir un monde meilleur à nos enfants. Non pas en prétendant penser à eux pour nous donner bonne conscience, mais en tentant de penser comme ils le feront. En les traitant comme un pays ami à respecter, un partenaire. Non plus comme une poubelle sans fond.

Signé : votre futur

Note de l’auteur : L’idée de considérer le futur comme un pays avec qui entretenir des relations internationales m’a été inspirée par Vinay Gupta lors d’une rencontre au parlement européen en 2017. Vinay a ensuite publié une analyse très intéressante où il suggère de voir toutes nos actions à travers le filtre du futur que nous réservons aux enfants de cette planète.

=> https://medium.com/@vinay_12336/a-simple-plan-for-repairing-our-society-we-need-new-human-rights-and-this-is-how-we-get-them-cee5d6ededa9 https://medium.com/@vinay\_12336/a-simple-plan-for-repairing-our-society-we-need-new-human-rights-and-this-is-how-we-get-them-cee5d6ededa9

Bien que ces deux inspirations n’aient pas été conscientes au moment de la rédaction de ce texte, elles m’apparaissent comme indubitables à la relecture.

Photo by Simon Hurry on Unsplash

=> https://unsplash.com/@bullterriere?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText Simon Hurry
=> https://unsplash.com/?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText Unsplash
