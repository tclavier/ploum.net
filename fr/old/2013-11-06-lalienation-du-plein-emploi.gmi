# L’aliénation du plein emploi
=> files/old/workers.jpg

Asseyez-vous une seconde et mettez-vous dans la peau du bambin rêveur que vous fûtes. Tentez d’imaginer le futur avec vos yeux d’enfants. Un avenir où les machines, les robots se chargeraient de toutes les tâches que nous trouvons déplaisantes. Un monde où chacun pourrait se concentrer sur l’amélioration du bien commun, à commencer par lui-même. Un univers où nous consacrerions son temps à l’éducation, aux activités artistiques, sociales et culturelles voire, tout simplement, au plaisir de vivre.

Techniquement, tout cela semble aujourd’hui possible. La preuve: le monde est plus riche que jamais. Et nous progressons sur cette voie: il y a de moins en moins de travail ! Sans compter l’immense majorité des emplois qui pourraient être rationalisés. Merveilleux !

Mais alors…

À quel moment nous sommes-nous fourvoyés jusqu’à élire des dirigeants qui nous promettent de « créer du travail » ? À partir de quand sommes-nous devenus mentalement dégénérés au point de considérer le mot « plein emploi » comme un idéal plutôt qu’une insulte au progrès et à la modernité ? Par quelle perversion totale de l’esprit en arrivons-nous à accuser les artistes de rue, les créateurs peu reconnus ou les personnes qui se consacrent à leur famille de ne pas avoir « un vrai travail » ? Pourquoi ce qui est une utopie pour nos yeux d’enfant se révèle-t-il, soudainement, « une crise » contre laquelle nous luttons de toutes nos forces ?

Peut-être est-il temps de réaliser que la crise, la vraie, nous l’avons nous même amorcée en acclamant ceux qui nous promettaient de nous faire travailler, en nous glorifiant de passer d’inutiles heures en cravate dans un cube grisâtre, en stigmatisant ceux qui avait l’air de souffrir différement voire, infamie suprême, d’être heureux sans se tuer à la tâche !

Quelles que soient les méthodes invoquées, je suis désormais convaincu que les slogans de « relance », « relocalisation », «reprise économique » ne sont que des pierres pour nous enfoncer encore plus profondément. Asservi par des siècles de travail, l’homme a peur de cette nouvelle liberté qui s’offre à lui. Il lutte pour renforcer ses propres chaînes. La solution viendra de ceux qui auront le courage de monter au front politique en disant « Il y a encore trop de travail, nous allons en supprimer autant que possible ! »

La panique résultant de la soudaine ouverture de notre cage millénaire n’est-elle pas dangereuse ? Ne risque-t-elle pas d’avoir des effets négatifs ? Peut-être. Mais doit-on pour autant garder la cage définitivement fermée ? N’avons-nous pas le devoir, nous, première génération disposant de la clé, de faire grandir nos descendants loin de cette oppressante prison ?

Concrètement, je vous invite à lire pourquoi la création de l’emploi est nuisible et pourquoi vous êtes, sans peut-être le savoir, en faveur du revenu de base. Nous disposons aujourd’hui d’une occasion unique de mettre le revenu de base sur la table politique de la commission européenne. Pour cela, je vous invite à signer l’initiative européenne en faveur du revenu de base (vos données personnelles sont nécessaires pour la validité du processus et stockées sur un serveur officiel de la commission européenne, aucun danger de récupération commerciale), à parler autour de vous du revenu de base et à soutenir la campagne de financement nécessaire pour obtenir le million de signatures requis. Merci ! L’image d’illustration est de Chris Brown.

=> /creusez-un-trou/ pourquoi la création de l’emploi est nuisible
=> /pourquoi-vous-etes-sans-le-savoir-favorable-au-revenu-de-base/ pourquoi vous êtes, sans peut-être le savoir, en faveur du revenu de base
=> https://ec.europa.eu/citizens-initiative/REQ-ECI-2012-000028/public/index.do?uid=1383755718620&amp;oct_path=%2Fsignup.do&amp;initiativeLang=fr&amp;oct_system_state=OPERATIONAL&amp;oct_collector_state=true%C2%A4tLanguage=fr&amp;showMap=true à signer l’initiative européenne en faveur du revenu de base
=> http://www.kisskissbankbank.com/help-us-bring-basic-income-closer-to-reality-in-europe--2 à soutenir la campagne de financement nécessaire pour obtenir le million de signatures requis
=> http://www.flickr.com/photos/92496717@N00/5559517904 Chris Brown
