# Les pistes cyclables n’existent pas, ce sont des parkings !
=> files/old/20160916_125643-1.jpg

À force de parcourir le brabant-wallon à vélo, j’ai eu une révélation : les pistes cyclables n’existent pas, ce sont tout simplement des parkings.

C’est aussi simple que cela et cela explique tout.

Dois-je accuser ce conducteur de me faire risquer ma vie en m’obligeant à brusquement me rabattre sur la chaussée sans avoir la moindre visibilité ?

=> ../files/old/20161004_091359-1.jpg 

Pas si l’on considère que cette bande est tout simplement un parking. Cet usage est encouragé par les pouvoirs publics qui utilisent cet espace pour mettre une signalisation temporaire.

=> ../files/old/20161025_182525.jpg 

Au moins, l’espace sert à quelque chose et cet usage ne bloque presque pas les possibilités de parking.

=> ../files/old/20161025_183053.jpg 

Au fond, si cela fonctionne pour les installations temporaires, pourquoi n’en serait-il pas autant pour les installations permanentes ?

=> ../files/old/20160917_164851.jpg 

Le terme « piste cyclable » est une sympathique blague mais soyons sérieux : qui utilise réellement un vélo pour se déplacer ? Alors qu’une route se doit d’être dégagée immédiatement, notre société considérant un embouteillage de plusieurs heures comme une catastrophe, les pistes cyclab… pardon les voies de parking, elles, peuvent rester bloquées plusieurs jours.

=> ../files/old/20161121_093321.jpg 

Bien sûr qu’un cycliste ne pourra pas passer et devra risquer littéralement sa vie sur une voie rapide pour continuer son chemin. Mais il n’avait qu’à prendre la voiture comme tout le monde.

=> ../files/old/20160917_175441.jpg 

Plus sérieusement, je suis désormais convaincu que les pistes cyclables servent à tout sauf à passer à vélo. La preuve ? Il est explicitement demandé aux automobilistes de ne pas se garer sur les pistes cyclables lors de certaines manifestations.

=> ../files/old/20160917_164813.jpg 

Ces panneaux empêcheraient le passage d’un vélo mais nous savons désormais que les cyclistes n’existent pas vraiment.

=> ../files/old/20160917_165149.jpg 

À moins que les pistes cyclables ne soient pas des parkings ?

=> ../files/old/20160916_125643-1.jpg 

Mais que seraient-elles dans ce cas ? J’ai ma petite idée… (à suivre)

Les photos de cet article et du suivant illustrent le combat quotidien d’un cycliste à travers les communes de Grez-Doiceau (bourgmestre MR), Chaumont-Gistoux (bourgmestre MR, Ecolo dans la majorité), Mont-Saint-Guibert (bourgmestre Ecolo) et Ottignies-Louvain-la-Neuve (bourgmestre Ecolo).
