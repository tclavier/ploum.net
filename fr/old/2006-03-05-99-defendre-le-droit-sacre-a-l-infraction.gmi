# Défendre le droit sacré à l’infraction
=> files/old/redlight.jpg

Il est de ces débats où l’on a parfois l’impression que la discussion se réduit à des détails techniques alors que l’intuition nous fait bien sentir que le problème est beaucoup plus fondamental. Prenons l’exemple de la conquête spatiale :
– Les robots sont bien moins chers et plus efficaces que les hommes sur Mars !
– Jamais un robot ne remplacera un homme ! Il faut envoyer des humains !
– Vous pensez pas qu’on devrait utiliser nos sous à autres choses ? Lutter contre la faim dans le monde par exemple.
– Mais le spatial est justement un vecteur de progrès. Les satellites aident déjà à prévenir les famines.
– …

Vous ne remarquez rien ? Le débat est ramené à des arguties techniques. On n’entendra que rarement une voix fuser :
– L’Homme (avec un H majuscule) se doit d’explorer, de tenter de progresser. La stagnation et la frilosité sont le premier signe de déclin d’une civilisation.

Quand je lis les billets sur les DRMs un peu partout sur la blogosphère, le même sentiment m’envahit. On est en train de chipoter pour savoir si ce serait légal ou non de contourner une méthode de protection, d’avoir un droit à la copie privée. Mais cela me semble bien plus fondamental que cela : qu’en serait-il de notre libre arbitre et de notre droit à ne pas respecter la loi ?
Quid de notre droit fondamental à l’infraction ?

=> http://standblog.org/blog/2006/03/02/93114686-quelques-verites-sur-les-drm-et-dadvsi billets
=> http://standblog.org/blog/2006/03/03/93114691-la-grosse-erreur-des-drm sur
=> http://standblog.org/blog/2006/03/03/93114690-actu-dadvsi les
=> http://boumcke.fritalk.com/dotclear/index.php?2006/01/16/17-un-billet-qui-sent-bon-la-cannelle DRMs
=> http://www.lemachin.net/blog/index.php/2006/03/03/49-ou-l-on-reparle-des-drm partout
=> http://www.opossum.ca/cotte/archives/002725.html sur la
=> http://vacnor.free.fr/blog/index.php?2006/03/04/10-les-drm-ou-comment-faire-chier-le-client blogosphère

Au cours des derniers siècles, nos démocraties se sont basées sur certains principes fondamentaux dont la responsabilité individuelle et le célèbre « Nul n’est censé ignorer la loi ! ». Notre société comporte extrêmement peu, voire pas du tout, de mesures techniques visant à nous forcer à respecter la loi. La loi n’est pas faite pour être respectée. La loi est faite pour ne pas être surpris en train de la violer.

=> ../files/old/barriere.png Attention barriere

### Le libre arbitre

L’exemple de la sécurité routière est particulièrement frappant. Chaque année, les accidents de voitures font leur nombre de morts, de paralysés à vie, de blessés graves et de dégâts matériels importants. Un problème autrement préoccupant que la copie de musique, vous en conviendrez. Pour lutter contre cela, la loi est très claire : limitations de vitesse, code de la route, homologation des véhicules, ceintures de sécurité, conduite en état d’ivresse prohibée.

Or, à aucun moment de cette chaîne n’apparaît la moindre contrainte technique. Toutes les voitures en circulation peuvent dépasser aisément la limitation sans que le chauffeur ne s’en rende compte. Aucun feu rouge n’est doté d’une barrière. Aucune voiture ne refusera de démarrer si la ceinture du conducteur n’est pas enclenchée. Même les barrières des passages à niveau sont agencées d’une manière qui permet le passage relativement aisé d’un véhicule. Pourtant, de telles mesures permettraient sans aucun doute de sauver des vies, d’empêcher des accidents. Non ? Or, les systèmes mis en place ne visent jamais à empêcher les infractions mais bien à détecter les fraudeurs : caméras de surveillance, radars automatiques, antivols qui activent des alarmes. Nous pouvons violer la loi. Mais il faudra alors en assumer les conséquences et c’est parfaitement logique.

Nous le sentons pourtant bien au fond de nous. Une situation imprévue peut surgir où l’on sera légitiment amené à violer la loi. Nous sommes prêts à accepter que 99% des infractions relèvent du plus pur incivisme car nous sentons confusément que ce 1% de libre arbitre est important. La loi ne doit pas être imposée, la loi doit être respectée ! Si, dans une société, une loi n’est massivement plus respectée sans pour autant mettre en péril l’intégrité physique et morale des membres de cette société, alors nous nous rendons bien compte que la loi n’est tout simplement plus adaptée. Nous choisissons de respecter la loi ou pas. Nous avons un droit fondamental à ne pas respecter la loi à condition d’en accepter les conséquences si on se fait prendre.
C’est la règle du jeu.

### De l’inutilité des mesures techniques

À ma connaissance, il n’est pas, dans l’histoire humaine, d’exemple probant de mesures techniques imposant une loi. Prenez les systèmes de protection des logiciels, c’est un exemple parfait.

Aujourd’hui, les protections des logiciels sont devenues d’une complexité redoutable. À tel point que pirater un logiciel est bien plus aisé que d’être honnête. Avez-vous déjà essayé d’utiliser légalement un anti-virus ? Illégalement, rien de plus facile : on télécharge le logiciel, on télécharge le « crack » et nous voici avec une protection optimale et une mise-à-jour disponible ad vitam aeternam. Pour le client honnête, ce sera cependant un véritable parcours du combattant : entrer des numéros de série kilométriques à copier depuis le mode d’emploi en finlandais, connexion sur le site, envoi du numéro de carte visa, réception d’un code. Où faut-il le rentrer ? Enfin, après bien des chipotages, nous voici donc disposant d’une mise-à-jour pour… 15 jours ! Et puis, faudra pas oublier acheter l’option GOLD dans 15 jours pour disposer d’un an de mise-à-jour.

Bref, même les utilisateurs honnêtes utilisent des versions pirates. Qui achète donc les logiciels ? Les utilisateurs scrupuleux, qui utilisent la version pirate mais achètent une version légale, pour se donner bonne conscience. Mais aussi les sociétés, par peur d’un contrôle. On remarque donc que la mesure technique n’a aucune influence sur les ventes.

Pire : aujourd’hui, avoir la compétence technique pour installer un logiciel signifie avoir aussi la capacité à trouver le crack. Cela va de soi. D’ailleurs, cette facilité permet de répandre le logiciel et de le rendre populaire. Rendre un logiciel inpiratable signifierait assurément la fin de son succès.

=> http://fr.wikipedia.org/wiki/Gestion_des_droits_num%C3%A9riques DRMs

### De la fatuité des DRMs…

…une tempête dans un verre d’eau.

Machinalement, je compulsais les rayons du disquaire. Mon porte-feuille me criait :

– Tu ne trouves pas que t’as assez de CDs comme ça ? Pense un peu à moi ! Je ressemble à un mannequin anorexique à une réunion Weight Watchers.
Mais je ne l’écoutais pas. Mon esprit se baladait. Jamais les hommes ne se laisseront imposer une loi. Une loi se respecte, une loi ne s’impose pas par la force. De tous temps, dès qu’un groupe avait suffisament de pouvoir, il tentait de consolider par la force sa position. Mais nous savons, au plus profond de nous, que le progrès nait de ces génies qui ont su briser les lois et les tabous au moment opportun.
Tiens ? C’est un chouette CD ça ! Je le sais, je l’ai sur mon ordinateur depuis quelques mois. 20€ ? Pfff… Allez, il les vaut bien. Je le prends et me mets dans la file pour payer.
En plus, les DRMs peuvent se réduire au problème de cryptographie suivant : A envoie des informations à B. Comment faire pour que B n’y accède pas tout en permettant à B d’y accéder. Absurde. C’en est même comique. Qu’on les laisse essayer seulement, je sens qu’on va rigoler !
Ah ! Plus que deux personnes, ça va être à moi. Je me prépare à poser ma traditionnelle question au vendeur : « Est-ce qu’il s’agit d’un véritable CD lisible partout ? »
Je pose toujours cette question au vendeur, c’est devenu un réflexe. De cette manière, je peux facilement rapporter au magasin en cas de problème. Pas con le mec ! Tiens ? C’est quoi ce logo ? CopyProtectedMachin ? Ah ben pas besoin de faire la file en fait. Je vais reposer le CD en rayon. T’es content porte-feuille ?

Au fond, si ils ne veulent pas de mon argent, tant pis pour eux !

Je m’en fous en fait, la musique, je l’ai déjà sur mon ordinateur…

Photo par Florian. Relecture par Sylvestre.

=> https://www.flickr.com/photos/madflo/104891365/in/photostream/ Florian
=> http://sylvestre.org Sylvestre
