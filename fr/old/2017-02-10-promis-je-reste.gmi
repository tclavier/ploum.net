# Promis, je reste !
=> files/old/25998114085_dfa76b4042_k.jpg

— S’il-te-plait, ne m’abandonne pas ! Résiste ! Reste !

Écrasé par la douleur, je broie sans m’en rendre compte les doigts frêles posé sur le lit d’hôpital. De longues larmes lourdes et pesantes ruissellent sur ma joue, inondant le drap.

Elle tourne vers moi un regard fatigué, épuisé par la douleur.

– Je t’aime, fais-je d’une voix implorante.

D’un clignement des yeux, elle me répond.

— Je t’aime, murmure un souffle, une ébauche de sourire.

Soudain, je me sens apaisé. Mon esprit s’est clarifié. D’une voix nette et fluide, je me mets à parler.

— J’ai toujours été deux avec toi. Ma vie s’est construite sur nous. Je n’ai jamais imaginé que l’un de nous puisse partir avant l’autre. L’amour, ce concept abstrait des poètes, a guidé chacun de mes pas, chacun de mes soupirs. C’est vers toi que j’ai toujours marché, c’est pour toi que j’ai toujours respiré.

Comme un barrage soudainement détruit, j’éclate en sanglot. Ma voix se déforme.

— Que vais-je faire sans toi ? Comment puis-je encore vivre ? Ne me laisse pas ! Reste !
— Je… Je te promets de rester, balbutie une voix faible. De rester aussi longtemps que tu le souhaiteras. Je partirai seulement quand tu me laisseras partir. Promis, je reste…
— Mon amour…

Pendant des heures, je baise cette main désormais décharnée, je pleure, je ris.

— Je t’aime ! Je t’aime mon amour !

Rien n’a plus d’importance que l’amour qui nous unit.

Une poigne ferme s’abat soudainement sur mon épaule.

— Monsieur ! Monsieur !

Hébété, je me retourne.

— Docteur ? Que…

— Je suis désolé. Il n’y a plus rien à faire. Nous devons procéder à la toilette du corps.

— Hein ? Mais…

Perdu, je me tourne vers ma bien aimée. Ses yeux sont fermés, un très léger sourire illumine son visage.

— Elle dort ! Elle s’est simplement assoupie !

Dans mes doigts, sa main est devenue glacée, rigide.

— Venez, me dit doucement le docteur en m’accompagnant. Avez-vous de la famille à appeler ?

*

J’entends à peine le chauffeur démarrer et faire demi-tour derrière moi. Sous mes pieds, les familiers graviers de l’allée crissent et se mélangent. Machinalement, j’ai introduit ma clé et ouvert la porte. Un sombre silence m’accueille. Ma bouche est sèche, mes tempes bourdonnent d’avoir trop pleuré.

Sans allumer la lumière, je traverse le hall d’entrée et m’installe dans la cuisine. Ouvrant le robinet, je me sers un verre d’eau.

Un frisson me parcourt l’échine. Une porte claque. Dans l’armoire du salon, les verres en cristal se mettent à chanter.

— Qui est là ?

Une fenêtre s’ouvre violemment et un tourbillon de vent envahit la pièce, m’enveloppant dans l’air froid de la nuit.

À mon oreille, une voix proche et lointaine susurre :

— Promis, je reste…

Photo par Matthew Perkins.

=> https://www.flickr.com/photos/mattyp/25998114085 Matthew Perkins
