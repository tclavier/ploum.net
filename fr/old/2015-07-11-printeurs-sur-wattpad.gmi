# Printeurs sur Wattpad
=> files/old/7008333501_f599f8f4aa_z.jpg

TL;DR: Printeurs est désormais disponible sur Wattpad. Suivez-moi sur cette plateforme et commencez votre lecture.

=> https://www.wattpad.com/user/zeploum Suivez-moi sur cette plateforme
=> https://www.wattpad.com/story/44039821-printeurs commencez votre lecture

Il y a plus de deux ans, je m’interrogeais sur le futur du livre et l’édition. J’imaginais une plateforme où n’importe qui pourrait publier des séries, des romans, des articles, une plateforme qui abolirait la frontière entre lecteurs et auteurs. Deux ans plus tard, la plateforme qui semble avoir le plus de succès sur ce créneau est sans conteste Wattpad.

=> /le-dilemme-de-lediteur/ le futur du livre et l’édition
=> https://www.wattpad.com Wattpad

Encore peu connu chez les adultes « sérieux », Wattpad, dédié entièrement à l’écriture et à la lecture, semble rencontrer son plus grand succès… chez les adolescents. Ceux dont on dit qu’ils ne lisent pas, qu’ils n’ont pas de culture. Sur Wattpad, les adolescents se déchaînent, écrivent des kilomètres de fan-fictions, lisent, commentent, critiquent le style d’un texte.

Testé et recommandé par les blogauteurs Greg, Alias, Neil Jomunsi et analysé par Thierry Crouzet, qui y voit le contraire d’un blog, j’avoue être très en retard pour le train Wattpad.

=> http://www.antredugreg.be/mes-petits-conseils-pour-ecrire-en-serie-sur-le-web/ Greg
=> http://alias.codiferes.net/wordpress/index.php/wattpad-et-atramenta-ecriture-et-lecture-en-ligne/ Alias
=> http://page42.org/wattpad-une-nouvelle-corde-a-larc-des-auteurs/ Neil Jomunsi
=> http://blog.tcrouzet.com/2015/01/15/wattpad-deplacement-de-lespace-litteraire/ Thierry Crouzet
=> http://blog.tcrouzet.com/2015/05/01/wattpad-le-contraire-dun-blog/ le contraire d’un blog

Alors, certes, Wattpad est plein de défaut : il affiche des pubs si on n’a pas installé Adblock, il ne permet pas de rémunérer les auteurs, il n’est pas lisible sur ma liseuse Kobo (un énorme frein en ce qui me concerne). Dois-je pour autant l’éviter à tout prix ?

Au même moment, je constate que je ne progresse pas aussi vite que je le voudrais sur ma série Printeurs et que je n’arrive pas à terminer le dernier chapitre de l’Écume du temps. Pourtant, l’écriture de ces romans me procure un plaisir autrement plus important que celle d’un billet de blog. Mais je perçois un frein, une gêne.

=> /printeurs-livre-1-la-fin-de-linnocence/ Printeurs
=> /plongeon-dans-lecume-du-temps/ l’Écume du temps

Et si ce frein c’était tout simplement le fait que je n’utilise pas le bon outil ? Comme le théorise très bien Thierry Crouzet dans La mécanique du texte, l’outil façonne autant que l’auteur. Et l’outil ne se limite pas au clavier ou au stylo, il représente toute la chaine entre l’auteur et son lectorat. Ne suis-je pas en train d’essayer de sculpter du marbre avec un tournevis ?

=> http://www.senscritique.com/livre/La_mecanique_du_texte/critique/61567553 La mécanique du texte

Il est donc temps pour moi de tester ce nouvel outil et de vous annoncer l’arrivée de Printeurs sur Wattpad.

=> https://www.wattpad.com/story/44039821-printeurs Printeurs sur Wattpad

Pour les lecteurs, la recette est simple :

1. Installez l’application Wattpad sur votre smartphone ou tablette (ou créez un compte via le site web).
2. Suivez moi !
3. Commencez à lire Printeurs.
4. N’oubliez pas de voter pour chaque épisode.

Pourquoi voter ? Tout simplement car cela donne de la visibilité à l’histoire sur la plateforme Wattpad, cela attire de nouveaux lecteurs et, au final, cela peut être un incitant très fort à poster l’épisode suivant. Je fais donc l’expérience d’un pur chantage qui semble être la norme chez les utilisateurs de Wattpad : vous voulez la suite ? Votez !

=> https://www.wattpad.com/user/LioPloum Suivez moi
=> https://www.wattpad.com/story/44039821-printeurs Printeurs

Bonne lecture !

Remarque : je continuerai à publier les épisodes de Printeurs sur mon blog, par soucis d’uniformité. Mais l’expérience nous dira si cette approche vaut la peine d’être continuée.

Photo par Tim Hamilton.

=> https://www.flickr.com/photos/bestrated1/7008333501/in/photolist-bFixoe-8dh9tU-9Dmtu-mAG98y-hjbY9-N3fwr-masJP-9uekHs-e5yE39-cUxeGq-5BJrNp-7kW94J-8VRoDe-hKEF7R-8QKuz4-rBGGii-4ZD9tp-dEK32f-Zm3sr-6bDgYu-61D5nr-4RevVU-DqjS-86Nkdd-7WA9VK-7kW8N9-7kSe8v-ncMjFu-8p35CL-5Z241e-5BJkg9-9XbnwX-fWh7KP-7kSdWz-GKo4H-7kSegk-6aH1Sc-9Dhs7-5Gsigr-8mMA88-e5yChE-6xDeFF-38zrW-bw8EdY-5eYk97-7kW8FA-at2T9V-oBAv4V-68rcQL-cBBh Tim Hamilton
