# Printeurs 6
=> files/old/factory.jpg

Avertissement: cet épisode de Printeurs est particulièrement violent et certaines scènes peuvent choquer.

Saisir, assembler, visser. Saisir, assembler, visser. Mon esprit se vide et s’éteint tandis que mes mains répètent inlassablement la lancinante et éternelle sarabande. Saisir, assembler, visser. Saisir…, saisir… Le rythme fléchit, je tourne la tête pour comprendre l’origine de ce ralentissement impromptu. Un liquide brunâtre et grumeleux coule le long des jambes de 647. Elle s’est chié dessus. Pourtant, on a trois minutes toutes les quatre heures pour aller au trou. Elle n’a pas tenu. Elle s’est déconcentrée, les pièces s’accumulent sur son poste.

Elle est à portée de mon bras, je pourrais l’aider. C’est risqué. Les gardes n’apprécient pas l’entraide. Mais, au niveau six, 612 tente de nous l’inculquer. Tout le monde l’appelle le vieux, il nous apprend les mots, les pensées, la solidarité. Mais grouille-toi 647, tu vas mettre toute la production en retard. Ils vont nous couper la nourriture pendant trois jours. Sans compter que ta merde va attirer les insectes. Ceux qui rampent le long des jambes et dans les gamelles et ceux qui volent en faisant du bruit. Je ne les aime pas, ils ne cessent de vouloir me ronger les yeux. Tant pis pour 647. Je vais la dénoncer. Le niveau six va me passer à tabac mais les gardiens me donneront peut-être une double ration. Voire, un jour qui sait, me faire monter en grade.

Je prends une inspiration et j’appuie sur le bouton d’appel sous la table de travail. Cela fait vingt ans que le vieux est enchaîné au montage, sans espoir de promotion autre que l’éjection. Si ses idées sont belles, elles ne mènent pas très loin. Moi, j’ai de l’ambition. D’aussi loin que je me souvienne, j’ai toujours voulu grimper, m’en sortir. Je suis différent, tant pis pour le vieux.

À la pouponnière, j’ai dénoncé à un gardien, sans vraiment comprendre la portée de mon geste, le fait que notre instructeur de montage nous racontait des histoires et nous enseignait « l’extérieur ». Il a été éjecté. Cela m’a fait comprendre que je détenais un pouvoir. Les gardiens sont des dieux, intouchables, omniscients, omnipotents. Mais moi, enfant chétif, j’en avais détruit un sans presque y penser. Les autres enfants m’ont battu. On ne démontre pas impunément à un esclave qu’il n’est pas aussi impuissant qu’il aimerait l’être, qu’il est en partie responsable de son propre malheur. J’ai gardé mon pouvoir secret, enfoui au plus profond de mon esprit. Je sais que je ne dois m’en servir qu’avec parcimonie, attendre l’occasion parfaite.

Je regarde 647. Elle tient à deux mains son gros ventre gonflé et hurle. Du sang coule entre ses jambes. Elle est en train de pondre un petit ! Pas maintenant ! Elle va foutre en l’air le planning de production ! Je presse furieusement le bouton mais le garde n’arrive pas. Deux autres niveau six ont étendu 647. Le vieux est là et lui écarte les jambes. Une tête minuscule apparaît. Parfois, je me dis que moi aussi je suis sorti du ventre d’une femme comme 647. Le vieux dit qu’on l’appelle « la mère ». Ma mère est peut-être encore vivante. Peut-être est-ce 647. Non, elle est trop jeune. Et puis, de toutes façons, quelle importance ?

Deux gardes arrivent. Ils donnent des coups de matraque, par réflexe et par habitude. L’état de 647 leur arrache un grognement. Ils la saisissent par les jambes avant de la traîner jusqu’au couloir du médi-garde. Ses hurlements se mêlent au fracas des machines et aux habituels gémissements. Je crie à l’adresse du niveau six: « On reprend ! On va se faire punir ! »

Tous se tournent vers le vieux. Ses lèvres frémissent comme s’il allait dire quelque chose. Mais il baisse les yeux et empoigne machinalement une pièce sur le tapis roulant. C’est le signal. Comme un seul homme, le niveau six se remet au travail. Saisir, assembler, visser. Ils sont lents. Saisir, assembler, visser. Je vaux mieux que ça.

— 689 ! 689 ! Au rapport !

689 ? C’est moi ! Surprises dans leur hypnotique mouvement, mes mains restent un instant suspendues en l’air. Un garde s’approche de moi. C’est G12, un sadique.
— T’es sourd raclure de chiotte ? Au rapport !

Par réflexe, je me plie en deux sous la matraque mais je n’ai presque pas mal. Le coup a été léger, venant de G12, c’est presque une caresse. Je me lève et lâche mon travail. Tout le niveau six me fixe intensément. Cette nuit, ils vont me battre. G12 me crache au visage.
— Avance, sous-merde !

Je fixe intensément mes chaussures et le suis à travers le couloir. Des cris nous parviennent.
— Mon bébé, mon bébé, pitié !

C’est 647. Son corps est tâché de sang et de merde, son visage ruisselle de larmes, sa bouche se tord en un rictus de douleur. Pourtant, personne ne la frappe.
— Mon bébé, je vous en supplie. S’il-vous-plaît !

Un médi-garde, reconnaissable à sa blouse couleur blanc sanguinolent, s’adresse aux deux gardiens. D’une main, il tient une masse de chair rosâtre.
— Emmenez-la à son poste. Elle peut reprendre le travail.
647 est traînée en hurlant. Elle se tord en se jetant à genoux. Un garde lui envoie un violent coup de pied dans les seins.
— Salope ! Ça fait deux heures que tu as quitté le travail et tu continues à vouloir tirer au flanc !

J’aperçois alors F1, le chef des gardes. Je me souviens l’avoir vu deux fois s’adresser directement à un travailleur. Un frisson me parcourt l’échine, je prie pour ne pas être le troisième. C’est un dieu, une brute épaisse et puissante. D’une voix sourde, il lance au médi-garde :
— Alors ? Viable ?
— Il respire, répond ce dernier en examinant le petit corps poisseux qui s’agite dans ses bras. Il peut vivre.
— Assez pour être productif ? Nous avons des impératifs de rentabilité. Pas question d’élever un gringalet qui va nous claquer dans les doigts à la puberté.
— Je ne peux pas offrir de garanties. Il est limite.
— Alors jette, on a beaucoup de naissances pour le moment.

Sans un regard, le médi-garde jette l’informe amas dans le trou à excrément. F1 se tourne vers le garde qui m’escorte.
— Et lui, c’est quoi ?
— C’est 689. Le seul du niveau six qui sonne. Loyal et il tient le rythme. On a justement besoin d’un barreur au niveau six.
— Mmmm, on va le mettre à l’épreuve. G17, G19 ! Venez par ici. Cassez-moi cette racaille. Vous avez un quart d’heure de libre sur lui. On verra jusqu’où va sa loyauté.

Un sourire cruel éclaire leur faciès. Un quart d’heure ! Les distractions sont rares pour les gardes. L’un m’empoigne les cheveux et me jette à ses pieds. Sur un rythme lancinant, il tape mon front sur le sol humide et froid. Les coups résonnent dans ma tête comme un mécanisme lointain, une production rythmée par les éclairs de douleur. Les douleurs, je les connais si bien, compagnes indissociables de ma vie et de mon enfance. Elles me parlent, me bercent, me consolent. Il y a la violente, la brusque, la flamme qui coupe le souffle comme une botte dans les testicules. Il y a la hurlante, celle qui ravage et brûle comme une longue décharge électrique. Il y a la sourde, qui m’aide à me traîner sur un coin de sol humide pour mes quatre heures de nuit. Enfin, il y a la grondante, celle qui bloque ma gorge et gonfle mes paupières tout en m’accompagnant dans les lambeaux de sommeil.

G17 et G19 me traînent et m’enfoncent la tête dans le trou à excréments. Dans un éclair de douleur, entre deux larmes de sang, j’entraperçois la forme rose qui bouge et qui crie. L’odeur est effroyable, mon estomac se révulse. J’enfouis les cris du tas de chair sous mon vomi. Des mains hilares arrachent mon pantalon. Je sens l’extrémité d’une matraque qui fouille et cherche à s’enfoncer dans mon anus.

Un quart d’heure. Je dois tenir un quart d’heure. Je me concentre sur l’idée, sur la phrase que j’ai entendue : ils ont besoin d’un barreur. Je vais devenir barreur du niveau six. Plus qu’un simple travailleur, barreur. Le vieux va en baver. Je vais grimper les échelons. D’abord barreur et puis…

Photo par Davide Calabresi

=> http://500px.com/photo/21587505 Davide Calabresi
