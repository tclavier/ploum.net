# Pourquoi vous devriez viser Inbox 0
=> files/old/5900749129_9b03170cee_o.jpg

Si vous n’êtes pas un adepte de la méthode inbox 0, il y a fort à parier que votre boîte mail soit actuellement bien remplie et que le nombre de mails dans votre inbox soit un chiffre sans aucune signification réelle.

Passer à l’inbox 0 représente un effort non négligeable et il est donc très important d’être intiment convaincu que cet effort soit nécessaire et rentable.

L’inbox 0 est comme un sport : si vous la pratiquez régulièrement, elle vous semble couler de source, vous procure un sentiment de satisfaction et vous semble indispensable pour être productif. Mais si vous ne l’avez jamais pratiqué, cela vous semble une montagne, un effort surhumain et une perte de temps sans réel bénéfice.

Décharger le cerveau

Un mail reçu n’a que deux résultats possibles : soit il nécessite une action de votre part, soit il n’en nécessite pas. C’est aussi simple que cela. Après avoir pris connaissance de l’email, vous devez soit agir, soit pas.

Si vous n’êtes pas adepte de la méthode inbox 0, votre boîte mail est donc remplie des deux types de mails, sans distinction. Vous allez donc inconsciemment construire dans votre cerveau la liste des emails sur lesquels vous devez agir.

Votre cerveau devient donc responsable de maintenir cette liste, ce qui implique un stress permanent et surtout un manque de fiabilité total.

Une astuce utilisée par beaucoup consiste à utiliser le marquer lu/non-lu. Cependant, ce marqueur repassera à “lu” à chaque fois que vous consulterez le mail. Vous aurez donc le stress de ne pas oublier de le remettre à non-lu.

En pratique, votre inbox deviendra donc la liste des emails “sur lesquels je dois peut-être faire quelque chose mais je ne suis pas sûr”. Après tout, imagineriez-vous laisser tout votre courrier dans votre boîte aux lettres en refermant les enveloppes de ce qui est important ?

L’incitant de la liste vide

À partir d’un certains nombre d’éléments, le cerveau humain ne distingue plus les individus mais parle d’un groupe. Nous parlons à Jean et Marie. Mais si il y a trente personnes, nous parlons à un groupe et non plus aux individus.

Si votre inbox est un groupe, votre cerveau le percevra comme tel. Chaque nouveau mail ne changera rien au fait que votre inbox est un groupe de mails. Il s’ensuit que vous n’avez aucun incitant positif pour agir sur le mail. Le seul incitant qui vous pousse à agir est la peur de rater un email, la peur d’oublier.

Une fois la tâche accomplie et le mail archivé, vous n’éprouvez aucune réelle satisfaction. Votre inbox est passée de 102 à 101? Et alors ? Pire, l’incitant peut même être négatif : “À quoi bon ? De toutes façons, cela fait des mois que ces mails trainent”.

Pouvoir admirer une inbox vide, au contraire, offre une grande satisfaction et, en soi, est une motivation pour être productif et pour prendre des décisions.

Soyez actif !

Lorsqu’on n’utilise pas la méthode inbox 0, il y a toujours un email où l’on se dit qu’il serait bien d’agir. Peut-être. Oui, ce serait bien.

Cependant, comme on n’est pas très sûr, on marque le mail comme lu en se disant qu’on s’en souviendra. Le mail est finalement enterré par pure passivité.

La méthode inbox 0 empêche la passivité : pour chaque mail, il est nécessaire de prendre une décision : vais-je agir, oui ou non ? Il n’y a pas de “peut-être” qui est finalement l’état dans lequel se trouvent les mails qui sont actuellement dans votre mailbox.

Un des secrets du bonheur est justement de ne pas être passif mais, au contraire, d’être pro-actif et de prendre le contrôle. Si vous souhaitez être aux commandes de vos communications et, par extension, de votre productivité, de vos projets, la méthode inbox 0 s’impose.

Minimisez vos efforts

Il est tout à fait possible d’être heureux et productif sans inbox 0. C’est juste beaucoup plus stressant et cela demande plus d’efforts et de discipline.

Certains prétendent qu’ils reçoivent trop de mails pour la méthode 0. Mais cet argument est un aveu même de passivité et du fait qu’ils sont surpassés. Si vous recevez trop de mails, c’est le cas que vous soyez à l’inbox 0 ou non. La différence est que l’inbox 0 permet de vous rendre immédiatement compte des périodes où vous êtes dépassé afin de prendre les actions qui s’imposent.

La méthode inbox 0 est un changement de paradigme. Elle demande effort et discipline uniquement le temps de la transition. Après, elle coule de source, elle devient un réflexe d’hygiène quotidien. Mais pour réussir cette transition, il est important que vous soyez intiment convaincu de sa nécessité.

Dans un billet suivant, je détaillerai mes astuces pour parvenir à l’inbox 0.

Photo par Denis Dore. Relecture par Le Gauchiste.

=> https://www.flickr.com/photos/denisdore/5900749129 Denis Dore
=> http://gauchiste.fr Le Gauchiste
