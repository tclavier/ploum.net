# Rendre l’obsolescence obsolète ?

Le capitalisme et la société de consommation peuvent être la base d’une société idéale. J’en suis intiment persuadé. Le consommateur, par ses choix, dirigerait le marché dans la direction qu’il souhaite. Démocratique, logique et équitable. Inéluctable.

=> /go/06 
=> ../files/old/decharge.jpg Décharge

Mais pour que cette utopie se réalise, plusieurs aspects sont indispensables: le consommateur doit faire un choix volontaire, logique et parfaitement informé.

J’ai déjà traité de l’aspect non-volontaire des achats, les entreprises mettant en place des stratégies de dépendance et d’asservissement du client. Le choix logique me semble de plus en plus illusoire eu égard du matraquage publicitaire dont nous sommes l’objet.

=> /go/01 des stratégies de dépendance et d’asservissement du client

Quant à l’information parfaite sur les produits que nous achetons…

Savez-vous qu’une grande partie des sous que vous dépensez sert à payer des ingénieurs dont la mission est de diminuer la qualité des produits ? Une partie de votre argent sert donc à faire baisser votre qualité de vie !

Ces fonctionnalités sont appelées en anglais des « anti-features », des contre-fonctionnalités. On a beaucoup parlé, et moi le premier, des DRM, ces protections restreignant vos droits sur vos biens achetés. Mais le principe existe depuis bien longtemps comme me l’a appris cet édifiant documentaire sur l’obsolescence programmée (visible également ici).

=> /go/02 anti-features
=> /go/03 moi le premier
=> /go/04 édifiant documentaire sur l’obsolescence programmée
=> /go/12 visible également ici

Pour résumer, le réalisateur nous démontre comment l’industrie de l’ampoule a formé un cartel chargé de faire en sorte que la durée de vie d’une ampoule ne dépasse pas mille heures. Dans le but évident de vendre plus d’ampoules. Et comment cette idée géniale s’est répandue dans le reste de l’industrie, des bas nylon aux imprimantes car cela améliore les ventes et l’économie en général.

Cet argument de relancer l’économie par la consommation revient souvent. La massue ultime restant la création d’emplois: si on ne faisait pas comme ça, tous ces gens seraient sans emploi. L’écologie et l’automatisation se heurtent à la même barrière: le terrifiant spectre du chômage.

Doit-on accepter de dégrader notre vie afin de préserver l’emploi ? N’est-ce pas paradoxal ? Travailler plus, produire plus lorsque les gens achètent le moins ? Cherchez l’erreur…

=> /go/07 
=> ../files/old/poubelles.jpg Poubelles

Car après tout, tant que nous n’aurons pas une vie parfaite, il devrait y avoir de l’emploi. N’aimeriez-vous pas que votre linge, vos repas et votre nettoyage soient faits quand vous rentrez du travail ? N’aimeriez-vous pas déléguer une partie de votre travail à d’autres afin de voir un peu plus vos enfants ? D’ailleurs, cela vous permettrait de vous passer de four, de cuisinière et de machine à laver. Donc de ne pas devoir payer le réparateur.

Non, nous ne devons pas accepter une diminution de notre qualité de vie. Au contraire ! Travaillons à notre bonheur, favorisons l’expérience de vie plutôt que la possession. Achetons des services en lieu et place des produits. Le leasing et la location sont de plus en plus courants et parfois intéressants, surtout en considérant la vitesse à laquelle de nombreux produits deviennent obsolètes. Demandons aux politiques de favoriser la réduction du temps de travail, de faire payer la possession complète (ce qui devrait comprendre les frais de recyclage).

Lorsque, sur les rives de l’Achéron, nous fermerons une dernière fois les paupières, peu nous importeront les biens accumulés puis jetés, les chiffres virtuels de notre compte en banque. Dans nos ultimes instants, nos expériences et nos souvenirs sont toute la fortune que nous emporterons.

La pérennité de nos expériences, de nos données n’est-elle pas préférable à l’obsolescence du corps, du matériel ?

Photo de décharge sous CC By par D’Arcy Norman
Photo de poubelles sous CC By-NC-ND par iNyar‘. Relecture par Sylvestre.

=> http://sylvestre.org Sylvestre
