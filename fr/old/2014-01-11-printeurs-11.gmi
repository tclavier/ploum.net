# Printeurs 11
=> files/old/broken_glass1.jpg

Je n’ose faire le moindre geste, je retiens ma respiration. Eva me fait signe d’approcher et je la rejoins prudemment, à pas feutrés comme si j’avais peur de bousculer les atomes d’atmosphère. Cette atmosphère qui contient à peu près tous les éléments chimiques en quantité infime mais suffisante pour imprimer la plupart des matériaux courants. Je jette un œil dans l’aquarium du printeur et étouffe un cri de surprise. Le Roi Arthur !

Il semble endormi. Doucement, j’avance une main vers son pelage. Je le caresse. Il est chaud, doux au toucher. Mais il ne bouge pas. Je tente de le chatouiller, de le faire réagir, je lève une patte. Elle retombe inerte.
— Il est mort, murmuré-je.
— Ce corps n’a jamais été vivant, me répond Eva. Il n’est pas mort. Il s’agit tout simplement d’un assemblage d’atomes. Le printeur…
Elle s’arrête, soudainement terrifiée, comme si un diable venait de se dresser devant ses yeux exorbités. Je l’entends murmurer d’une voix à peine audible.
— Ils arrivent.
— Eva ?
Elle se reprend brusquement.
— Je disais que ce corps n’a jamais été vivant. Il lui a manqué ce petit quelque chose…
— Que veux-tu dire ?
La voix de George résonne dans mon dos.
— L’âme ! C’est bien à cela que tu penses Eva ?
Elle acquiesce en silence. L’âme ? Cela n’a aucun sens ! Georges nous tourne le dos et s’éloigne vers la cuisine. Je ne peux retenir une exclamation.
— Mais enfin Eva, qu’est-ce qui te prend ? L’âme ?
— Chut, pas si fort !
Elle agite les mains pour me faire signe de me taire. Je suis complètement déconcerté. J’ai l’impression que certains aspects de cette affaire m’échappent.
— Eva, ce que tu appelles âme n’est qu’une série de courants électriques entre des neurones.
— Nellio, je t’en prie. Restons-en là !
Je réfléchis à voix haute.
— Le printeur n’est en effet pas programmé pour analyser et reproduire un courant électrique mais…
Eva m’interrompt en m’attrapant le poignet. Elle fait apparaître un petit objet que je reconnais comme étant une carte mémoire. Cependant, les bords me semblent brillants, métalliques, comme aiguisés.
— Nellio, je sais qu’ils arrivent. Nous n’avons pas beaucoup de temps. Je te supplie de me faire confiance et de ne pas poser de questions.
— Mais… Ouaïe !
D’un coup sec du pouce, elle vient de m’enfoncer la carte mémoire sous la peau. Le bord tranchant a pénétré dans le dos de ma main comme dans du beurre et s’est parfaitement glissé sous l’épiderme. Je contemple avec surprise la petite plaie suintante de sang. Rien ne permet de deviner la greffe dont je viens de faire l’objet. Une cachette parfaite.
— Écoute-moi bien Nellio, ta vie vaut désormais plus que la mienne.
De ses deux mains, elle enserre brusquement mon visage.
— Quoi qu’il puisse arriver, quoi que tu puisses voir, sauve ta peau. Ta peau, c’est notre avenir, tu comprends ?
Je tente de l’écarter doucement.
— Écoute Eva, nous sommes chez Georges, tout va bien, ne t’inquiète pas comme ça.
Je la vois jeter un regard angoissé vers l’ascenseur. Une lumière clignote annonçant l’ouverture prochaine des portes. Au même moment, un bruit de verre brisé nous parvient de la cuisine.
— Georges ! murmuré-je.
Je me tourne vers la baie vitrée juste à temps pour voir une forme noire. Des dizaines de formes noires. Suspendues à des câbles. Des jambes. Des bottes. Des semelles qui transpercent le vitrage, qui le déchirent et le blessent. Le fracas est assourdissant. Les formes noires sortent également de l’ascenseur, coulantes, silencieuses. J’entraperçois certaines venir de la cuisine, souples et fluides. Je distingue les épaules rembourrées, les torses blindés, les armes tenues à bout de bras. Surréelle, venue de nulle part, une voix gronde et rugit, emplissant la pièce.
— Ne bougez plus, placez calmement les mains sur la tête.
Avant que le cercle des ombres ne se referme sur nous, Eva me tire par le bras et me projette dans un coin de la pièce où se trouve une fenêtre rectangulaire de la largeur d’une porte étroite. Elle lance un bras sous une tenture décorative.
— Ne bougez plus ! ordonne la voix. Peloton, en position de tir !
Dans un feulement, une forme blanche bondit soudain de sous le divan et saute vers le visage d’une des formes noires. Toutes les armes se tournent vers le Roi Arthur et sa malheureuse victime. Profitant de la confusion, Eva me tend une mince ceinture reliée à un filin. Un harnais d’évacuation ! Bien sûr ! Les riches en ont encore dans leurs tours ! Le filin doit faire exactement la taille de l’immeuble et être relié à un enrouleur à vitesse contrôlée. Je sais également que dès les premiers signes de traction sur le câble, la vitre se désagrégera grâce à de minuscules explosions dans l’épaisseur même du verre.
— Sauve ta peau Nellio ! Fais-moi confiance !
— Mais… Et toi ?
— L’âme est immortelle !
— Halte ! rugit la voix.
Je reste interdit. Sans remuer les lèvres, Eva me chuchotte :
— Au premier coup de feu, tu sautes sans hésiter. Ne t’inquiète pas, l’âme est immortelle.
Les mains tremblantes, je contemple le baudrier que je n’aurai pas le temps d’enfiler. Je ne comprends pas ce qu’elle veut dire. Bravement, Eva s’avance vers les hommes en armes. Policiers ? Soldats ? Milice privée ? La différence n’est de toute façon qu’une argutie juridique et les balles ont toutes le même effet sur un corps de chair et de sang.
— Attendez, supplie-t-elle, ne nous faites pas de mal ! Nous sommes innocents !
— Restez où vous êtes, gronde la voix que je devine provenir de drones équipés de haut-parleurs.
Ignorant l’injonction, Eva s’avance à travers l’appartement dévasté et les morceaux de verre. Elle est une fleur portée par la brise au milieu des silhouettes caparaçonnées de noir, étranges insectes casqués aux visages cachés par des masques menaçants. Les fusils se tournent vers elle, j’entends le cliquètement des doigts qui se posent sur les détentes. Lentement, Eva tourne la tête vers moi. Elle me fait un sourire. Calme. Résigné. Je lis dans son regard la confiance. Elle fait un pas de plus.
— Noooon ! hurlé-je.
— Feu ! ordonne la voix.
L’enfer jaillit soudain des canons. Je suis aveugle, je suis sourd. Je hurle, je veux me précipiter vers le corps en train d’être déchiré, déchiqueté. Je m’élance mais je suis soudain retenu par le câble du baudrier. La voix d’Eva résonne en moi.
— Saute sans hésiter, l’âme est immortelle !
Il ne s’est pas passé une seconde depuis le début de la fusillade. Dans une brusque volte-face, les doigts agrippés au harnais, je me jette dans la fenêtre. Aidé par les micro-explosions, le verre cède sans effort sous mon poids, le fracas se mêle à mon long hurlement et je me retrouve en train de tomber dans le vide.

Photo par Arsenie Coseac. Relecture par François Martin.

=> http://www.flickr.com/photos/99556296@N00/409026866 Arsenie Coseac
=> /no-proprietary-service.html François Martin
