# How to manage a mailing list?

Asking for help to send by blog posts by email.

I’m currently in the process of moving my www blog out of Worpdress and merging it with this Gemini capsule. I’m quite excited by the progress I made in this project and will soon talk more about it. But there’s one bottleneck I’m currently facing : migrating users subscribed to my mailing list.

Currently, I’m using Mailpoet, a solution integrated into Wordpress. I only started to send my blog post by email 2 years ago, thinking that nobody would use that, but I’ve more than a thousand subscribers so I want to keep this feature. After all, mail is the only truly decentralised protocol and I love it. I especially like the fact that people often reply to my blog post to send me their feedback. 

Which means I need to find an alternative. As my new blog will be hosted on sourcehut, the logical step would be to use sourcehut mailing-list system. But sourcehut has some severe limitations and I’m looking for feedback from users or for alternatives.

My greatest fear is being too complex for my non-technical audience. I’m quite proud of writing about technology and being read by people really unfamiliar with the topic. By definition, those people are not using RSS, rarely using Mastodon and a newsletter is the easiest.

So let’s explore Sourcehut’s limitation

## No migration, no statistics

If I move to sourcehut, I would need to ask people to resubscribe. I cannot import them. I cannot verify if it works for them. I cannot see if they are subscribed or not.

Guess what? It’s actually a feature I like a lot. I don’t want to manage subscribers. I don’t want to see the number of people unsubscribing after a given post. 

Also, I believe that when we subscribe to something, we should be asked every year if we would like to keep being subscribed and, by default, to be unsubscribed. If I had any power on the GDPR, I would have done it that way: no permanent consent, only consent for a limited timeframe with a maximum of one year. That way, we keep subscriptions in touch with our interests and we are automatically dropped from every commercial database.

So, yeah, I have no problem with asking people to resubcribe. It’s a good thing. In fact, I’m quite excited by "not knowing" who is subscribed. Like RSS.

## Hard to subscribe

The only way to subscribe is to send an email to one of those addresses :

=> mailto:~lioploum/en+subscribe@lists.sr.ht ~lioploum/en+subscribe@lists.sr.ht
=> mailto:u.lioploum.en+subscribe@lists.sr.ht u.lioploum.en+subscribe@lists.sr.ht

(this is the mailing list for English posts. Replace "en" by "fr" for the French ones if you are interested to test with me)

My biggest question is to know if sending such an email is complex or not. Is it a barrier that would prevent someone from subscribing? I’d like to hear from your experiences.

## No translation

I know that lots of people reading my French blog are quite afraid of anything in English. Sourcehut is only in English and there’s no clear way of unsubscribing.

Maybe I could add a French explanation to every email with instructions on how to unsubscribe. I don’t know if it’s a problem or not.

## How the mail will look on smartphones

I’m a text-only fan. I write and read my emails in neomutt, with a fixed 72-character limit per line. But those emails look ugly on smartphones (because the smartphone is often displaying at 60 chars per line, splitting every line as 60+12).

This made me realise that, perhaps, I should send an html version of the email together with the text version. Which is what most mailing-list software are doing. Support for html emails in sourcehut is quite new (I don’t know if it’s already in production). I also have no idea on how to produce my own html email version but I guess I could find out.

My only point is: how to make my text easily readable by email when read on a smartphone and will sourcehut allows me to do that?


## Alternatives to sourcehut

Being a happy paying user of sourcehut, trying to host my blog on sourcehut, sourcehut lists are, of course, my first choice. But maybe you have recommendations for alternatives.

I need a solution which is based on free software, which is ethical about storing user’s email and to which I could send emails without going through a web interface.

## Contact me

So please, send me your feedback or your story by mail.

If you are curious about my new blog, the whole thing is hosted on ploum.be, both web and Gemini (but the content is the same as here for now). I will talk more about it in the near future.

=> gemini://ploum.net ploum.net (Gemini)
=> https://ploum.be ploum.be (https)
=> https://sr.ht/~lioploum/ploum.net/ The project on sourcehut
