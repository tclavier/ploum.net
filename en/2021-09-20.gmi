# Back to paper (and typewriters)

2021 is probably my most productive year in term of words written. But not on my blog nor on this gemlog. The reason is quite simple : I fell in love with typewriters. I’m now writing mostly on paper, choosing the best paper I can find, anotating, reading and re-reading what I did.

I love typewriters and highly recommend any smolnet user to read "The Typewriter Revolution" by Richard Polt which encapsulates perfectly all the good (and bad) reasons to use a typewriter in this morden era.

Yes, I miss my Bépo keyboard. I’m back on Azerty (on my typewriters, not on my computer or my Freewrite). There’s lot to say. Too much for this small gemlog.

But I’m dreaming of building a scanner that would do some OCR and automatically publish what I feed to a gemlog. This would be without any screen interaction at all (which would means quite a few typos).

I’ve investigated a bit but I don’t know where to start. My OCR tests are quite bad with my typewriters (mixing a and e for example).

Don’t hesitate to contact lionel at ploum.net if you have any lead.
