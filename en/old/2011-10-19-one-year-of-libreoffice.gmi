# One year of LibreOffice

Not so long ago, OpenOffice.org was the less attractive project of the Linux ecosystem. You would need it, you would use it daily but you would not think it was possible to contribute to that project or to improve it in any way.

It was a necessary pile of spaghetti code from the eighties that only Michael Meeks was able to understand. He was even spending every FOSDEM trying to convince you that compiling OpenOffice was not so bad, that it took only a couple of weeks and a few terabytes of hard disk.

Then, in only one year, multiple things happened:

1. OpenOffice.org was forked into LibreOffice
2. Lanedo, my employer, started to offer services around LibreOffice.
3. The first LibreOffice Conference took place in Paris.

=> ../files/old/lo_paris.png LO conference in Paris

=> http://www.libreoffice.org/ LibreOffice
=> http://www.lanedo.com Lanedo
=> http://www.lanedo.com/libreoffice.html services around LibreOffice
=> http://conference.libreoffice.org/ LibreOffice Conference

The achievements of this year are amazing.

1. LibreOffice managed to build a strong and really friendly community. The conference was packed by awesome people and I immediately felt welcomed.
2. Michael Meeks did a demo of LibreOffice OnLine (Lool), a version of LibreOffice displayed in your browser thanks to GTK+3 and its Broadway backend.
3. The Région Île-de-France announced that they will ship hundred of thousands of USB keys with LibreOffice to their students. They will be shipped with an extension that allows direct access to their own cloud server. That extension was developed by Lanedo and I’m proud to be part of this project.
4. Other personal achievement: I gave a talk about Usability in LibreOffice which resulted in an afternoon-long meeting with insanely great usability guys. I’m really delighted to see that usability will soon become a major concern of LibreOffice.

=> ../files/old/officespace_users.jpg Improving LO usability
UX team improving usability of LibreOffice

=> http://people.gnome.org/~michael/data/2011-10-10-lool-demo.webm LibreOffice OnLine
=> http://extensions-test.libreoffice.org/extension-center/webdav-integration an extension that allows direct access to their own cloud server
=> http://www.lanedo.com/~lionel/201110_usability_libreoffice.pdf Usability in LibreOffice

But let’s not stop there. There are many more stuffs for the future:

1. Work will happen on an Iphone and Android port of LibreOffice.
2. Compiling LibreOffice under Linux is easier those days (and I will gladly help anyone who want to give it a try). But Windows is another story, even though most of LibreOffice users are using it. Let’s face it: building, testing and debugging LO on Windows is time-consuming and slow down the whole project. As a consequence, The Document Foundation and Lanedo decided to join their effort to improve this situation. I will cover our progress on this blog. Not that I’m particularly impatient to get my hand filthy with Windows mud but, hey, it has to be done!

=> ../files/old/officespace_tps.jpg TPS report
If there were a TPS report, it would be great!

=> http://www.documentfoundation.org/ The Document Foundation

The conclusion of all of this is that contributing to LibreOffice is not as hard as you would think. And that I’m already impatient to attend next year’s conference.
