# I don’t care about your business model
=> files/old/oldcd.jpg

Une traduction en français est disponible.

=> /je-nai-faire-de-votre-business-model traduction en français

The world is changing and that’s a good thing. A little bit every day, gradually. Sometimes for the worst, often for the best. One consequence is that some businesses are no longer profitable. Or do not answer adequately to the market. They must adapt or fail. This is normal, it is called the evolution of society.

There are countless examples. The candle industry was marginalized by electricity. In my country, thousands of coal miners became unemployed because of the petroleum and nuclear energy. Ice sellers disappeared completely when refrigerators became widespread.

=> http://torrentfreak.com/nobody-asked-for-a-refrigerator-fee-110821/ Ice sellers disappeared completely

But let’s take some modern cases. What happened to those women who responded to overtaxed erotic phone lines which had ads in the local newspaper during my childhood? And that date-by-phone service for which I still have the radio advertising in mind, twenty years later? How did the printing of maps deal with the GPS?

Speaking about GPS. While it was initially an option for luxury cars, costing between 2000 € and 5000 € only a few years ago, portable GPS such as Tomtom and Garmin changed the game. A small compact GPS, more efficient at tenth of the price! Less than 5 years ago, it was a revolution.

But would you buy Garmin or Tomtom shares now that any 150€ phone is also a GPS? With updated maps, real-time traffic information and integration of your online address book?

We look at this evolution from a distance. It even makes sense until the day your own job is on the hot seat. After the denial, « Consumers will not follow, it’s only about a minority », you will end on the typical argument: « Every work deserves a salary, we have to make a living ».

Is making a living of your current job really an immutable natural law? You earned money as long as you provided something for which customers were willing to pay. However, this does not give you the moral right to do the same forever. If nobody wants to pay for your work or if clients have the same result without paying you, it is up to you to reinvent yourself. Or switch off the lights and close the door.

You will then probably look at the public institutions, imagining how you and your industry could live out of public money. If your service is essential, like education, culture or health care, the state must actually take it in charge without any need for profitability. But if it is not, it will only exist as long as a large enough customers base is willing to pay. Should the state subsidize candles, maps or deliquescent GPS?

At this point of the discussion, you will certainly challenge me to find another way to make money with your work, to create a new business model for you. As if not finding one was the ultimate proof that nothing should change.

You know what? I do not even want to think about it. It is your job, not mine. If I have an idea of a ​​business model, I would create a company, I would be an entrepreneur. And if I can not find one, we can not draw any conclusion anyway. Maybe is there no more possible business model in your case? Like ice sellers? I wish you the opposite.

As a counter argument, your will focus on the costs of your business and detail the number of hours spent on your work. As if it were a justification of value. But nothing forces you to do this particular work. Nobody is requiring you to continue. You think you are paid for your work. In reality we work to get paid. Do not confuse cause and effect.

=> /backyard-digging-point justification of value

In last resort comes the blackmail argument: if you are not paid, your work will disappear. You will go as far as calling the evolution a threat for your entire industry. Well, let’s try. If your work is essential, a threat should open wallets. But do not count on it: the cemeteries are filled with irreplaceable people.

In the end, you will be shut me up by simply telling that I’m wrong. But whatever your work is, it will soon be obsolete. In a year or ten. The world is changing. Some people do not realize it because they adapt continuously. Other reinvent themselves periodically. Some put all their power to prevent the evolution instead of questioning themselves. Trying to erect their own venality as an universal morality.

I think we all agree that people in this last category do not deserve our money.

Picture by MKFautoyère

=> http://www.flickr.com/photos/mkfautoyere/7243334326/in/photostream/ MKFautoyère

Une traduction en français est disponible.

=> /je-nai-faire-de-votre-business-model traduction en français
