# Why I don’t contribute to Wikipedia anymore

I cannot live anymore without Wikipedia. Each time I’m discussing a random subject with somebody and that a question pops into my head, I think « Let’s look on Wikipédia » before realizing that I’m in the Big Room with the Big Blue Roof, not in front of my computer.

Nevertheless, I don’t have a Wikipedia account. I don’t see the need for it. Sure, it could be useful for others, but I don’t think I’m really good at writing encyclopedia stuffs, I don’t find it funny at all and I believe that I’m already doing some useful stuffs. I don’t want yet another account, I don’t want a password nor statistics.

=> http://gtg.fritalk.com useful stuffs

Of course, I was correcting mistakes when reading them, I was sometimes adding one or two sentences because my knowledge of the page’s subject was enough. I even created a few « stubs » when I thought it was missing. Some of them became real well-formatted Wikipedia pages afterward.

It was back in those days…

I don’t do that anymore.

=> ../files/old/wikimedia.jpg Wikimedia

For the last year, every change I did on Wikipedia was a catastrophic failure. As I was not logged in, they were most of the time seen as non relevant. Most of my changes I manually followed where flagged with the infamous « Reference or Citation Needed« , even those that could be confirmed by any history book.

=> http://xkcd.com/285/ Citation Needed

Sometimes, you know something so well that you can’t even tell a source for it. Ask any Belgian the name of the first Belgian king. And then a reliable source to prove their answer.

Anyway, if I replied with a website source, it was rejected because no blogs or forums are allowed as a reliable source. Think about it. A blog is simply a website with chronological content. Most of modern website with information and content are blogs. ArsTechnica and Slashdot are considered as blogs. The fact that a date is put on the content should not interfere with the quality of the said content. In theory.

Wait, it’s worst!

When you edit a page, even for a one-letter typo change, the page immediately falls in the whiter-than-white-wikipedia-ayatollah-commando-squadron radar. And, oh surprise, they discovered that the page hosted for years a content that was not perfectly following the Wikipedia religion. And, as every good zealot should do, they immediately corrected the error, regardless of the fact that the content was fine and useful for years.

On one article I was reading about some sport, I corrected a mistake in an URL in the external link section. One day later, the whole section was removed with all the links because every pointed website was considered as « a blog ». Those links were really relevant to the subject and very useful for me. But they were « blog ». Now, I consult the history of that page to have the links.

Another article[1] I recently corrected for a spelling mistake was deleted as « non-notable enough ». The fact that I was looking for that article, that I found it, that it was existing for weeks or months and that I corrected a mistake should be a proof that it was at least an useful article.

But Wikipedia doesn’t want to be useful anymore.

It’s a common story in the human species. First, we want to achieve a goal. Second, we discover that we are all different[2] and that we need some rules to organize our work. Third, we make the rules really complicated to fit every corner case. Fourth, we completely forget the goal of those rules and we apply them blindly for the sake of it. Fifth, we punish or kill those who don’t follow the rules as strictly as we do.

=> /197-le-conte-du-mousse-et-des-vingt-neuf-navires forget the goal of those rules
=> /215-heureusement-ils-sont-fondamentalement-betes-et-je-suis-leur-chef we punish or kill

Every religion started that way, most companies internal rules have the same origin. Sadly, Wikipedia is no different. For some time, I hoped but… no. Sad. Let’s pay extra attention that my projects and my own life don’t follow the same scenario.

#### Notes

[1] I deliberately keep my examples vague to keep a general tone in the debate. I don’t want to discuss every case and have a justification for the deletion-modification I faced. The reflection is general as, according to my experience, it happened on both EN and FR wikipedias

[2] I’m not!
