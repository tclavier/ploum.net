# Getting GTG 0.2 released – Done !

Short version : GTG! 0.2 is out

=> http://gtg.fritalk.com/post/2009/12/10/The-new-Getting-Things-GNOME!-0.2-Gorignak-has-landed! GTG! 0.2 is out

Last year, I met several times with my friend Bertrand to discuss how we imagine a good GTD tasks manager. On October 17 2008, Bertrand told me that he had a funny idea for a name and I immediately created the project and commited my first try with a GTK TextView widget.

2 months later (exactly one year ago), what we had was a kind of buggy monstrous experiment.

=> ../files/old/galaxy_quest_0.jpg Monster

Now, the Getting Things GNOME! team is very proud to announce the release of GTG! 0.2 – Gorignak, a wonderful and rock solid release that, we hope, will pave the road for a future inclusion in GNOME 3.X.

=> http://gtg.fritalk.com Getting Things GNOME!
=> http://gtg.fritalk.com/post/2009/12/10/The-new-Getting-Things-GNOME!-0.2-Gorignak-has-landed! GTG! 0.2 – Gorignak

=> ../files/old/gorignak.jpg Gorignak

We have many ideas but we appreciate your feedback, on the application itself but also maybe on how we handle the development, the community, stuffs like that.

=> https://bugs.edge.launchpad.net/gtg many ideas

Now, it’s time to celebrate that 0.2 release and to spread GTG love everywhere (we are even on FR wikipedia). See you at FOSDEM 2010 or at GUADEC 2010?

=> https://www.ohloh.net/p/gtg everywhere
=> http://fr.wikipedia.org/wiki/Getting_Things_Gnome FR wikipedia

The GTG team :

=> ../files/old/galaxyquest_equipage2.jpg The GTG! Team
From left to right :

Kevin « Guy » Mehall, « I’m the guy in the episode who dies to prove how serious the situation is.«
Doctor Bertrand Lazarus, « Just like mother used to make.«
Commander Ploum, « As long as there is injustice, whenever a Targathian baby cries out, wherever a distress signal sounds among the stars, we’ll be there. This fine ship, this fine crew.«
Young Paulo Laredo, « How you’ve grown!«
Luca Gwenn Falavigna, « *Look! I have one job on this lousy ship, it’s *stupid*, but I’m gonna do it! Okay?«
Luca Fred Invernizzi, « Hey guys, I just wanted you to know that, the reactors won’t take it; the ship is breaking apart and all that… Just FYI.*«

=> http://blog.kevinmehall.net/ Kevin « Guy » Mehall
=> http://rousseau.frispeech.com/ Doctor Bertrand Lazarus
=> http://www.paulocabido.com/ Young Paulo Laredo
=> http://behindmotu.wordpress.com/2007/11/27/luca-falavigna/ Luca Gwenn Falavigna
=> http://allievi.sssup.it/user/guillo/ Luca Fred Invernizzi
