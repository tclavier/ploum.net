# The Blogger from Tomorrow (Part II)
=> files/old/cocktail.jpg

Version en français. Read the first part of the interview.

=> /le-blogueur-venu-de-demain-seconde-partie/ Version en français
=> /the-blogger-from-tomorrow-part-i/ the first part of the interview

I’m still facing Max, the famous blogger who is trying to gently lead the conversation towards politics. Innocently, I ask him:


Why the European elections?

Because that year was a clear breakthrough for the Pirate Party.

For years, the Pirate Party had been facing a paradox: it was defending freedom, citizenship, decentralization but needed a president or a central agency that could determine how to spend the money or how to punish a member with a wrong behaviour. What was the rules for a small group to become an official Pirate Party? Depending on what definition? Should we send them money for their project or were they scammers?

The pirates found an awesome solution: they took the crowdfunding concept and scaled it from the project level to the whole party.

Can you give an example?

Yes, on the platform of the Pirate Party, anyone, I mean anyone can propose a project related to the Pirate Party: printing posters, organizing a happening, creating a website dedicated to a specific issue, organizing a general assembly.

For each project, the budget is fixed by organizers and, like any crowdfunding platform, anyone can support the project of their choice. If a person claiming to be a pirate has a bad reputation, the community stop to support his projects.

This is however not very different from Kickstarter or KissKissBankBank.

There is a difference: outside projects, there are also « organizations », typically the Pirate Party of France, the Pirate Party of Belgium, etc..

If you want, you can support the organization rather than a specific project. I love pirates, I want to send them money but I wish they use it at their own discretion. The Pirate Party doesn’t have a bank account any more. All the money from public grants is immediately sent to the crowdfunding platform.

There’s still the need of deciding how to use that money.

That’s the genius part: everything is automatic. When you give money to a project, you can decide how important this project is according to you. The highest priority projects are then financed automatically by the organization.

Let’s say that I make a project to print posters for my city and I need 10 bitcoins. I already received 2 bitcoins but, as the project is voted as a top priority one by 20 donors, the central organization automatically complete the 8 missing bitcoins.

Once the posters are printed and the project is completed, I put the final bill on the platform, which is of 12 btc because I forgot to take the glue into account. I receive the 12 btc on my account immediately. At the same time, the posters are so successful that half the city decides to give to the project or even to Flattr the initiative. The project is closed but still accept money which is sent back to the central fund.

All of this is completely transparent. Invoices are public and searchable. Some graphics are automatically generated. There’s no longer a central authority that determines who pays what.

So there are print workers who accept bitcoins?

This is only an example. Bitcoin is my main currency, I tend to think in bitcoins. I know this is far from the norm.

I’ve also installed the extension BitSpend in my Firefox: it allows me to pay with bitcoins on the vast majority of e-commerce sites even if they do not accept Bitcoin yet.

An obvious problem is that it is not possible to save, for example for the next election. All the money of the organization is immediately spent on short-term projects.

The solution is immediate: creating a project « Saving for the next elections » with a specific goal. This encourages people to contribute to this fund. And if nobody sees this project as a priority, so be it. This is also democracy.

And what about the member fee?

There’s no need for it any more. Each local team selects candidates for each election. Some have wacky candidates. But others succeed and share their experience. It’s not a real party any more. We moved from a pyramidal structure to a network of local factions. It is no longer possible to say « I am a member of the Pirate Party. » A pirate publish some racist statements? He just lost his credit and will struggle to become a candidate or to obtain the trust of others for his projects. We are in a do-ocracy, not in the discussion.

Fun Fact: At the time, the Pirate Party was widely criticized for his name. They were always told to remove the term « Pirate », which had a negative connotation.

They agreed to change their name but it is the word « party » they removed. I’ve even heard « In your daily life, do you suffer mostly because of piracy or particracy? « .

Everybody now says « Pirates » and not « Pirate Party ». Which is beautiful to those predicting the decay of the party concept.

But what about your own career? Did you ever get involved in politics?

Not really. But, like many blogger, I closely followed the rise of pirates. I have never been a member or a candidate as I wish to remain independent. But I do not hide that I have voted for them and contribute to some projects.

They were mainly a source of inspiration. If they can do it for politics, why not for me, for my own life?

How did you apply the concept to your life?

I started using a similar crowdfunding platform. I created a project for my monthly rent, a project for a trip to the Silicon Valley, a project for hosting my blog and then a « miscellaneous expenses » project.

It should be noted that, unlike the Pirate Party, I’m not an « open » organization. In a « closed » organization, only members of the organization have the ability to give priority to a project. As I am the only member, I keep control over my life.

A simple example is my rent. I have a recurring project to pay the rent. This is a priority project. The recipient is my landlord’s bank. I have nothing to do. If a reader wants to contribute to my rent for the month, he can. But this is rare. In general, my rent is completely filled by money from the organization, namely me.

When I was in the Google I / O conference in San Francisco, I had a given budget. My readers were interested because I was doing some live broadcasting and analysis. They contributed a lot. My test of a fully automatic car was a buzz and received many Flattrs. As Flattr is integrated to the crowdfunding platform, these specific flattrs were immediately added to the project budget.

In the end, after putting every bill in the platform, including the smallest restaurants, I realized that I had been saving. The project has made a profit that was added to the capital of my organization.

Is everything transparent?

Yes, my life is very transparent. Note, however, that I do consulting for some companies. I create these projects as any other on the crowdfunding platform but they are invisible to the public.

This allowed two of my customers to jointly pay one of my project while keeping the invoicing and accounting straightforward.

Those projects are fully beneficiaries. Note that, as a result, the global budget for my « organization » is private. Like my « saving for a pension » project. Being transparent does not mean not having any privacy.

The crowdfunding platform serves me as a budgeting, billing and accounting tool.

Do you still have a bank account?

Yes because there still are shops where you must pay with a bank card rather than scanning a QR code with your glasses. And also for taxes purpose. But there is only a very small amount of money that goes through my account. It is an accessory. A bit like a CD drive on an old computer: it’s there but we do not use it anymore. My savings is fully in bitcoins on different services. I also have a little money on my Google Wallet.

Besides, I pay the taxes only regarding the money in my traditional bank account. Everything else is in an absolute legal limbo. I’ve exhausted three accountants before giving up. I’m not hiding, I’m not trying to cheat but it’s not my job to solve the problems of the administration and the flaws of the law. Ater all, if I’m ever sued, it would make great blog posts.

But it is a good idea to completely remove the bank account. It would make a series of interesting posts for my blog. I already buy VISA prepaid cards with bitcoins. Can I withdraw cash from an ATM with them?

But your rent is paid on a bank account that is not yours. It means you are not paying any taxes on that money, right? But your landlord is!

Oh, that’s right. I’ll try to convince my landlord to accept bitcoins.

In summary, are you living out of donations?

It was the case as long as crowdfunding were donations only. However, the platform I use also allow to manage contracts, which I use for my consultancy work. They also introduced a new feature: the purchase of shares. A creative project may sell shares of itself. If the project is profitable, the investor will automatically receive a portion of the profit. It is also possible to purchase shares of an umbrella organization setting up several projects.

All of this is done automatically without requiring any accounting. If you hold shares on any traditional exchange, my advice is to sell everything. I’m not sure they will last long. Besides, do you really think that big good old companies will be able to fight against the simplicity of an « organization » on a crowdfunding platform?

But isn’t your crowdfunding platform centralized?

No, it is built on Ethereum which is a Bitcoin derivative. Everything is completely decentralized. There’s no way to shut down the platform.

Thank you Max, a word for the end?

When a new technology appears, we tend to overestimate the short-term impact. The smartglasses the bitcoins, crowdfunding. Everyone raved and made outlandish predictions. And two or three years later, we realized that the world has, in fact, not really changed.

By cons, we have a huge tendency to underestimate the long-term impact of these technologies. Do you remember that 15 years ago, nobody had smartphone and 3G did not exist? All of this is producing a deep change. A change that I foresee without understanding it. A change which is not only underestimated but completely ignored by the powerful, the rich and politicians.

Perhaps it is their interest to not understand this change. Whatever, I’m not waiting for them.

I call the waiter. He brings me a ticket with a QR code. While looking at it, I whisper « accept payment copy expense. » Max gets up and gives me a happy « Thanks for the drink, see you next time! « Before walking away and folding his glasses on his nose.

A little stunned, I stay speechless. I was meeting a blogger to discuss audience and web publishing. I ended reconsidering the whole society. What I thought was science fiction is already the past for him. He no longer lives in the same country as me, the same society. Proof: he does not use the same currency, he does not work and does not pay taxes here. Max is not from here, he’s from Tomorrow.

I sip the last drop of my Mojito. I think I’ve found a catchy title for this paper!

Picture by Elisa Pictures. Disponible en français.

=> https://www.flickr.com/photos/32080494@N02/7624324004 Elisa Pictures
=> /le-blogueur-venu-de-demain-seconde-partie/ en français
