# Very simple video editing/DVD burning in Ubuntu

You have a IEEE1394 (Firewire) camera and you want to burn your birthday/wedding/funerals on a DVD. And maybe cut the scene where you look silly, dancing madly with a potato in your pants. It’s Ok, Ubuntu can save your reputation…

Vous avez une caméra IEEE1394 (Firewire) and vous voulez graver un DVD vidéo de votre anniversaire/mariage/enterrement. Et peut-être en profiter pour couper la scène où vous dansez comme un fou avec une patate dans votre pantalon. Ubuntu peut sauver votre réputation…

First, we will workaround Bug #196743 by adding you to the « disk » group. Open a terminal and type de following command ($USER must be replaced by your login):

=> https://bugs.edge.launchpad.net/ubuntu/+source/pmount/+bug/196743 Bug #196743

Tout d’abord, on va résoudre le Bug #196743 en vous ajoutant au groupe « disk ». Ouvrez un terminal et tapez la commande suivante (remplacez $USER par votre login):


sudo adduser $USER disk

Now, logout and login again on your computer. I know, it’s tricky, it’s a bug, it will be solved someday.

Quittez votre session et reloguez-vous. Voilà, le bug est contourné.

Now, we will install the required packages. You can install the following packages by selecting them in Synaptic. Or you can open a terminal and type « sudo apt-get install » followed by the list of the packages. That’s exactly the same.

Maintenant, on va installer les paquets nécessaires. Vous pouvez simplement les sélectionner dans Synaptic. Où vous pouvez ouvrir un terminal et taper « sudo apt-get install » suivi de la liste des paquets. C’est exactement pareil.

dvdauthor dvd+rw-tools mjpegtools kino

Now, in the « Sound & Video » menu, you have one new tool called « Kino ». Plug your firewire cable, start your camera and launch Kino. Go straight to the acquisition tab. Do your work with kino. Edit scenes. Do not hesitate to read the help from the help menu, it’s really informative. Or read the Kino documentation.

=> http://www.kinodv.org/docbook/ Kino documentation

L’application Kino est apparue dans le menu « Son & Video ». Branchez votre cable Firewire, allumez votre caméra et lancez Kino. Vous pouvez directement aller à l’acquisition. Ensuite, à vous de faire votre film. N’hésitez pas à lire l’aide inclue avec l’application. Vous trouverez également de l’aide en ligne.

=> http://www.tux-planet.fr/blog/?2008/02/11/223-montage-video-sous-linux-avec-kino également
=> http://www.funix.org/fr/linux/kino de l’aide
=> http://doc.ubuntu-fr.org/kino?s=dvd en ligne

=> ../files/old/kino2.png Burn DVD with Kino

Happy with the result ? It’s time to burn the DVD. Insert the blank DVD in your burner and ignore the proposition of burning a DVD that will popup. In Kino, go to the « Export » tab -> « MPEG » tab. Choose the format « 8 – DVD » and, for the « dvdauthor XML output », choose « Burn to /dev/dvd with growisofs ». Press the « Export » button. Take some coffee. Wait. Sleep. Voilà, it’s done !

Content de votre vidéo ? On va la graver sur un DVD. Insérer le DVD vierge dans le graveur et ignorez la proposition de gravure qui apparaît automatiquement. Dans Kino, allez à l’onglet « Exporter », ongler « MPEG ». Choisissez le format de fichier « 8 – DVD » et, pour « Sortie XML dvdauthor », choissez « Burn to /dev/dvd with growisofs ». Appuyez sur le boutton Exporter, attendez un peu, prenez un café. Voilà, votre DVD est prêt !


Your DVD should play in any DVD reader but, of course, it’s very simple. No fancy menu, no complex sequences, no special effects. If you want to burn complex DVD, you can install « qdvdauthor ». Qdvdauthor can work with Kino, it will be added to the « dvdauthor XML output ». Another software is Tovid. Tovit will be in Ubuntu Hardy, the version expected in April. Fortunatly, you can install it on Ubuntu 7.10. Download the install file and click on it.

=> http://tovid.wikia.com/wiki/Main_Page Tovid
=> http://tovid.sourceforge.net/download/ubuntu/tovid_0.31_all.deb the install file

Votre DVD devrait être lisible par n’importe quel lecteur DVD mais, bien sûr, il reste très simple. Pas de menu, pas de séquences complexes ou d’effets spéciaux. Si vous voulez réaliser tout ça, vous pouvez installer le paquet « qdvdauthor ». Qdvdauthor s’interface avec Kino et sera ajouté au menu « Sortie XML dvdauthor ». Un autre logiciel prometteur est Tovid. Tovid sera dans les dépôts de la prochaine version de Ubuntu, prévue pour avril. Mais vous pouvez le télécharger et l’installer sur votre Ubuntu 7.10.

=> http://tovid.wikia.com/wiki/Main_Page Tovid
=> http://tovid.sourceforge.net/download/ubuntu/tovid_0.31_all.deb télécharger

This mini howto is under the WTFPL. Don’t ask to copy/modify it, just do it.

=> http://sam.zoy.org/wtfpl/ WTFPL
