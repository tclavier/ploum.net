# The Morality of Filesharing and the Thought Police
=> files/old/thought_police.jpg

A lot of people like to see morality as a clear line dividing the world between good and bad, white and black. As Randall Munroe would say: » My hobby: making a thought experiment to test where exactly they put this line » (XKCD point, checked).

=> http://en.wikipedia.org/wiki/Randall_munroe Randall Munroe
=> /xkcds-law XKCD point

When I told you why I was a pirate, I received a lot of reactions telling me that I was only trying to give a justification to something inherently bad. When I wrote an open letter to artists and suggested a new way of being paid, I also had those kind of reactions and I was told that I had to respect the artist’s choice. If the artist doesn’t want to be found on The Pirate Bay, it was inherently bad to download his content there.

=> /im-a-pirate why I was a pirate
=> /open-letter-pirated-artists an open letter to artists
=> /paying-web a new way of being paid

Inherently bad? But to what extend?

Let say that I legally bought a CD from an artist which is against the evil pirates of the internet. You know what I mean.

Is it bad to rip the CD to my hard disk and make MP3 files? Probably not.

I regularly backup my data on an external USB disk. The MP3 are then also on that disk. Is it bad? I would say no.

Being tired of the music, I sell the CD in a second hand store. Or I give it for free to a friend. Is that bad? Some would say that I have to delete the MP3s from my computer. Being honest, I do it. I delete the files.

One week later, my hard drive crashes. Hopefully, I have a week old backup and I restore from there. At this point, the MP3s are restored too. But, being busy recovering important files, I completely forgot those MP3s.

Am I a pirate because I have illegal MP3s on my computer? Even if I forgot about it? Is that morally bad? If yes, then piracy is the mere possession of a given information on a computer. In that case, listening to a streaming website should be OK, isn’t it? I can also make all my friends become pirates just by sending them the files. As long as they don’t delete the email and empty the trash, they are pirates! So, I would guess that only having the file is not inherently bad.

What if I didn’t originally bought the CD but received it for free from a friend who was tired of it? And who had a backup too?

When I listen to music, it’s usually random. There’s then a good chance that I will listen to those MP3. Do I become a pirate when I listen to the music, even if I forgot that I sold the CD weeks ago? If yes, then I’m also a pirate when my neighbour listen to loud music.

Or do I become a pirate when I realise that I don’t have the CD and willingly choose to not delete the music? Then, being a pirate is only a thought, it’s something completely intangible in your head.

I don’t know what you think but, to me, living in a society where you can be punished for a thought is probably the worst nightmare I can dream of.

How do you rate a « Thought Police » on your morality scale?

Picture by Scott Ogilvie

=> http://www.flickr.com/photos/75231603@N00/115978730 Scott Ogilvie
